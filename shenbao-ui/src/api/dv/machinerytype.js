import request from '@/utils/request'

// 查询设备类型设置列表
export function listMachinerytype(query) {
  return request({
    url: '/dv/machinerytype/list',
    method: 'get',
    params: query
  })
}

// 查询设备类型设置详细
export function getMachinerytype(machineryTypeId) {
  return request({
    url: '/dv/machinerytype/' + machineryTypeId,
    method: 'get'
  })
}

// 新增设备类型设置
export function addMachinerytype(data) {
  return request({
    url: '/dv/machinerytype',
    method: 'post',
    data: data
  })
}

// 修改设备类型设置
export function updateMachinerytype(data) {
  return request({
    url: '/dv/machinerytype',
    method: 'put',
    data: data
  })
}

// 删除设备类型设置
export function delMachinerytype(machineryTypeId) {
  return request({
    url: '/dv/machinerytype/' + machineryTypeId,
    method: 'delete'
  })
}
