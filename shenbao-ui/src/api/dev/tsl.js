import request from '@/utils/request'

// 查询产品管理详细
export function addEitTsl(data) {
  return request({
    url: '/dev/tsl/addEitTsl',
    method: 'post',
    data: data
  })
}

// 新增
export function addTsl(data) {
  return request({
    url: '/dev/tsl/addTsl',
    method: 'post',
    data: data
  })
}

// 修改
export function updateTsl(data) {
  return request({
    url: '/dev/tsl/editTsl',
    method: 'post',
    data: data
  })
}

// 删除
export function removeTsl(data) {
  return request({
    url: '/dev/tsl/removeTsl',
    method: 'post',
    data: data
  })
}
