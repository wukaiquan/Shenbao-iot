import request from '@/utils/request'

// 查询设备类型列表
export function listClassified(query) {
  return request({
    url: '/dev/classified/list',
    method: 'get',
    params: query
  })
}

// 查询设备类型详细
export function getClassified(id) {
  return request({
    url: '/dev/classified/' + id,
    method: 'get'
  })
}

// 新增设备类型
export function addClassified(data) {
  return request({
    url: '/dev/classified',
    method: 'post',
    data: data
  })
}

// 修改设备类型
export function updateClassified(data) {
  return request({
    url: '/dev/classified',
    method: 'put',
    data: data
  })
}

// 删除设备类型
export function delClassified(id) {
  return request({
    url: '/dev/classified/' + id,
    method: 'delete'
  })
}
