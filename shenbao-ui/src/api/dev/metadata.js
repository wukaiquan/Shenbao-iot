import request from '@/utils/request'

// 查询物模型信息列表
export function listMetadata(query) {
  return request({
    url: '/dev/metadata/list',
    method: 'get',
    params: query
  })
}

// 查询物模型信息详细
export function getMetadata(id) {
  return request({
    url: '/dev/metadata/' + id,
    method: 'get'
  })
}

// 新增物模型信息
export function addMetadata(data) {
  return request({
    url: '/dev/metadata',
    method: 'post',
    data: data
  })
}

// 修改物模型信息
export function updateMetadata(data) {
  return request({
    url: '/dev/metadata',
    method: 'put',
    data: data
  })
}

// 删除物模型信息
export function delMetadata(id) {
  return request({
    url: '/dev/metadata/' + id,
    method: 'delete'
  })
}
