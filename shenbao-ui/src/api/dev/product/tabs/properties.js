import request from '@/utils/request'

// 查询属性列表
export function listProperties(query) {
  return request({
    url: '/system/properties/list',
    method: 'get',
    params: query
  })
}

// 查询属性详细
export function getProperties(identifier) {
  return request({
    url: '/system/properties/' + identifier,
    method: 'get'
  })
}

// 新增属性
export function addProperties(data) {
  return request({
    url: '/system/properties',
    method: 'post',
    data: data
  })
}

// 修改属性
export function updateProperties(data) {
  return request({
    url: '/system/properties',
    method: 'put',
    data: data
  })
}

// 删除属性
export function delProperties(identifier) {
  return request({
    url: '/system/properties/' + identifier,
    method: 'delete'
  })
}
