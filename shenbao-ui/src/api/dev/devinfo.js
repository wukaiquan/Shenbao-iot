import request from '@/utils/request'

// 查询设备管理列表
export function listDevinfo(query) {
  return request({
    url: '/dev/devinfo/list',
    method: 'get',
    params: query
  })
}

// 查询设备管理详细
export function getDevinfo(id) {
  return request({
    url: '/dev/devinfo/' + id,
    method: 'get'
  })
}

// 新增设备管理
export function addDevinfo(data) {
  return request({
    url: '/dev/devinfo',
    method: 'post',
    data: data
  })
}

// 修改设备管理
export function updateDevinfo(data) {
  return request({
    url: '/dev/devinfo',
    method: 'put',
    data: data
  })
}

// 删除设备管理
export function delDevinfo(id) {
  return request({
    url: '/dev/devinfo/' + id,
    method: 'delete'
  })
}
