/*
 Navicat Premium Data Transfer

 Source Server         : 122.114.60.67
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : 122.114.60.67:3306
 Source Schema         : shenbao-iot

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 11/07/2023 14:01:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for baokun_test
-- ----------------------------
DROP TABLE IF EXISTS `baokun_test`;
CREATE TABLE `baokun_test`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `head_file` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `is_open` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否启用（0.否，1. 是）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '测试' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of baokun_test
-- ----------------------------

-- ----------------------------
-- Table structure for dv_check_machinery
-- ----------------------------
DROP TABLE IF EXISTS `dv_check_machinery`;
CREATE TABLE `dv_check_machinery`  (
  `record_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `plan_id` bigint(20) NOT NULL COMMENT '计划ID',
  `machinery_id` bigint(20) NOT NULL COMMENT '设备ID',
  `machinery_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备编码',
  `machinery_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备名称',
  `machinery_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '品牌',
  `machinery_spec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格型号',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`record_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '点检设备表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dv_check_machinery
-- ----------------------------

-- ----------------------------
-- Table structure for dv_check_plan
-- ----------------------------
DROP TABLE IF EXISTS `dv_check_plan`;
CREATE TABLE `dv_check_plan`  (
  `plan_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '计划ID',
  `plan_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '计划编码',
  `plan_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '计划名称',
  `plan_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '计划类型',
  `start_date` datetime(0) NULL DEFAULT NULL COMMENT '开始日期',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '结束日期',
  `cycle_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '频率',
  `cycle_count` int(11) NULL DEFAULT NULL COMMENT '次数',
  `status` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`plan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备点检保养计划头表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dv_check_plan
-- ----------------------------

-- ----------------------------
-- Table structure for dv_check_subject
-- ----------------------------
DROP TABLE IF EXISTS `dv_check_subject`;
CREATE TABLE `dv_check_subject`  (
  `record_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `plan_id` bigint(20) NOT NULL COMMENT '计划ID',
  `subject_id` bigint(20) NOT NULL COMMENT '项目ID',
  `subject_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目编码',
  `subject_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `subject_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目类型',
  `subject_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目内容',
  `subject_standard` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标准',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`record_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '点检项目表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dv_check_subject
-- ----------------------------

-- ----------------------------
-- Table structure for dv_machinery
-- ----------------------------
DROP TABLE IF EXISTS `dv_machinery`;
CREATE TABLE `dv_machinery`  (
  `machinery_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '设备类型ID',
  `machinery_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备类型编码',
  `machinery_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备类型名称',
  `machinery_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '品牌',
  `machinery_spec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格型号',
  `machinery_type_id` bigint(20) NOT NULL COMMENT '设备类型ID',
  `machinery_type_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备类型编码',
  `machinery_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备类型名称',
  `workshop_id` bigint(20) NOT NULL COMMENT '所属车间ID',
  `workshop_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属车间编码',
  `workshop_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属车间名称',
  `status` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'STOP' COMMENT '设备状态',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`machinery_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dv_machinery
-- ----------------------------

-- ----------------------------
-- Table structure for dv_repair
-- ----------------------------
DROP TABLE IF EXISTS `dv_repair`;
CREATE TABLE `dv_repair`  (
  `repair_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '维修单ID',
  `repair_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '维修单编号',
  `repair_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '维修单名称',
  `machinery_id` bigint(20) NOT NULL COMMENT '设备ID',
  `machinery_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备编码',
  `machinery_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备名称',
  `machinery_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '品牌',
  `machinery_spec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格型号',
  `machinery_type_id` bigint(20) NOT NULL COMMENT '设备类型ID',
  `require_date` datetime(0) NULL DEFAULT NULL COMMENT '报修日期',
  `finish_date` datetime(0) NULL DEFAULT NULL COMMENT '维修完成日期',
  `confirm_date` datetime(0) NULL DEFAULT NULL COMMENT '验收日期',
  `repair_result` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '维修结果',
  `accepted_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '维修人员',
  `confirm_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '验收人员',
  `status` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'PREPARE' COMMENT '单据状态',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`repair_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备维修单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dv_repair
-- ----------------------------

-- ----------------------------
-- Table structure for dv_repair_line
-- ----------------------------
DROP TABLE IF EXISTS `dv_repair_line`;
CREATE TABLE `dv_repair_line`  (
  `line_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '行ID',
  `repair_id` bigint(20) NOT NULL COMMENT '维修单ID',
  `subject_id` bigint(20) NOT NULL COMMENT '项目ID',
  `subject_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目编码',
  `subject_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `subject_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目类型',
  `subject_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目内容',
  `subject_standard` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标准',
  `malfunction` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '故障描述',
  `malfunction_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '故障描述资源',
  `repair_des` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '维修情况',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`line_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备维修单行' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dv_repair_line
-- ----------------------------

-- ----------------------------
-- Table structure for dv_subject
-- ----------------------------
DROP TABLE IF EXISTS `dv_subject`;
CREATE TABLE `dv_subject`  (
  `subject_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '项目ID',
  `subject_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目编码',
  `subject_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `subject_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '项目类型',
  `subject_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目内容',
  `subject_standard` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标准',
  `enable_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否启用',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`subject_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备点检保养项目表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dv_subject
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'shenbao_project', '项目管理', NULL, NULL, 'ShenbaoProject', 'crud', 'com.shenbaoiot.basics', 'basics', 'project', '项目管理', '常宝坤-13964179025', '0', '/', '{\"parentMenuId\":\"1061\"}', 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08', NULL);
INSERT INTO `gen_table` VALUES (3, 'shenbao_workshop', '车间管理', NULL, NULL, 'ShenbaoWorkshop', 'crud', 'com.shenbaoiot.basics', 'basics', 'workshop', '车间管理', '常宝坤-13964179025', '0', '/', '{\"parentMenuId\":\"1061\"}', 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55', NULL);
INSERT INTO `gen_table` VALUES (6, 'shenbao_dev_classified', '设备类型表', '', '', 'DevClassified', 'tree', 'com.shenbaoiot.dev', 'dev', 'classified', '设备类型', '常宝坤-13964179025', '0', '/', '{\"treeCode\":\"id\",\"treeName\":\"classified_name\",\"treeParentCode\":\"parent_id\",\"parentMenuId\":\"1068\"}', 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27', NULL);
INSERT INTO `gen_table` VALUES (7, 'shenbao_dev_product', '产品信息表', NULL, NULL, 'DevProduct', 'crud', 'com.shenbaoiot.dev', 'dev', 'product', '产品管理', '常宝坤 - 13964179025', '0', '/', '{\"parentMenuId\":\"1068\"}', 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46', NULL);
INSERT INTO `gen_table` VALUES (8, 'baokun_test', '测试', NULL, NULL, 'BaokunTest', 'crud', 'com.baokun.system', 'system', 'test', '测试', '常宝坤', '0', '/', '{}', 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15', NULL);
INSERT INTO `gen_table` VALUES (9, 'model_properties', '属性', NULL, NULL, 'ModelProperties', 'crud', 'com.shenbaoiot.system', 'system', 'properties', '属性', 'ruoyi', '0', '/', '{}', 'admin', '2023-05-10 13:44:06', '', '2023-05-10 14:32:57', NULL);
INSERT INTO `gen_table` VALUES (10, 'shenbao_metadata', '物模型信息', NULL, NULL, 'Metadata', 'crud', 'com.shenbaoiot.dev', 'dev', 'metadata', '物模型信息', '常宝坤', '0', '/', '{\"parentMenuId\":\"1068\"}', 'admin', '2023-05-10 14:46:27', '', '2023-05-10 16:26:41', NULL);
INSERT INTO `gen_table` VALUES (11, 'shenbao_dev_info', '设备信息', NULL, NULL, 'DevInfo', 'crud', 'com.shenbaoiot.dev', 'dev', 'devinfo', '设备管理', '常宝坤', '0', '/', '{\"parentMenuId\":\"1068\"}', 'admin', '2023-05-12 15:40:01', '', '2023-05-12 16:07:19', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 179 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (2, '1', 'project_name', '项目名称', 'varchar(255)', 'String', 'projectName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (3, '1', 'project_num', '项目编号', 'varchar(255)', 'String', 'projectNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (4, '1', 'project_address', '项目地址', 'varchar(255)', 'String', 'projectAddress', '0', '0', NULL, '1', '1', '1', '0', 'EQ', 'textarea', '', 4, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (5, '1', 'project_linkman', '联系人', 'varchar(255)', 'String', 'projectLinkman', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (6, '1', 'project_tel', '联系方式', 'varchar(255)', 'String', 'projectTel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (7, '1', 'project_des', '项目描述', 'varchar(255)', 'String', 'projectDes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (8, '1', 'project_file', '项目资料', 'varchar(500)', 'String', 'projectFile', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'fileUpload', '', 8, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (9, '1', 'open', '是否启用', 'char(2)', 'String', 'open', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'shenbao_state', 9, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (10, '1', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (11, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (12, '1', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (13, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (14, '1', 'remark', '备注信息', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 14, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (15, '1', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'input', '', 15, 'admin', '2023-03-10 08:01:36', '', '2023-03-10 10:21:08');
INSERT INTO `gen_table_column` VALUES (31, '3', 'workshop_id', '车间ID', 'bigint(20)', 'Long', 'workshopId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (32, '3', 'workshop_code', '车间编码', 'varchar(64)', 'String', 'workshopCode', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (33, '3', 'workshop_name', '车间名称', 'varchar(255)', 'String', 'workshopName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (34, '3', 'area', '面积', 'double(12,2)', 'BigDecimal', 'area', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (35, '3', 'charge', '负责人', 'varchar(64)', 'String', 'charge', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (36, '3', 'enable_flag', '是否启用', 'char(1)', 'String', 'enableFlag', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'shenbao_state', 6, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (37, '3', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 7, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (38, '3', 'attr1', '预留字段1', 'varchar(64)', 'String', 'attr1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (39, '3', 'attr2', '预留字段2', 'varchar(255)', 'String', 'attr2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (40, '3', 'attr3', '预留字段3', 'int(11)', 'Long', 'attr3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (41, '3', 'attr4', '预留字段4', 'int(11)', 'Long', 'attr4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (42, '3', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (43, '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (44, '3', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (45, '3', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2023-03-10 10:49:48', '', '2023-03-10 10:52:55');
INSERT INTO `gen_table_column` VALUES (81, '6', 'id', '设备类型id', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (82, '6', 'classified_code', '设备类型编码', 'varchar(64)', 'String', 'classifiedCode', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (83, '6', 'classified_name', '设备类型名称', 'varchar(255)', 'String', 'classifiedName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (84, '6', 'device_type', '设备类型分类', 'varchar(255)', 'String', 'deviceType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'device_type', 4, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (85, '6', 'parent_id', '父类型id', 'bigint(20)', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (86, '6', 'enable_flag', '是否启用', 'char(1)', 'String', 'enableFlag', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'shenbao_state', 6, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (87, '6', 'ancestors', '祖级列表', 'varchar(50)', 'String', 'ancestors', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (88, '6', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (89, '6', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 9, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (90, '6', 'attr1', '预留字段1', 'varchar(64)', 'String', 'attr1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (91, '6', 'attr2', '预留字段2', 'varchar(255)', 'String', 'attr2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (92, '6', 'attr3', '预留字段3', 'int(11)', 'Long', 'attr3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (93, '6', 'attr4', '预留字段4', 'int(11)', 'Long', 'attr4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (94, '6', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (95, '6', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (96, '6', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 16, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (97, '6', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 17, 'admin', '2023-03-11 08:31:58', '', '2023-03-11 08:34:27');
INSERT INTO `gen_table_column` VALUES (98, '7', 'id', 'ID', 'bigint(64)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (99, '7', 'product_name', '产品名称', 'varchar(255)', 'String', 'productName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (100, '7', 'product_marking', '产品唯一标识', 'varchar(255)', 'String', 'productMarking', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (101, '7', 'product_classified_id', '所属品类ID', 'bigint(64)', 'Long', 'productClassifiedId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (102, '7', 'product_classified_name', '所属品类名称', 'varchar(255)', 'String', 'productClassifiedName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (103, '7', 'device_type', '设备类型', 'varchar(255)', 'String', 'deviceType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'device_type', 6, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (104, '7', 'project_id', '所属项目', 'varchar(64)', 'String', 'projectId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (105, '7', 'project_name', '项目名称', 'varchar(255)', 'String', 'projectName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (106, '7', 'dept_id', '所属部门', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (107, '7', 'dept_name', '部门名称', 'varchar(255)', 'String', 'deptName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 10, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (108, '7', 'product_state', '产品状态', 'tinyint(4)', 'Integer', 'productState', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'device_status', 11, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (109, '7', 'metadata', '物模型定义', 'longtext', 'String', 'metadata', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 12, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (110, '7', 'photo_url', '设备图标', 'varchar(1024)', 'String', 'photoUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'imageUpload', '', 13, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (111, '7', 'network_way', '连网方式', 'varchar(64)', 'String', 'networkWay', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'fileUpload', 'network_way', 14, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (112, '7', 'message_protocol', '消息协议ID', 'varchar(255)', 'String', 'messageProtocol', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (113, '7', 'protocol_name', '消息协议名称', 'varchar(255)', 'String', 'protocolName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 16, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (114, '7', 'transport_protocol', '传输协议', 'varchar(255)', 'String', 'transportProtocol', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (115, '7', 'store_policy_configuration', '数据存储策略相关配置', 'text', 'String', 'storePolicyConfiguration', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 18, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (116, '7', 'access_name', '设备接入方式名称', 'varchar(255)', 'String', 'accessName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 19, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (117, '7', 'access_provider', '设备接入方式', 'varchar(64)', 'String', 'accessProvider', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 20, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (118, '7', 'store_policy', '数据存储策略', 'varchar(255)', 'String', 'storePolicy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (119, '7', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 22, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (120, '7', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 23, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (121, '7', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 24, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (122, '7', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 25, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (123, '7', 'remark', '备注信息', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 26, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (124, '7', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'input', '', 27, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (125, '7', 'attr1', '预留字段1', 'varchar(64)', 'String', 'attr1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 28, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (126, '7', 'attr2', '预留字段2', 'varchar(255)', 'String', 'attr2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 29, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (127, '7', 'attr3', '预留字段3', 'int(11)', 'Long', 'attr3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 30, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (128, '7', 'attr4', '预留字段4', 'int(11)', 'Long', 'attr4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 31, 'admin', '2023-03-11 08:52:04', '', '2023-03-11 10:59:46');
INSERT INTO `gen_table_column` VALUES (129, '8', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (130, '8', 'name', '姓名', 'varchar(255)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (131, '8', 'sex', '性别', 'char(1)', 'String', 'sex', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'sys_user_sex', 3, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (132, '8', 'head_file', '头像地址', 'varchar(500)', 'String', 'headFile', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'fileUpload', '', 4, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (133, '8', 'is_open', '是否启用（0.否，1. 是）', 'char(2)', 'String', 'isOpen', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (134, '8', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (135, '8', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (136, '8', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (137, '8', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (138, '8', 'remark', '备注信息', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (139, '8', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (140, '8', 'attr1', '预留字段1', 'varchar(64)', 'String', 'attr1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (141, '8', 'attr2', '预留字段2', 'varchar(255)', 'String', 'attr2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (142, '8', 'attr3', '预留字段3', 'int(11)', 'Long', 'attr3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (143, '8', 'attr4', '预留字段4', 'int(11)', 'Long', 'attr4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2023-03-15 13:37:06', '', '2023-03-15 13:38:15');
INSERT INTO `gen_table_column` VALUES (144, '9', 'identifier', '属性唯一标识符', 'varchar(255)', 'String', 'identifier', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2023-05-10 13:44:06', '', '2023-05-10 14:32:57');
INSERT INTO `gen_table_column` VALUES (145, '9', 'name', '属性名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-05-10 13:44:06', '', '2023-05-10 14:32:57');
INSERT INTO `gen_table_column` VALUES (146, '9', 'accessMode', '属性读写类型', 'varchar(255)', 'String', 'accessMode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'read_write_type', 3, 'admin', '2023-05-10 13:44:06', '', '2023-05-10 14:32:57');
INSERT INTO `gen_table_column` VALUES (147, '9', 'required', '是否是标准功能', 'varchar(255)', 'String', 'required', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'true_false', 4, 'admin', '2023-05-10 13:44:06', '', '2023-05-10 14:32:57');
INSERT INTO `gen_table_column` VALUES (148, '9', 'dataType', '数据类型', 'varchar(255)', 'String', 'dataType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2023-05-10 13:44:06', '', '2023-05-10 14:32:57');
INSERT INTO `gen_table_column` VALUES (149, '10', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-05-10 14:46:27', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (150, '10', 'metadata', '物模型', 'longtext', 'String', 'metadata', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 2, 'admin', '2023-05-10 14:46:27', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (151, '10', 'who_belongs_id', '属于产品或设备的id', 'bigint(20)', 'Long', 'whoBelongsId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-05-10 14:46:27', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (152, '10', 'who_belongs', '属于谁（0-产品，1-设备）', 'char(2)', 'String', 'whoBelongs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-05-10 14:46:27', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (153, '10', 'open', '是否启用（0.否，1. 是）', 'char(2)', 'String', 'open', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-05-10 14:46:27', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (154, '10', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2023-05-10 14:46:27', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (155, '10', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (156, '10', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (157, '10', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (158, '10', 'remark', '备注信息', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (159, '10', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (160, '10', 'attr1', '预留字段1', 'varchar(64)', 'String', 'attr1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (161, '10', 'attr2', '预留字段2', 'varchar(255)', 'String', 'attr2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (162, '10', 'attr3', '预留字段3', 'int(11)', 'Long', 'attr3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (163, '10', 'attr4', '预留字段4', 'int(11)', 'Long', 'attr4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2023-05-10 14:46:28', '', '2023-05-10 16:26:41');
INSERT INTO `gen_table_column` VALUES (164, '11', 'id', 'ID', 'bigint(64)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (165, '11', 'dev_name', '设备名称', 'varchar(255)', 'String', 'devName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (166, '11', 'dev_marking', '设备唯一标识', 'varchar(255)', 'String', 'devMarking', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (167, '11', 'product_id', '所属产品ID', 'bigint(64)', 'Long', 'productId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (168, '11', 'dev_state', '设备状态', 'tinyint(4)', 'Integer', 'devState', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'device_status', 5, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (169, '11', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (170, '11', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (171, '11', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (172, '11', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (173, '11', 'remark', '备注信息', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (174, '11', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (175, '11', 'attr1', '预留字段1', 'varchar(64)', 'String', 'attr1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (176, '11', 'attr2', '预留字段2', 'varchar(255)', 'String', 'attr2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (177, '11', 'attr3', '预留字段3', 'int(11)', 'Long', 'attr3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');
INSERT INTO `gen_table_column` VALUES (178, '11', 'attr4', '预留字段4', 'int(11)', 'Long', 'attr4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2023-05-12 15:40:02', '', '2023-05-12 16:07:19');

-- ----------------------------
-- Table structure for model_properties
-- ----------------------------
DROP TABLE IF EXISTS `model_properties`;
CREATE TABLE `model_properties`  (
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性唯一标识符（物模型模块下唯一',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性名称',
  `accessMode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性读写类型：只读（r）或读写（rw）',
  `required` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否是标准功能的必选属性：是（true），否（false）。',
  `dataType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据类型'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '属性' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_properties
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日历信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '调度器状态表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for shenbao_dev_classified
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_dev_classified`;
CREATE TABLE `shenbao_dev_classified`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '设备类型id',
  `classified_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备类型编码',
  `classified_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备类型名称',
  `device_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备类型：0-直连设备，1-网关子设备，2-网关设备',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父类型id',
  `enable_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否启用（0正常 1停用）',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 206 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shenbao_dev_classified
-- ----------------------------
INSERT INTO `shenbao_dev_classified` VALUES (200, 'M_TYPE_000', '设备分类', '0', 0, '1', '', 0, '', NULL, NULL, 0, 0, '', NULL, '', '2023-05-15 14:20:18');
INSERT INTO `shenbao_dev_classified` VALUES (201, 'M_TYPE_001', '加注机', '0', 200, '1', '', 1, '', NULL, NULL, 0, 0, '', '2023-03-11 08:38:27', '', NULL);
INSERT INTO `shenbao_dev_classified` VALUES (202, 'M_TYPE_002', '打标机', '0', 200, '1', '', 2, '', NULL, NULL, 0, 0, '', '2023-03-11 08:39:20', '', NULL);
INSERT INTO `shenbao_dev_classified` VALUES (203, 'M_TYPE_100', '退火', '0', 0, '1', '', 0, '', NULL, NULL, 0, 0, '', '2023-03-11 11:08:36', '', NULL);
INSERT INTO `shenbao_dev_classified` VALUES (204, 'M_TYPE_101', '退火炉', '0', 203, '1', '', 0, '', NULL, NULL, 0, 0, '', '2023-03-11 11:09:03', '', NULL);
INSERT INTO `shenbao_dev_classified` VALUES (205, 'M_TYPE_200', '温湿度', '0', 0, '1', '', 0, '', NULL, NULL, 0, 0, '', '2023-05-16 10:13:52', '', NULL);

-- ----------------------------
-- Table structure for shenbao_dev_info
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_dev_info`;
CREATE TABLE `shenbao_dev_info`  (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dev_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备名称',
  `dev_marking` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备唯一标识',
  `product_id` bigint(64) NULL DEFAULT NULL COMMENT '所属产品ID',
  `dev_state` tinyint(4) NULL DEFAULT NULL COMMENT '设备状态 1启用,0禁用',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '设备信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shenbao_dev_info
-- ----------------------------
INSERT INTO `shenbao_dev_info` VALUES (1, '温湿度1', 'wsd1', 1, 1, NULL, '2023-05-12 16:31:04', NULL, NULL, NULL, '0', NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for shenbao_dev_product
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_dev_product`;
CREATE TABLE `shenbao_dev_product`  (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '产品名称',
  `product_marking` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '产品唯一标识',
  `product_classified_id` bigint(64) NULL DEFAULT NULL COMMENT '所属品类ID',
  `product_classified_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属品类名称',
  `device_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备类型：0-直连设备，1-网关子设备，2-网关设备',
  `project_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属项目',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '所属部门',
  `dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '物模型定义',
  `photo_url` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `network_way` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）',
  `message_protocol` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息协议ID',
  `protocol_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息协议名称',
  `transport_protocol` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '传输协议',
  `store_policy_configuration` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '数据存储策略相关配置',
  `access_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备接入方式名称',
  `access_provider` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备接入方式',
  `store_policy` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据存储策略',
  `product_state` tinyint(4) NULL DEFAULT 0 COMMENT '产品状态 1正常,0禁用',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_prod_class_id`(`product_classified_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shenbao_dev_product
-- ----------------------------
INSERT INTO `shenbao_dev_product` VALUES (1, '温湿度', 'wsd12', 202, NULL, '0', NULL, NULL, NULL, NULL, NULL, '/profile/upload/2023/06/12/微信图片_20230424173539_20230612140824A001.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2023-03-11 11:06:09', NULL, '2023-07-06 11:30:15', NULL, '0', NULL, NULL, 0, 0);
INSERT INTO `shenbao_dev_product` VALUES (2, '串口服务器1', 'ckfwq1', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, '/profile/upload/2023/05/07/设备_20230507190750A008.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2023-03-11 11:06:57', NULL, '2023-05-31 16:40:51', NULL, '0', NULL, NULL, 0, 0);
INSERT INTO `shenbao_dev_product` VALUES (3, '加注机', 'jzj1', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '/profile/upload/2023/05/07/111_20230507174447A004.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2023-03-11 11:07:37', NULL, '2023-05-07 17:44:49', NULL, '0', NULL, NULL, 0, 0);
INSERT INTO `shenbao_dev_product` VALUES (9, '测试5', '5', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '/profile/upload/2023/05/07/设备_20230507200152A011.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2023-05-07 20:02:00', NULL, '2023-05-15 14:18:19', NULL, '5', '5', '5', 5, 5);
INSERT INTO `shenbao_dev_product` VALUES (14, '产品测试1', 'cp1', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '/profile/upload/2023/05/15/企业微信截图_20230512103620_20230515154915A001.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2023-05-15 15:49:18', NULL, NULL, NULL, '0', NULL, NULL, 0, 0);
INSERT INTO `shenbao_dev_product` VALUES (15, '产品测试2', '6', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '/profile/upload/2023/05/16/设备_20230507200245A014_20230516101810A002.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2023-05-16 10:18:19', NULL, NULL, NULL, '0', NULL, NULL, 0, 0);
INSERT INTO `shenbao_dev_product` VALUES (19, '11', '11', 201, NULL, '0', NULL, NULL, NULL, NULL, NULL, '/profile/upload/2023/06/05/微信图片_20230424173539_20230605153831A001.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2023-06-05 15:38:33', NULL, NULL, NULL, '0', NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for shenbao_metadata
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_metadata`;
CREATE TABLE `shenbao_metadata`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '物模型',
  `who_belongs_id` bigint(20) NULL DEFAULT 0 COMMENT '属于产品或设备的id',
  `who_belongs` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属于谁（0-产品，1-设备）',
  `open` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否启用（0.否，1. 是）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物模型信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shenbao_metadata
-- ----------------------------
INSERT INTO `shenbao_metadata` VALUES (1, '{\"events\":[{}],\"profile\":{\"productKey\":\"hb1neLMPUNf\"},\"properties\":[{\"accessMode\":\"r\",\"dataType\":\"{\\\"type\\\":\\\"struct\\\",\\\"specs\\\":[{\\\"identifier\\\":\\\"Longitude\\\",\\\"name\\\":\\\"经度\\\",\\\"dataType\\\":{\\\"type\\\":\\\"double\\\",\\\"specs\\\":{\\\"min\\\":\\\"-180\\\",\\\"max\\\":\\\"180\\\",\\\"unit\\\":\\\"°\\\",\\\"unitName\\\":\\\"度\\\",\\\"step\\\":\\\"0.01\\\"}}},{\\\"identifier\\\":\\\"Latitude\\\",\\\"name\\\":\\\"纬度\\\",\\\"dataType\\\":{\\\"type\\\":\\\"double\\\",\\\"specs\\\":{\\\"min\\\":\\\"-90\\\",\\\"max\\\":\\\"90\\\",\\\"unit\\\":\\\"°\\\",\\\"unitName\\\":\\\"度\\\",\\\"step\\\":\\\"0.01\\\"}}},{\\\"identifier\\\":\\\"Altitude\\\",\\\"name\\\":\\\"海拔\\\",\\\"dataType\\\":{\\\"type\\\":\\\"double\\\",\\\"specs\\\":{\\\"min\\\":\\\"0\\\",\\\"max\\\":\\\"9999\\\",\\\"unit\\\":\\\"m\\\",\\\"unitName\\\":\\\"米\\\",\\\"step\\\":\\\"0.01\\\"}}},{\\\"identifier\\\":\\\"CoordinateSystem\\\",\\\"name\\\":\\\"坐标系统\\\",\\\"dataType\\\":{\\\"type\\\":\\\"enum\\\",\\\"specs\\\":{\\\"1\\\":\\\"WGS_84\\\",\\\"2\\\":\\\"GCJ_02\\\"}}}]}\",\"identifier\":\"GeoLocation\",\"name\":\"地理位置\",\"required\":true},{\"accessMode\":\"rw\",\"dataType\":\"{\\\"type\\\":\\\"float\\\",\\\"specs\\\":{\\\"min\\\":\\\"0\\\",\\\"max\\\":\\\"1000\\\",\\\"unit\\\":\\\"mg/L\\\",\\\"unitName\\\":\\\"毫克每升\\\",\\\"step\\\":\\\"0.1\\\"}}\",\"identifier\":\"alkalinity\",\"name\":\"碱度\",\"required\":false},{\"accessMode\":\"r\",\"dataType\":\"{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"10\\\",\\\"unit\\\":\\\"度\\\",\\\"unitName\\\":\\\"摄氏度\\\",\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{}}}\",\"identifier\":\"wd\",\"name\":\"温度\",\"required\":false},{\"accessMode\":\"r\",\"dataType\":\"{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"1\\\",\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{}}}\",\"identifier\":\"sd\",\"name\":\"湿度\",\"required\":false},{\"accessMode\":\"r\",\"dataType\":\"{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"11\\\",\\\"unit\\\":\\\"111\\\",\\\"unitName\\\":\\\"34\\\",\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{}}}\",\"identifier\":\"34234\",\"name\":\"34dsf\",\"required\":false}],\"schema\":\"https://iotx-tsl.oss-ap-southeast-1.aliyuncs.com/schema.json\",\"services\":[{},{}]}', 1, '0', '0', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, 0, 0);
INSERT INTO `shenbao_metadata` VALUES (2, '{\n  \"schema\": \"https://iotx-tsl.oss-ap-southeast-1.aliyuncs.com/schema.json\",\n  \"profile\": {\n    \"version\": \"1.0\",\n    \"productKey\": \"hb1neLMPUNf\"\n  },\n  \"properties\": [\n    {\n      \"identifier\": \"GeoLocation\",\n      \"name\": \"地理位置\",\n      \"accessMode\": \"r\",\n      \"required\": true,\n      \"dataType\": {\n        \"type\": \"struct\",\n        \"specs\": [\n          {\n            \"identifier\": \"Longitude\",\n            \"name\": \"经度\",\n            \"dataType\": {\n              \"type\": \"double\",\n              \"specs\": {\n                \"min\": \"-180\",\n                \"max\": \"180\",\n                \"unit\": \"°\",\n                \"unitName\": \"度\",\n                \"step\": \"0.01\"\n              }\n            }\n          },\n          {\n            \"identifier\": \"Latitude\",\n            \"name\": \"纬度\",\n            \"dataType\": {\n              \"type\": \"double\",\n              \"specs\": {\n                \"min\": \"-90\",\n                \"max\": \"90\",\n                \"unit\": \"°\",\n                \"unitName\": \"度\",\n                \"step\": \"0.01\"\n              }\n            }\n          },\n          {\n            \"identifier\": \"Altitude\",\n            \"name\": \"海拔\",\n            \"dataType\": {\n              \"type\": \"double\",\n              \"specs\": {\n                \"min\": \"0\",\n                \"max\": \"9999\",\n                \"unit\": \"m\",\n                \"unitName\": \"米\",\n                \"step\": \"0.01\"\n              }\n            }\n          },\n          {\n            \"identifier\": \"CoordinateSystem\",\n            \"name\": \"坐标系统\",\n            \"dataType\": {\n              \"type\": \"enum\",\n              \"specs\": {\n                \"1\": \"WGS_84\",\n                \"2\": \"GCJ_02\"\n              }\n            }\n          }\n        ]\n      }\n    },\n    {\n      \"identifier\": \"alkalinity\",\n      \"name\": \"碱度\",\n      \"accessMode\": \"rw\",\n      \"desc\": \"碱度\",\n      \"required\": false,\n      \"dataType\": {\n        \"type\": \"float\",\n        \"specs\": {\n          \"min\": \"0\",\n          \"max\": \"1000\",\n          \"unit\": \"mg/L\",\n          \"unitName\": \"毫克每升\",\n          \"step\": \"0.1\"\n        }\n      }\n    }\n  ],\n  \"events\": [\n    {\n      \"identifier\": \"post\",\n      \"name\": \"post\",\n      \"type\": \"info\",\n      \"required\": true,\n      \"desc\": \"属性上报\",\n      \"method\": \"thing.event.property.post\",\n      \"outputData\": [\n        {\n          \"identifier\": \"GeoLocation\",\n          \"name\": \"地理位置\",\n          \"dataType\": {\n            \"type\": \"struct\",\n            \"specs\": [\n              {\n                \"identifier\": \"Longitude\",\n                \"name\": \"经度\",\n                \"dataType\": {\n                  \"type\": \"double\",\n                  \"specs\": {\n                    \"min\": \"-180\",\n                    \"max\": \"180\",\n                    \"unit\": \"°\",\n                    \"unitName\": \"度\",\n                    \"step\": \"0.01\"\n                  }\n                }\n              },\n              {\n                \"identifier\": \"Latitude\",\n                \"name\": \"纬度\",\n                \"dataType\": {\n                  \"type\": \"double\",\n                  \"specs\": {\n                    \"min\": \"-90\",\n                    \"max\": \"90\",\n                    \"unit\": \"°\",\n                    \"unitName\": \"度\",\n                    \"step\": \"0.01\"\n                  }\n                }\n              },\n              {\n                \"identifier\": \"Altitude\",\n                \"name\": \"海拔\",\n                \"dataType\": {\n                  \"type\": \"double\",\n                  \"specs\": {\n                    \"min\": \"0\",\n                    \"max\": \"9999\",\n                    \"unit\": \"m\",\n                    \"unitName\": \"米\",\n                    \"step\": \"0.01\"\n                  }\n                }\n              },\n              {\n                \"identifier\": \"CoordinateSystem\",\n                \"name\": \"坐标系统\",\n                \"dataType\": {\n                  \"type\": \"enum\",\n                  \"specs\": {\n                    \"1\": \"WGS_84\",\n                    \"2\": \"GCJ_02\"\n                  }\n                }\n              }\n            ]\n          }\n        },\n        {\n          \"identifier\": \"alkalinity\",\n          \"name\": \"碱度\",\n          \"dataType\": {\n            \"type\": \"float\",\n            \"specs\": {\n              \"min\": \"0\",\n              \"max\": \"1000\",\n              \"unit\": \"mg/L\",\n              \"unitName\": \"毫克每升\",\n              \"step\": \"0.1\"\n            }\n          }\n        }\n      ]\n    }\n  ],\n  \"services\": [\n    {\n      \"identifier\": \"set\",\n      \"name\": \"set\",\n      \"required\": true,\n      \"callType\": \"async\",\n      \"desc\": \"属性设置\",\n      \"method\": \"thing.service.property.set\",\n      \"inputData\": [\n        {\n          \"identifier\": \"alkalinity\",\n          \"name\": \"碱度\",\n          \"dataType\": {\n            \"type\": \"float\",\n            \"specs\": {\n              \"min\": \"0\",\n              \"max\": \"1000\",\n              \"unit\": \"mg/L\",\n              \"unitName\": \"毫克每升\",\n              \"step\": \"0.1\"\n            }\n          }\n        }\n      ],\n      \"outputData\": []\n    },\n    {\n      \"identifier\": \"get\",\n      \"name\": \"get\",\n      \"required\": true,\n      \"callType\": \"async\",\n      \"desc\": \"属性获取\",\n      \"method\": \"thing.service.property.get\",\n      \"inputData\": [\n        \"GeoLocation\",\n        \"alkalinity\"\n      ],\n      \"outputData\": [\n        {\n          \"identifier\": \"GeoLocation\",\n          \"name\": \"地理位置\",\n          \"dataType\": {\n            \"type\": \"struct\",\n            \"specs\": [\n              {\n                \"identifier\": \"Longitude\",\n                \"name\": \"经度\",\n                \"dataType\": {\n                  \"type\": \"double\",\n                  \"specs\": {\n                    \"min\": \"-180\",\n                    \"max\": \"180\",\n                    \"unit\": \"°\",\n                    \"unitName\": \"度\",\n                    \"step\": \"0.01\"\n                  }\n                }\n              },\n              {\n                \"identifier\": \"Latitude\",\n                \"name\": \"纬度\",\n                \"dataType\": {\n                  \"type\": \"double\",\n                  \"specs\": {\n                    \"min\": \"-90\",\n                    \"max\": \"90\",\n                    \"unit\": \"°\",\n                    \"unitName\": \"度\",\n                    \"step\": \"0.01\"\n                  }\n                }\n              },\n              {\n                \"identifier\": \"Altitude\",\n                \"name\": \"海拔\",\n                \"dataType\": {\n                  \"type\": \"double\",\n                  \"specs\": {\n                    \"min\": \"0\",\n                    \"max\": \"9999\",\n                    \"unit\": \"m\",\n                    \"unitName\": \"米\",\n                    \"step\": \"0.01\"\n                  }\n                }\n              },\n              {\n                \"identifier\": \"CoordinateSystem\",\n                \"name\": \"坐标系统\",\n                \"dataType\": {\n                  \"type\": \"enum\",\n                  \"specs\": {\n                    \"1\": \"WGS_84\",\n                    \"2\": \"GCJ_02\"\n                  }\n                }\n              }\n            ]\n          }\n        },\n        {\n          \"identifier\": \"alkalinity\",\n          \"name\": \"碱度\",\n          \"dataType\": {\n            \"type\": \"float\",\n            \"specs\": {\n              \"min\": \"0\",\n              \"max\": \"1000\",\n              \"unit\": \"mg/L\",\n              \"unitName\": \"毫克每升\",\n              \"step\": \"0.1\"\n            }\n          }\n        }\n      ]\n    }\n  ]\n}', 2, '0', '0', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for shenbao_oauth2
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_oauth2`;
CREATE TABLE `shenbao_oauth2`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `oauth2_app_id` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `oauth2_app_secret` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `oauth2_code` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shenbao_oauth2
-- ----------------------------

-- ----------------------------
-- Table structure for shenbao_project
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_project`;
CREATE TABLE `shenbao_project`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `project_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `project_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目编号',
  `project_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目地址',
  `project_linkman` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `project_tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `project_des` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目描述',
  `project_file` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目资料',
  `open` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否启用（0.否，1. 是）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '项目管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shenbao_project
-- ----------------------------
INSERT INTO `shenbao_project` VALUES (1, '数据采集平台', 'balkwill', '山东省济南市历城区', '常宝坤', '13964179025', NULL, NULL, '1', NULL, '2020-08-15 14:43:18', NULL, '2023-03-15 13:35:10', '                                                                                                                                                                                                                                                                                                                                                                                                                        \r\n                    \r\n                    \r\n                    \r\n                    \r\n  ', '1');
INSERT INTO `shenbao_project` VALUES (2, '智慧消防演示平台', '0001', '山东省济南市历下区', '常宝坤', '', NULL, NULL, '0', NULL, '2021-03-01 14:15:37', NULL, '2023-03-15 13:35:26', '                                                                                                \r\n                    \r\n                    \r\n                    \r\n                    ', '1');

-- ----------------------------
-- Table structure for shenbao_test
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_test`;
CREATE TABLE `shenbao_test`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `open` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否启用（0.否，1. 是）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '测试' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shenbao_test
-- ----------------------------

-- ----------------------------
-- Table structure for shenbao_workshop
-- ----------------------------
DROP TABLE IF EXISTS `shenbao_workshop`;
CREATE TABLE `shenbao_workshop`  (
  `workshop_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '车间ID',
  `workshop_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '车间编码',
  `workshop_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '车间名称',
  `area` double(12, 2) NULL DEFAULT NULL COMMENT '面积',
  `charge` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `enable_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否启用',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`workshop_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '车间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shenbao_workshop
-- ----------------------------
INSERT INTO `shenbao_workshop` VALUES (1, '111', '1号车间', 125.00, '常宝坤', '1', '', NULL, NULL, 0, 0, '', '2023-03-10 10:56:29', '', '2023-03-10 11:08:20');
INSERT INTO `shenbao_workshop` VALUES (2, '222', '2号车间', 1110.00, '常宝坤', '1', '', NULL, NULL, 0, 0, '', '2023-03-10 11:08:15', '', NULL);

-- ----------------------------
-- Table structure for sys_auto_code_part
-- ----------------------------
DROP TABLE IF EXISTS `sys_auto_code_part`;
CREATE TABLE `sys_auto_code_part`  (
  `part_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '分段ID',
  `rule_id` bigint(20) NOT NULL COMMENT '规则ID',
  `part_index` int(11) NOT NULL COMMENT '分段序号',
  `part_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分段类型，INPUTCHAR：输入字符，NOWDATE：当前日期时间，FIXCHAR：固定字符，SERIALNO：流水号',
  `part_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分段编号',
  `part_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分段名称',
  `part_length` int(11) NOT NULL COMMENT '分段长度',
  `date_format` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `input_character` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '输入字符',
  `fix_character` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '固定字符',
  `seria_start_no` int(11) NULL DEFAULT NULL COMMENT '流水号起始值',
  `seria_step` int(11) NULL DEFAULT NULL COMMENT '流水号步长',
  `seria_now_no` int(11) NULL DEFAULT NULL COMMENT '流水号当前值',
  `cycle_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流水号是否循环',
  `cycle_method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '循环方式，YEAR：按年，MONTH：按月，DAY：按天，HOUR：按小时，MINITE：按分钟，OTHER：按传入字符变',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`part_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 304 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '编码生成规则组成表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_auto_code_part
-- ----------------------------
INSERT INTO `sys_auto_code_part` VALUES (200, 205, 2, 'SERIALNO', 'P1', '流水号', 8, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-04-26 21:13:17', 'admin', '2022-04-26 22:47:49');
INSERT INTO `sys_auto_code_part` VALUES (201, 205, 1, 'FIXCHAR', 'P0', '前缀', 4, NULL, NULL, 'ITEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-04-26 22:44:03', 'admin', '2022-08-15 15:59:19');
INSERT INTO `sys_auto_code_part` VALUES (202, 206, 1, 'FIXCHAR', 'P1', '前缀', 10, NULL, NULL, 'ITEM_TYPE_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-04-26 23:02:12', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (203, 206, 2, 'SERIALNO', 'P2', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-04-26 23:02:42', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (204, 207, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-06 21:21:04', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (205, 207, 2, 'SERIALNO', 'SERIAL', '流水号部分', 5, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-06 21:21:44', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (206, 208, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-06 22:50:38', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (207, 208, 2, 'SERIALNO', 'SERIAL', '流水号', 5, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-06 22:51:02', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (208, 209, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'WS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-07 17:49:16', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (209, 209, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-07 17:49:40', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (210, 210, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'WH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-07 22:00:17', 'admin', '2022-08-16 18:58:36');
INSERT INTO `sys_auto_code_part` VALUES (211, 210, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-07 22:00:40', 'admin', '2022-07-30 11:26:14');
INSERT INTO `sys_auto_code_part` VALUES (212, 211, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 14:50:29', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (213, 211, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 14:52:12', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (214, 212, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 18:38:29', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (215, 212, 2, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 18:38:51', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (216, 213, 1, 'FIXCHAR', 'PREFIX', '前缀', 7, NULL, NULL, 'M_TYPE_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 19:46:42', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (217, 213, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 19:47:03', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (218, 214, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 21:26:59', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (219, 214, 2, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 21:27:18', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (220, 215, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'MO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-09 11:40:23', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (222, 215, 2, 'NOWDATE', 'DATEPART', '年月日部分', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-09 11:58:57', 'admin', '2022-05-09 12:46:34');
INSERT INTO `sys_auto_code_part` VALUES (223, 215, 3, 'SERIALNO', 'SERIAL', '流水号部分', 4, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-09 11:59:31', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (224, 216, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'WS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-10 21:55:51', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (225, 216, 2, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-10 21:56:19', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (226, 217, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'TT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-11 00:22:02', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (227, 217, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-11 00:22:25', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (228, 218, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'T', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-11 22:07:44', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (229, 218, 2, 'SERIALNO', 'SERIAL', '流水号', 5, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-11 22:08:17', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (230, 219, 1, 'FIXCHAR', 'PREFIX', '前缀', 7, NULL, NULL, 'PROCESS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-12 00:10:13', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (231, 219, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-12 00:10:33', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (232, 220, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-12 23:07:01', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (233, 220, 2, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1000, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-12 23:07:23', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (234, 221, 1, 'FIXCHAR', 'PREFIX', '固定前缀', 4, NULL, NULL, 'TASK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-15 18:22:53', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (235, 221, 2, 'NOWDATE', 'YEAR', '年份', 4, 'yyyy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-15 18:23:39', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (236, 221, 3, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'YEAR', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-15 18:24:03', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (237, 222, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'I', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-17 21:57:46', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (238, 222, 2, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-17 21:58:05', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (239, 223, 1, 'FIXCHAR', 'PREFIX', '前缀', 3, NULL, NULL, 'QCT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-17 22:43:31', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (240, 223, 2, 'NOWDATE', 'YEAR', '年份', 4, 'yyyy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-17 22:44:04', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (241, 223, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'YEAR', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-17 22:44:25', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (242, 224, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'DF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-19 11:33:52', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (243, 224, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-19 11:34:11', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (244, 225, 1, 'FIXCHAR', 'PREFIX', '前缀', 3, NULL, NULL, 'IQC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-19 16:29:59', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (245, 225, 2, 'NOWDATE', 'DATE', '年月日', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-19 16:30:28', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (246, 225, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-19 16:31:00', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (247, 226, 1, 'FIXCHAR', 'PREFIX', '前缀', 1, NULL, NULL, 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-22 20:51:47', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (248, 226, 2, 'NOWDATE', 'DATE', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-05-22 20:52:10', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (249, 226, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-22 20:52:58', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (250, 227, 1, 'FIXCHAR', 'PREFIX', '固定前缀', 1, NULL, NULL, 'T', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-06 19:54:45', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (251, 227, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-06 19:55:06', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (252, 228, 1, 'FIXCHAR', 'PREFIX', '前缀', 4, NULL, NULL, 'PLAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-06 22:08:39', 'admin', '2022-07-31 16:42:59');
INSERT INTO `sys_auto_code_part` VALUES (253, 228, 2, 'NOWDATE', 'YEAR', '年', 4, 'yyyy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-06 22:08:59', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (254, 228, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'YEAR', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-06 22:09:24', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (255, 229, 1, 'FIXCHAR', 'PREFIX', '前缀', 3, NULL, NULL, 'RTV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-13 16:06:14', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (256, 229, 2, 'NOWDATE', 'DATE', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-13 16:06:42', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (257, 229, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-13 16:07:10', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (258, 230, 1, 'FIXCHAR', 'PREFIX', '固定前缀', 3, NULL, NULL, 'SUB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-16 20:28:22', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (259, 230, 2, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-16 20:28:44', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (260, 231, 1, 'FIXCHAR', 'PREFIX', '前缀', 4, NULL, NULL, 'PLAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-16 21:50:22', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (261, 231, 2, 'NOWDATE', 'YEAR', '年份', 4, 'yyyy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-06-16 21:50:43', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (262, 231, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'YEAR', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-16 21:51:07', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (263, 232, 1, 'FIXCHAR', '1', '1', 3, NULL, NULL, 'BAT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-07-14 12:02:54', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (264, 232, 2, 'NOWDATE', '2', '2', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-07-14 12:03:16', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (265, 233, 1, 'FIXCHAR', 'PREFIX', '前缀', 5, NULL, NULL, 'ISSUE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-07-17 19:32:46', 'admin', '2022-07-17 19:35:44');
INSERT INTO `sys_auto_code_part` VALUES (266, 233, 2, 'NOWDATE', 'DATE', '年月日', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-07-17 19:33:22', 'admin', '2022-07-17 19:35:57');
INSERT INTO `sys_auto_code_part` VALUES (267, 233, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-07-17 19:33:45', 'admin', '2022-07-17 19:36:05');
INSERT INTO `sys_auto_code_part` VALUES (271, 234, 1, 'INPUTCHAR', 'PREFIX', '1', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-07-30 14:20:49', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (275, 236, 1, 'FIXCHAR', '前缀', '固定字符', 2, NULL, NULL, 'IF', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-08-19 10:48:20', 'admin', '2022-08-19 14:13:30');
INSERT INTO `sys_auto_code_part` VALUES (276, 236, 2, 'NOWDATE', '后缀', '固定字段', 4, 'yyyyMMddss', NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-08-19 14:12:19', 'admin', '2022-08-19 14:19:39');
INSERT INTO `sys_auto_code_part` VALUES (277, 237, 1, 'FIXCHAR', 'PREFIX', '前缀', 4, NULL, NULL, 'IPQC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-08-29 22:07:43', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (278, 237, 2, 'NOWDATE', 'DATE', '年月日', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-08-29 22:08:18', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (279, 237, 3, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-08-29 22:08:46', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (280, 238, 1, 'FIXCHAR', 'PREFIX', '前缀', 3, NULL, NULL, 'OQC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-01 20:30:53', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (281, 238, 2, 'NOWDATE', 'DATE', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-01 20:32:11', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (282, 238, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-01 20:32:38', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (283, 239, 1, 'FIXCHAR', '001', '前缀', 5, NULL, NULL, 'PBACK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-03 23:49:07', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (284, 239, 2, 'NOWDATE', '002', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-03 23:49:31', 'admin', '2022-09-03 23:49:44');
INSERT INTO `sys_auto_code_part` VALUES (285, 239, 3, 'SERIALNO', '003', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'N', NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-03 23:50:10', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (286, 240, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'RT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-15 23:19:25', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (287, 240, 2, 'NOWDATE', 'DATE', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-15 23:19:47', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (288, 240, 3, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-15 23:20:09', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (289, 241, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'PR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-23 10:58:17', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (290, 241, 2, 'NOWDATE', 'DATE', '年月日', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-23 10:58:44', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (291, 241, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-23 10:59:06', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (292, 242, 1, 'FIXCHAR', 'PREFIX', '前缀', 3, NULL, NULL, 'REP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-28 22:01:19', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (293, 242, 2, 'NOWDATE', 'DATE', '日期', 4, 'yyyy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-09-28 22:01:39', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (294, 242, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'YEAR', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-28 22:02:00', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (295, 243, 1, 'FIXCHAR', 'PERFIX', '前缀', 2, NULL, NULL, 'PS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-10-05 19:46:02', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (296, 243, 2, 'NOWDATE', 'DATA', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-10-05 19:46:24', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (297, 243, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-10-05 19:46:48', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (298, 244, 1, 'FIXCHAR', 'PREFIX', '前缀', 2, NULL, NULL, 'RS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-10-06 21:40:42', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (299, 244, 2, 'NOWDATE', 'DATE', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-10-06 21:41:03', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (300, 244, 3, 'SERIALNO', 'SERIAL', '流水号', 3, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-10-06 21:41:22', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (301, 245, 1, 'FIXCHAR', 'PREFIX', '前缀', 4, NULL, NULL, 'PACK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-10-11 01:22:38', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (302, 245, 2, 'NOWDATE', 'DATE', '日期', 8, 'yyyyMMdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'admin', '2022-10-11 01:23:09', '', NULL);
INSERT INTO `sys_auto_code_part` VALUES (303, 245, 3, 'SERIALNO', 'SERIAL', '流水号', 4, NULL, NULL, NULL, 1, 1, NULL, 'Y', 'DAY', NULL, NULL, NULL, 0, 0, 'admin', '2022-10-11 01:23:35', '', NULL);

-- ----------------------------
-- Table structure for sys_auto_code_result
-- ----------------------------
DROP TABLE IF EXISTS `sys_auto_code_result`;
CREATE TABLE `sys_auto_code_result`  (
  `code_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `rule_id` bigint(20) NOT NULL COMMENT '规则ID',
  `gen_date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成日期时间',
  `gen_index` int(11) NULL DEFAULT NULL COMMENT '最后产生的序号',
  `last_result` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后产生的值',
  `last_serial_no` int(11) NULL DEFAULT NULL COMMENT '最后产生的流水号',
  `last_input_char` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后传入的参数',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`code_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 430 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '编码生成记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_auto_code_result
-- ----------------------------
INSERT INTO `sys_auto_code_result` VALUES (200, 205, '20220808095803', 76, 'ITEM00000076', 76, NULL, '', NULL, NULL, 0, 0, '', '2022-04-26 22:28:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (201, 206, '20220813231829', 62, 'ITEM_TYPE_0062', 62, NULL, '', NULL, NULL, 0, 0, '', '2022-04-27 10:17:19', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (202, 207, '20220802144206', 10, 'C00010', 10, NULL, '', NULL, NULL, 0, 0, '', '2022-05-06 21:22:23', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (203, 208, '20220805160743', 10, 'V00010', 10, NULL, '', NULL, NULL, 0, 0, '', '2022-05-06 22:52:14', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (204, 209, '20220805111938', 12, 'WS012', 12, NULL, '', NULL, NULL, 0, 0, '', '2022-05-07 17:54:01', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (205, 210, '20220729090506', 2, 'WH002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-05-07 22:01:31', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (206, 211, '20220729085518', 10, 'L010', 10, NULL, '', NULL, NULL, 0, 0, '', '2022-05-08 14:52:34', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (207, 212, '20220810082205', 11, 'A0011', 11, NULL, '', NULL, NULL, 0, 0, '', '2022-05-08 18:39:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (208, 213, '20220823091541', 24, 'M_TYPE_024', 24, NULL, '', NULL, NULL, 0, 0, '', '2022-05-08 19:47:31', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (209, 214, '20220819091251', 26, 'M0026', 26, NULL, '', NULL, NULL, 0, 0, '', '2022-05-08 21:38:26', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (210, 215, '20220509234547', 21, 'MO202205090021', 21, NULL, '', NULL, NULL, 0, 0, '', '2022-05-09 12:46:53', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (212, 216, '20220818103207', 34, 'WS0034', 34, NULL, '', NULL, NULL, 0, 0, '', '2022-05-10 21:59:44', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (213, 217, '20220816195823', 25, 'TT025', 25, NULL, '', NULL, NULL, 0, 0, '', '2022-05-11 00:22:40', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (214, 218, '20220804162652', 10, 'T00010', 10, NULL, '', NULL, NULL, 0, 0, '', '2022-05-11 22:35:05', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (215, 219, '20220729122036', 46, 'PROCESS046', 46, NULL, '', NULL, NULL, 0, 0, '', '2022-05-12 00:10:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (217, 220, '20220809171633', 29, 'R1028', 1028, NULL, '', NULL, NULL, 0, 0, '', '2022-05-12 23:11:24', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (219, 215, '20220514155158', 26, 'MO202205140026', 26, NULL, '', NULL, NULL, 0, 0, '', '2022-05-14 15:24:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (220, 215, '20220515134342', 5, 'MO202205150005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-05-15 13:41:34', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (221, 221, '20220927111039', 87, 'TASK20220087', 87, NULL, '', NULL, NULL, 0, 0, '', '2022-05-15 18:29:41', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (222, 222, '20220812142031', 13, 'I0013', 13, NULL, '', NULL, NULL, 0, 0, '', '2022-05-17 21:58:16', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (223, 223, '20220901212456', 42, 'QCT2022042', 42, NULL, '', NULL, NULL, 0, 0, '', '2022-05-17 22:44:37', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (224, 224, '20220819144546', 8, 'DF008', 8, NULL, '', NULL, NULL, 0, 0, '', '2022-05-19 11:37:14', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (225, 225, '20220519230422', 21, 'IQC20220519021', 21, NULL, '', NULL, NULL, 0, 0, '', '2022-05-19 16:49:49', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (226, 226, '20220522220740', 8, 'R20220522008', 8, NULL, '', NULL, NULL, 0, 0, '', '2022-05-22 20:53:25', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (227, 227, '20220814214249', 25, 'T025', 25, NULL, '', NULL, NULL, 0, 0, '', '2022-06-06 19:55:18', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (228, 228, '20220823150049', 121, 'PLAN2022121', 121, NULL, '', NULL, NULL, 0, 0, '', '2022-06-06 22:09:41', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (229, 229, '20220613162434', 2, 'RTV20220613002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-06-13 16:19:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (230, 226, '20220613164021', 1, 'R20220613001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-06-13 16:40:22', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (231, 229, '20220614161107', 3, 'RTV20220614003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-06-14 11:11:21', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (232, 226, '20220614122043', 3, 'R20220614003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-06-14 11:34:53', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (233, 230, '20220812221323', 4, 'SUB004', 4, NULL, '', NULL, NULL, 0, 0, '', '2022-06-16 20:29:01', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (234, 231, '20220901230257', 50, 'PLAN2022050', 50, NULL, '', NULL, NULL, 0, 0, '', '2022-06-16 21:51:19', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (235, 226, '20220717124046', 2, 'R20220717002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-07-17 12:40:42', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (236, 233, '20220717194751', 2, 'ISSUE20220717002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-07-17 19:36:18', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (237, 233, '20220725150745', 8, 'ISSUE20220725008', 8, NULL, '', NULL, NULL, 0, 0, '', '2022-07-25 11:16:26', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (238, 215, '20220727133912', 3, 'MO202207270003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-07-27 13:39:10', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (239, 229, '20220727135402', 3, 'RTV20220727003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-07-27 13:53:57', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (240, 215, '20220728231952', 1, 'MO202207280001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-07-28 23:19:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (241, 211, '20220729085519', 1, 'L011', 11, NULL, '', NULL, NULL, 0, 0, '', '2022-07-29 08:55:19', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (242, 211, '20220801170339', 6, 'L017', 17, NULL, '', NULL, NULL, 0, 0, '', '2022-07-29 08:55:20', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (243, 229, '20220729091253', 4, 'RTV20220729004', 4, NULL, '', NULL, NULL, 0, 0, '', '2022-07-29 08:56:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (244, 210, '20220729090520', 3, 'WH005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-07-29 09:05:11', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (245, 210, '20220801161858', 52, 'WH060', 60, NULL, '', NULL, NULL, 0, 0, '', '2022-07-29 09:05:39', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (246, 215, '20220729111608', 3, 'MO202207290003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-07-29 09:30:33', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (247, 219, '20220729122132', 3, 'PROCESS049', 49, NULL, '', NULL, NULL, 0, 0, '', '2022-07-29 12:21:02', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (248, 215, '20220730170708', 6, 'MO202207300006', 6, NULL, '', NULL, NULL, 0, 0, '', '2022-07-30 14:41:08', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (249, 226, '20220730164005', 3, 'R20220730003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-07-30 16:30:37', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (250, 219, '20220730170439', 3, 'PROCESS052', 52, NULL, '', NULL, NULL, 0, 0, '', '2022-07-30 17:02:43', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (251, 215, '20220731161554', 1, 'MO202207310001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-07-31 16:15:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (252, 226, '20220801162854', 33, 'R20220801033', 33, NULL, '', NULL, NULL, 0, 0, '', '2022-08-01 11:11:28', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (253, 215, '20220801154407', 1, 'MO202208010001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-08-01 15:44:07', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (254, 210, '20220801161857', 1, 'WH060', 60, NULL, '', NULL, NULL, 0, 0, '', '2022-08-01 16:18:57', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (255, 210, '20220801161857', 1, 'WH060', 60, NULL, '', NULL, NULL, 0, 0, '', '2022-08-01 16:18:57', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (256, 210, '20220809140656', 13, 'WH073', 73, NULL, '', NULL, NULL, 0, 0, '', '2022-08-01 16:26:43', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (257, 219, '20220801172652', 3, 'PROCESS055', 55, NULL, '', NULL, NULL, 0, 0, '', '2022-08-01 17:14:02', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (258, 215, '20220802174313', 13, 'MO202208020013', 13, NULL, '', NULL, NULL, 0, 0, '', '2022-08-02 09:27:31', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (259, 225, '20220802180956', 2, 'IQC20220802002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-08-02 17:54:11', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (260, 229, '20220802180930', 2, 'RTV20220802002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-08-02 18:01:18', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (261, 215, '20220803152758', 3, 'MO202208030003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-08-03 15:19:17', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (262, 218, '20220810105807', 7, 'T00017', 17, NULL, '', NULL, NULL, 0, 0, '', '2022-08-04 16:34:19', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (263, 209, '20220813133944', 14, 'WS026', 26, NULL, '', NULL, NULL, 0, 0, '', '2022-08-05 11:19:39', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (264, 226, '20220805131441', 4, 'R20220805004', 4, NULL, '', NULL, NULL, 0, 0, '', '2022-08-05 13:09:41', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (265, 208, '20220808161115', 4, 'V00014', 14, NULL, '', NULL, NULL, 0, 0, '', '2022-08-05 16:27:10', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (266, 205, '20220812160022', 12, 'ITEM00000088', 88, NULL, '', NULL, NULL, 0, 0, '', '2022-08-08 10:01:12', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (267, 229, '20220808105742', 2, 'RTV20220808002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-08-08 10:44:56', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (268, 207, '20220813112402', 14, 'C00024', 24, NULL, '', NULL, NULL, 0, 0, '', '2022-08-08 15:52:04', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (269, 208, '20220814231644', 10, 'V00024', 24, NULL, '', NULL, NULL, 0, 0, '', '2022-08-08 16:11:16', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (270, 219, '20220809091746', 2, 'PROCESS057', 57, NULL, '', NULL, NULL, 0, 0, '', '2022-08-09 09:17:40', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (271, 219, '20220813232649', 11, 'PROCESS068', 68, NULL, '', NULL, NULL, 0, 0, '', '2022-08-09 09:22:42', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (272, 215, '20220809173457', 7, 'MO202208090007', 7, NULL, '', NULL, NULL, 0, 0, '', '2022-08-09 12:27:34', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (273, 210, '20220809140657', 1, 'WH074', 74, NULL, '', NULL, NULL, 0, 0, '', '2022-08-09 14:06:57', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (274, 210, '20220814231315', 5, 'WH079', 79, NULL, '', NULL, NULL, 0, 0, '', '2022-08-09 14:06:58', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (275, 220, '20220813113700', 12, 'R1040', 1040, NULL, '', NULL, NULL, 0, 0, '', '2022-08-09 17:16:34', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (276, 226, '20220809174609', 2, 'R20220809002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-08-09 17:36:22', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (277, 211, '20220810082146', 5, 'L022', 22, NULL, '', NULL, NULL, 0, 0, '', '2022-08-10 08:21:39', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (278, 211, '20220927131733', 23, 'L045', 45, NULL, '', NULL, NULL, 0, 0, '', '2022-08-10 08:21:48', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (279, 212, '20220810082207', 2, 'A0013', 13, NULL, '', NULL, NULL, 0, 0, '', '2022-08-10 08:22:06', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (280, 212, '20220810083445', 4, 'A0017', 17, NULL, '', NULL, NULL, 0, 0, '', '2022-08-10 08:22:08', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (281, 218, '20220814194551', 6, 'T00023', 23, NULL, '', NULL, NULL, 0, 0, '', '2022-08-10 10:58:10', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (282, 215, '20220810141929', 7, 'MO202208100007', 7, NULL, '', NULL, NULL, 0, 0, '', '2022-08-10 12:30:21', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (283, 215, '20220811155054', 2, 'MO202208110002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-08-11 15:50:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (284, 215, '20220812163345', 36, 'MO202208120036', 36, NULL, '', NULL, NULL, 0, 0, '', '2022-08-12 11:40:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (285, 225, '20220812202238', 5, 'IQC20220812005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-08-12 13:54:10', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (286, 222, '20220822090243', 21, 'I0034', 34, NULL, '', NULL, NULL, 0, 0, '', '2022-08-12 14:20:34', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (287, 205, '20220813012808', 21, 'ITEM00000109', 109, NULL, '', NULL, NULL, 0, 0, '', '2022-08-12 16:00:23', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (288, 226, '20220812214513', 3, 'R20220812003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-08-12 16:51:18', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (289, 205, '20220813133124', 10, 'ITEM00000119', 119, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 01:28:09', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (290, 226, '20220813152657', 5, 'R20220813005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 01:47:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (291, 215, '20220813232357', 10, 'MO202208130010', 10, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 08:50:33', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (292, 207, '20220813134013', 7, 'C00031', 31, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 11:24:03', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (293, 220, '20220815153054', 12, 'R1052', 1052, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 11:37:04', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (294, 209, '20220816172957', 8, 'WS034', 34, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 13:39:46', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (295, 207, '20220813134017', 6, 'C00037', 37, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 13:40:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (296, 207, '20220815132333', 20, 'C00057', 57, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 13:40:18', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (297, 225, '20220813225044', 3, 'IQC20220813003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 15:14:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (298, 205, '20220815085111', 12, 'ITEM00000131', 131, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 21:48:41', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (299, 230, '20220815103546', 5, 'SUB009', 9, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 22:00:17', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (300, 206, '20220815143043', 4, 'ITEM_TYPE_0066', 66, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 23:18:41', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (301, 219, '20220815164921', 9, 'PROCESS077', 77, NULL, '', NULL, NULL, 0, 0, '', '2022-08-13 23:32:16', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (302, 215, '20220814231425', 5, 'MO202208140005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-08-14 10:40:29', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (303, 212, '20220818105233', 4, 'A0021', 21, NULL, '', NULL, NULL, 0, 0, '', '2022-08-14 11:19:56', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (304, 227, '20220814214249', 1, 'T026', 26, NULL, '', NULL, NULL, 0, 0, '', '2022-08-14 21:42:49', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (305, 227, '20220817195511', 33, 'T058', 58, NULL, '', NULL, NULL, 0, 0, '', '2022-08-14 21:42:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (306, 210, '20220817170523', 21, 'WH100', 100, NULL, '', NULL, NULL, 0, 0, '', '2022-08-14 23:13:23', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (307, 208, '20220816133617', 7, 'V00031', 31, NULL, '', NULL, NULL, 0, 0, '', '2022-08-14 23:16:45', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (308, 205, '20220815110633', 5, 'ITEM00000136', 136, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 08:51:13', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (309, 218, '20220823093332', 36, 'T00059', 59, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 09:53:55', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (310, 215, '20220815185151', 18, 'MO202208150018', 18, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 10:12:07', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (311, 226, '20220815143729', 8, 'R20220815008', 8, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 10:44:44', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (312, 205, '20220815140036', 3, 'ITEM00000139', 139, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 11:06:35', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (313, 225, '20220815111142', 1, 'IQC20220815001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 11:11:42', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (314, 207, '20220815132338', 6, 'C00063', 63, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 13:23:35', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (315, 207, '20220815132340', 4, 'C00067', 67, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 13:23:39', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (316, 207, '20220822100445', 95, 'C00162', 162, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 13:23:41', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (317, 230, '20220818164931', 21, 'SUB030', 30, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 14:40:05', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (318, 205, '20220817093425', 51, 'ITEM00000190', 190, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 15:43:11', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (319, 219, '20220818141839', 28, 'PROCESS105', 105, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 16:50:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (320, 229, '20220815172658', 2, 'RTV20220815002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 17:12:49', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (321, 220, '20220815200538', 2, 'R1054', 1054, NULL, '', NULL, NULL, 0, 0, '', '2022-08-15 19:52:44', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (322, 220, '20220816155501', 6, 'R1060', 1060, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 00:59:25', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (323, 226, '20220816173419', 18, 'R20220816018', 18, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 09:24:37', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (324, 215, '20220816232550', 38, 'MO202208160038', 38, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 10:18:25', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (325, 225, '20220816201615', 4, 'IQC20220816004', 4, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 10:32:14', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (326, 208, '20220816140813', 5, 'V00036', 36, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 13:36:19', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (327, 208, '20220823111843', 12, 'V00048', 48, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 14:08:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (328, 229, '20220816195356', 6, 'RTV20220816006', 6, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 17:35:14', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (329, 220, '20220818105353', 6, 'R1066', 1066, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 19:55:41', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (330, 217, '20220819093714', 7, 'TT032', 32, NULL, '', NULL, NULL, 0, 0, '', '2022-08-16 19:58:24', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (331, 215, '20220817222518', 14, 'MO202208170014', 14, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 08:47:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (332, 209, '20220820094836', 17, 'WS051', 51, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 08:55:43', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (333, 229, '20220817091138', 2, 'RTV20220817002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 09:11:36', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (334, 205, '20220818115033', 32, 'ITEM00000222', 222, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 09:34:26', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (335, 206, '20220822091118', 19, 'ITEM_TYPE_0085', 85, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 11:14:35', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (336, 226, '20220817205507', 22, 'R20220817022', 22, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 14:03:20', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (337, 225, '20220817203604', 5, 'IQC20220817005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 15:57:28', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (338, 210, '20220817170524', 1, 'WH101', 101, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 17:05:24', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (339, 233, '20220817231312', 6, 'ISSUE20220817006', 6, NULL, '', NULL, NULL, 0, 0, '', '2022-08-17 23:02:13', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (340, 226, '20220818211659', 12, 'R20220818012', 12, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 09:30:26', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (341, 210, '20220822145345', 35, 'WH136', 136, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 09:39:40', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (342, 225, '20220818165508', 12, 'IQC20220818012', 12, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 09:41:21', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (343, 229, '20220818180526', 7, 'RTV20220818007', 7, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 10:10:40', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (344, 215, '20220818230633', 77, 'MO202208180077', 77, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 10:33:04', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (345, 220, '20220819091351', 17, 'R1083', 1083, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 10:53:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (346, 212, '20220820170019', 5, 'A0026', 26, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 11:07:01', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (347, 216, '20220818164828', 11, 'WS0045', 45, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 11:39:49', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (348, 227, '20220818160823', 8, 'T066', 66, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 13:40:18', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (349, 205, '20220818134540', 1, 'ITEM00000223', 223, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 13:45:40', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (350, 205, '20220818141238', 3, 'ITEM00000226', 226, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 14:04:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (351, 205, '20220819101459', 33, 'ITEM00000259', 259, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 14:13:35', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (352, 219, '20220818144345', 2, 'PROCESS107', 107, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 14:43:21', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (353, 227, '20220818175718', 3, 'T069', 69, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 16:08:27', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (354, 216, '20220927110257', 73, 'WS0118', 118, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 16:48:30', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (355, 230, '20220823102743', 18, 'SUB048', 48, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 16:56:44', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (356, 227, '20220820110823', 7, 'T076', 76, NULL, '', NULL, NULL, 0, 0, '', '2022-08-18 17:57:45', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (357, 219, '20220927100835', 87, 'PROCESS194', 194, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 00:22:13', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (358, 226, '20220819211451', 22, 'R20220819022', 22, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 08:46:50', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (359, 215, '20220819211957', 44, 'MO202208190044', 44, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 08:47:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (360, 225, '20220819213654', 41, 'IQC20220819041', 41, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 08:49:46', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (361, 214, '20220821094813', 15, 'M0041', 41, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 09:12:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (362, 220, '20220819173023', 13, 'R1096', 1096, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 09:13:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (363, 217, '20220821133355', 15, 'TT047', 47, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 09:37:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (364, 236, '20220819111319', 3, '32323232323232323232323232ITEM_TYPE_', 14, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 11:13:16', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (365, 236, '20220819142129', 64, 'IF2022081929', 72, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 11:16:04', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (366, 236, '20220819145515', 12, 'IF2022081915', 1094, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 14:21:30', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (367, 236, '20220819205101', 16, 'IF2022081901', 21, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 16:18:20', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (368, 236, '20220823103219', 76, 'IF2022082319', 48, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 20:51:03', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (369, 229, '20220819205514', 1, 'RTV20220819001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 20:55:14', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (370, 220, '20220927100854', 26, 'R1122', 1122, NULL, '', NULL, NULL, 0, 0, '', '2022-08-19 21:12:27', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (371, 209, '20220820160129', 7, 'WS058', 58, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 09:48:37', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (372, 215, '20220820222241', 39, 'MO202208200039', 39, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 10:00:14', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (373, 226, '20220820215444', 21, 'R20220820021', 21, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 10:13:38', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (374, 227, '20220820110825', 2, 'T078', 78, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 11:08:24', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (375, 227, '20220820220731', 17, 'T095', 95, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 11:08:26', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (376, 225, '20220820160353', 3, 'IQC20220820003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 13:27:37', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (377, 209, '20220927105615', 22, 'WS080', 80, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 16:01:30', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (378, 227, '20220823083639', 10, 'T105', 105, NULL, '', NULL, NULL, 0, 0, '', '2022-08-20 22:07:33', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (379, 215, '20220821192938', 5, 'MO202208210005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-08-21 00:51:58', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (380, 214, '20220821094815', 1, 'M0042', 42, NULL, '', NULL, NULL, 0, 0, '', '2022-08-21 09:48:15', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (381, 212, '20221005194759', 8, 'A0034', 34, NULL, '', NULL, NULL, 0, 0, '', '2022-08-21 13:28:13', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (382, 214, '20220823173340', 13, 'M0055', 55, NULL, '', NULL, NULL, 0, 0, '', '2022-08-21 13:31:39', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (383, 217, '20220823205336', 11, 'TT058', 58, NULL, '', NULL, NULL, 0, 0, '', '2022-08-21 13:45:53', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (384, 226, '20220821225220', 7, 'R20220821007', 7, NULL, '', NULL, NULL, 0, 0, '', '2022-08-21 15:16:36', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (385, 215, '20220822234830', 19, 'MO202208220019', 19, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 08:38:59', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (386, 226, '20220822192029', 25, 'R20220822025', 25, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 08:48:16', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (387, 219, '20220822090456', 1, 'PROCESS140', 140, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 09:04:56', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (388, 219, '20220822090503', 1, 'PROCESS152', 152, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 09:05:03', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (389, 206, '20220927101231', 10, 'ITEM_TYPE_0095', 95, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 09:11:56', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (390, 229, '20220822155824', 7, 'RTV20220822007', 7, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 09:13:49', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (391, 207, '20220822100442', 1, 'C00153', 153, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 10:04:42', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (392, 207, '20220822102252', 5, 'C00167', 167, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 10:05:02', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (393, 207, '20220927102713', 32, 'C00199', 199, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 10:22:53', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (394, 210, '20220823083833', 3, 'WH139', 139, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 14:53:46', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (395, 225, '20220822154721', 1, 'IQC20220822001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-08-22 15:47:21', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (396, 226, '20220823203609', 20, 'R20220823020', 20, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 00:41:07', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (397, 215, '20220823200644', 52, 'MO202208230052', 52, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 07:51:01', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (398, 210, '20220927131638', 18, 'WH157', 157, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 08:38:34', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (399, 229, '20220823205401', 5, 'RTV20220823005', 5, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 08:43:32', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (400, 227, '20220823184025', 12, 'T117', 117, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 08:58:07', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (401, 225, '20220823192837', 7, 'IQC20220823007', 7, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 09:47:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (402, 236, '20220823173612', 17, 'IF2022082312', 51, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 10:41:04', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (403, 208, '20220823181426', 10, 'V00058', 58, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 15:09:35', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (404, 236, '20220823192141', 10, 'IF2022082341', 154, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 19:08:33', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (405, 236, '20220927102019', 16, 'IF2022092719', 95, NULL, '', NULL, NULL, 0, 0, '', '2022-08-23 19:21:45', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (406, 215, '20220828084935', 6, 'MO202208280006', 6, NULL, '', NULL, NULL, 0, 0, '', '2022-08-28 08:36:52', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (407, 237, '20220829224554', 4, 'IPQC202208290004', 4, NULL, '', NULL, NULL, 0, 0, '', '2022-08-29 22:09:01', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (408, 225, '20220831211548', 3, 'IQC20220831003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-08-31 20:57:09', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (409, 238, '20220901212705', 4, 'OQC20220901004', 4, NULL, '', NULL, NULL, 0, 0, '', '2022-09-01 20:35:11', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (410, 226, '20220902104056', 4, 'R20220902004', 4, NULL, '', NULL, NULL, 0, 0, '', '2022-09-02 10:36:07', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (411, 233, '20220902104302', 1, 'ISSUE20220902001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-09-02 10:43:02', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (412, 239, '20220904223417', 10, 'PBACK20220904010', 10, NULL, '', NULL, NULL, 0, 0, '', '2022-09-03 23:50:32', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (413, 237, '20220909211934', 1, 'IPQC202209090001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-09-09 21:19:38', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (414, 226, '20220910111422', 2, 'R20220910002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-09-10 10:57:18', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (415, 226, '20220912232033', 1, 'R20220912001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-09-12 23:20:33', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (416, 226, '20220913223354', 1, 'R20220913001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-09-13 22:33:54', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (417, 233, '20220913225049', 2, 'ISSUE20220913002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-09-13 22:35:02', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (418, 240, '20220915232017', 1, 'RT202209150001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-09-15 23:20:17', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (419, 226, '20220916212404', 1, 'R20220916001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-09-16 21:24:06', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (420, 241, '20220923105914', 1, 'PR20220923001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-09-23 10:59:14', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (421, 215, '20220926212817', 2, 'MO202209260002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-09-26 21:27:05', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (422, 215, '20220927112106', 2, 'MO202209270002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-09-27 10:25:50', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (423, 226, '20220927131833', 2, 'R20220927002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-09-27 13:15:57', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (424, 242, '20220928220423', 2, 'REP2022002', 2, NULL, '', NULL, NULL, 0, 0, '', '2022-09-28 22:02:26', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (425, 243, '20221005194837', 3, 'PS20221005003', 3, NULL, '', NULL, NULL, 0, 0, '', '2022-10-05 19:46:57', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (426, 233, '20221006132703', 1, 'ISSUE20221006001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-10-06 13:27:03', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (427, 244, '20221006214134', 1, 'RS20221006001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-10-06 21:41:34', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (428, 245, '20221011233528', 7, 'PACK202210110007', 7, NULL, '', NULL, NULL, 0, 0, '', '2022-10-11 01:23:50', '', NULL);
INSERT INTO `sys_auto_code_result` VALUES (429, 240, '20221015152217', 1, 'RT202210150001', 1, NULL, '', NULL, NULL, 0, 0, '', '2022-10-15 15:22:17', '', NULL);

-- ----------------------------
-- Table structure for sys_auto_code_rule
-- ----------------------------
DROP TABLE IF EXISTS `sys_auto_code_rule`;
CREATE TABLE `sys_auto_code_rule`  (
  `rule_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '规则ID',
  `rule_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规则编码',
  `rule_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规则名称',
  `rule_desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `max_length` int(11) NULL DEFAULT NULL COMMENT '最大长度',
  `is_padded` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否补齐',
  `padded_char` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '补齐字符',
  `padded_method` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'L' COMMENT '补齐方式',
  `enable_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否启用',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `attr1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段1',
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段2',
  `attr3` int(11) NULL DEFAULT 0 COMMENT '预留字段3',
  `attr4` int(11) NULL DEFAULT 0 COMMENT '预留字段4',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`rule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 246 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '编码生成规则表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_auto_code_rule
-- ----------------------------
INSERT INTO `sys_auto_code_rule` VALUES (206, 'ITEM_TYPE_CODE', '物料分类编码', NULL, 14, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-04-26 23:01:09', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (207, 'CLIENT_CODE', '客户编码规则', NULL, 6, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-06 21:20:29', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (208, 'VENDOR_CODE', '供应商编码规则', NULL, 6, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-06 22:50:13', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (209, 'WORKSHOP_CODE', '车间编码生成规则', NULL, 5, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-07 17:48:52', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (210, 'WAREHOUSE_CODE', '仓库编码规则', NULL, 5, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-07 21:59:51', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (211, 'LOCATION_CODE', '库区编码生成规则', NULL, 4, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 14:49:56', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (212, 'AREA_CODE', '库位编码规则', NULL, 5, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 18:38:08', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (213, 'MACHINERY_TYPE_CODE', '设备类型编码规则', NULL, 10, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 19:46:09', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (214, 'MACHINERY_CODE', '设备编码规则', NULL, 13, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-08 21:26:39', 'admin', '2022-08-23 09:15:17');
INSERT INTO `sys_auto_code_rule` VALUES (215, 'WORKORDER_CODE', '生产工单编码规则1', NULL, 14, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-09 11:39:59', 'admin', '2022-08-20 09:12:40');
INSERT INTO `sys_auto_code_rule` VALUES (216, 'WORKSTATION_CODE', '工作站编码规则', NULL, 6, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-10 21:55:24', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (217, 'TOOL_TYPE_CODE', '工装夹具类型编码', NULL, 5, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-11 00:21:37', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (218, 'TOOL_CODE', '工装夹具编码规则', NULL, 6, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-11 22:07:17', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (219, 'PROCESS_CODE', '工序编码规则', NULL, 10, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-12 00:09:45', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (220, 'ROUTE_CODE', '工艺流程编码规则', NULL, 5, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-12 23:06:36', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (221, 'TASK_CODE', '生产任务编码规则', NULL, 12, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-15 18:22:29', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (222, 'QC_INDEX_CODE', '检测项编码规则', NULL, 5, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-17 21:57:23', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (223, 'QC_TEMPLATE_CODE', '检测模板编码规则', NULL, 10, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-17 22:43:08', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (224, 'DEFECT_CODE', '常见缺陷编码', NULL, 5, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-19 11:33:27', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (225, 'QC_IQC_CODE', '来料检验单编码规则', NULL, 14, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-19 16:28:07', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (226, 'ITEMRECPT_CODE', '物料入库单编码规则', NULL, 12, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-05-22 20:51:29', 'admin', '2022-05-22 20:53:12');
INSERT INTO `sys_auto_code_rule` VALUES (227, 'CAL_TEAM_CODE', '班组编码规则', NULL, 4, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-06 19:54:22', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (228, 'CAL_PLAN_CODE', '排班计划编号', NULL, 11, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-06 22:08:10', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (229, 'WM_RTVENDOR_CODE', '供应商退货单编码', NULL, 14, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-13 15:48:07', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (230, 'SUBJECT_CODE', '设备点检保养项目编码', NULL, 6, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-16 20:27:54', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (231, 'CHECKPLAN_CODE', '点检编码规则', NULL, 11, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-06-16 21:50:00', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (232, 'BATCH_CODE', '批次规则', NULL, 11, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-07-14 12:02:10', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (233, 'ISSUE_CODE', '生产领料单编码', NULL, 16, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-07-17 19:32:10', 'admin', '2022-07-17 19:32:57');
INSERT INTO `sys_auto_code_rule` VALUES (234, '1', '2', NULL, 1, 'Y', '3', 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-07-29 16:34:20', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (236, 'ITEM_CODE', '物料规则', NULL, 10, 'N', '32', 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-08-19 10:44:20', 'admin', '2022-08-19 11:26:02');
INSERT INTO `sys_auto_code_rule` VALUES (237, 'IPQC_CODE', '过程检验单编码', NULL, 16, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-08-29 22:07:13', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (238, 'OQC_CODE', '出货编码规则', NULL, 14, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-01 20:30:31', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (239, 'PBACK_CODE', '生产退料单编码', '生产退料单编码', 16, 'N', '0', 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-03 23:47:11', 'admin', '2022-09-03 23:47:57');
INSERT INTO `sys_auto_code_rule` VALUES (240, 'RTISSUE_CODE', '生产退库单编号规则', NULL, 14, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-15 23:18:40', 'admin', '2022-09-15 23:19:04');
INSERT INTO `sys_auto_code_rule` VALUES (241, 'PRODUCTRECPT_CODE', '产品入库单编码规则', NULL, 13, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-23 10:57:47', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (242, 'REPAIR_CODE', '维修工单编号规则', NULL, 10, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-09-28 21:59:54', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (243, 'PRODUCTSALSE_CODE', '销售出库单编号', NULL, 13, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-10-05 19:45:35', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (244, 'RTSALSE_CODE', '销售退货单编码规则', NULL, 13, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-10-06 21:40:18', '', NULL);
INSERT INTO `sys_auto_code_rule` VALUES (245, 'PACKAGE_CODE', '装箱单编码规则', NULL, 16, 'N', NULL, 'L', 'Y', NULL, NULL, NULL, 0, 0, 'admin', '2022-10-11 01:22:08', '', NULL);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2023-03-01 09:02:44', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2023-03-01 09:02:44', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2023-03-01 09:02:44', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', 'admin', '2023-03-01 09:02:44', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2023-03-01 09:02:44', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (6, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2023-03-01 09:02:44', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (30, 0, '否', '0', 'shenbao_state', NULL, 'warning', 'N', '0', 'admin', '2023-03-10 08:04:07', 'admin', '2023-03-10 08:04:35', NULL);
INSERT INTO `sys_dict_data` VALUES (31, 1, '是', '1', 'shenbao_state', NULL, 'success', 'N', '0', 'admin', '2023-03-10 08:04:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (32, 0, '直连设备', '0', 'device_type', NULL, 'default', 'N', '0', 'admin', '2023-03-10 17:28:47', '', NULL, '0-直连设备，1-网关子设备，2-网关设备');
INSERT INTO `sys_dict_data` VALUES (33, 1, '网关子设备', '1', 'device_type', NULL, 'default', 'N', '0', 'admin', '2023-03-10 17:28:58', '', NULL, '0-直连设备，1-网关子设备，2-网关设备');
INSERT INTO `sys_dict_data` VALUES (34, 2, '网关设备', '2', 'device_type', NULL, 'default', 'N', '0', 'admin', '2023-03-10 17:29:27', '', NULL, '0-直连设备，1-网关子设备，2-网关设备');
INSERT INTO `sys_dict_data` VALUES (35, 0, '以太网', '0', 'network_way', NULL, 'default', 'N', '0', 'admin', '2023-03-11 09:08:57', '', NULL, '连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）');
INSERT INTO `sys_dict_data` VALUES (36, 1, 'wifi', '1', 'network_way', NULL, 'default', 'N', '0', 'admin', '2023-03-11 09:09:14', '', NULL, '连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\n');
INSERT INTO `sys_dict_data` VALUES (37, 2, '串口', '2', 'network_way', NULL, 'default', 'N', '0', 'admin', '2023-03-11 09:09:31', '', NULL, '连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\n');
INSERT INTO `sys_dict_data` VALUES (38, 3, '蜂窝（2G / 3G / 4G / 5G）', '3', 'network_way', NULL, 'default', 'N', '0', 'admin', '2023-03-11 09:09:42', '', NULL, '连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\n');
INSERT INTO `sys_dict_data` VALUES (39, 0, '不存储', '0', 'storage_strategy', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:49:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (40, 1, 'ElasticSearch-行式存储', '1', 'storage_strategy', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:49:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (41, 2, 'ElasticSearch-列式存储', '2', 'storage_strategy', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:49:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (42, 3, 'Influxdb-行式存储', '3', 'storage_strategy', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:49:48', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (43, 4, 'Influxdb-列式存储', '4', 'storage_strategy', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:49:56', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (44, 5, 'TDengine-行式存储', '5', 'storage_strategy', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:50:03', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (45, 6, 'TDengine-行式存储', '6', 'storage_strategy', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:50:12', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (46, 0, 'int(整数型)', 'int', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:57:08', '', NULL, 'int(整数型)');
INSERT INTO `sys_dict_data` VALUES (47, 1, 'long(长整数型)', 'long', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:58:07', '', NULL, 'long(长整数型)');
INSERT INTO `sys_dict_data` VALUES (48, 2, 'float(单精度浮点型)', 'float', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:58:22', 'admin', '2023-06-01 11:25:54', 'float(单精度浮点型)\n');
INSERT INTO `sys_dict_data` VALUES (49, 3, 'double(双精度浮点数)', 'double', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:58:40', 'admin', '2023-05-05 13:59:11', 'double(双精度浮点数)');
INSERT INTO `sys_dict_data` VALUES (50, 4, 'text(字符串)', 'text', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:59:05', 'admin', '2023-05-05 13:59:15', 'text(字符串)');
INSERT INTO `sys_dict_data` VALUES (51, 5, 'boolean(布尔型)', 'boolean', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:59:32', 'admin', '2023-06-01 15:35:15', 'bool(布尔型)');
INSERT INTO `sys_dict_data` VALUES (52, 6, 'date(时间型)', 'date', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 13:59:54', '', NULL, 'date(时间型)');
INSERT INTO `sys_dict_data` VALUES (53, 7, 'enum(枚举)', 'enum', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:00:11', '', NULL, 'enum(枚举)');
INSERT INTO `sys_dict_data` VALUES (54, 8, 'array(数组)', 'array(数组)', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:00:28', 'admin', '2023-05-05 14:00:32', 'array(数组)');
INSERT INTO `sys_dict_data` VALUES (55, 9, 'object(结构体)', 'object', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:00:51', '', NULL, 'object(结构体)');
INSERT INTO `sys_dict_data` VALUES (56, 10, 'file(文件)', 'file', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:01:11', '', NULL, 'file(文件)');
INSERT INTO `sys_dict_data` VALUES (57, 11, 'password(密码)', 'password', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:01:43', '', NULL, 'password(密码)');
INSERT INTO `sys_dict_data` VALUES (58, 12, 'geoPoint(地理位置)', 'geoPoint', 'data_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:02:01', '', NULL, 'geoPoint(地理位置)');
INSERT INTO `sys_dict_data` VALUES (59, 0, '否', '0', 'whether_or_not', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:03:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (60, 1, '是', '1', 'whether_or_not', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:03:07', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (61, 0, '设备', '0', 'attribute_source', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:04:27', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (62, 1, '手动', '1', 'attribute_source', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:04:39', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (63, 2, '规则', '2', 'attribute_source', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:04:49', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (65, 0, '一般', '0', 'alarm_level', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:08:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (66, 1, '较重', '1', 'alarm_level', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:08:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (67, 2, '严重', '2', 'alarm_level', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:08:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (68, 0, '在线', '0', 'device_state', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:10:27', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (69, 1, '离线', '1', 'device_state', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:10:33', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (70, 0, 'script', 'script', 'protocol_type', NULL, 'default', 'N', '0', 'admin', '2023-05-05 14:17:00', '', NULL, 'script');
INSERT INTO `sys_dict_data` VALUES (71, 0, '一般报警', '0', 'alarm_level_conf', NULL, 'info', 'N', '0', 'admin', '2023-05-07 16:54:08', 'admin', '2023-05-07 16:54:16', NULL);
INSERT INTO `sys_dict_data` VALUES (72, 1, '较重报警', '1', 'alarm_level_conf', NULL, 'warning', 'N', '0', 'admin', '2023-05-07 16:54:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (73, 2, '严重报警', '2', 'alarm_level_conf', NULL, 'default', 'N', '0', 'admin', '2023-05-07 16:54:39', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (74, 0, '百分比(%)', '0', 'common_unit', NULL, 'default', 'N', '0', 'admin', '2023-05-07 17:17:26', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (75, 1, '长度单位:纳米(nm)', '1', 'common_unit', NULL, 'default', 'N', '0', 'admin', '2023-05-07 17:17:56', '', NULL, '长度单位:纳米(nm)');
INSERT INTO `sys_dict_data` VALUES (76, 2, '长度单位:微米(μm)', '2', 'common_unit', NULL, 'default', 'N', '0', 'admin', '2023-05-07 17:18:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (77, 3, '长度单位:毫米(mm)', '3', 'common_unit', NULL, 'default', 'N', '0', 'admin', '2023-05-07 17:18:53', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (78, 4, '长度单位:厘米(cm)', '4', 'common_unit', NULL, 'default', 'N', '0', 'admin', '2023-05-07 17:19:24', '', NULL, '长度单位:厘米(cm)');
INSERT INTO `sys_dict_data` VALUES (79, 5, '长度单位:米(m)', '5', 'common_unit', NULL, 'default', 'N', '0', 'admin', '2023-05-07 17:19:41', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (80, 6, '长度单位:千米(km)', '6', 'common_unit', NULL, 'default', 'N', '0', 'admin', '2023-05-07 17:19:57', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (81, 0, '只读', 'r', 'read_write_type', NULL, 'default', 'N', '0', 'admin', '2023-05-10 13:35:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (82, 1, '读写', 'rw', 'read_write_type', NULL, 'default', 'N', '0', 'admin', '2023-05-10 13:35:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (83, 0, '是', 'true', 'true_false', NULL, 'default', 'N', '0', 'admin', '2023-05-10 13:37:10', '', NULL, 'true');
INSERT INTO `sys_dict_data` VALUES (84, 1, '否', 'false', 'true_false', NULL, 'default', 'N', '0', 'admin', '2023-05-10 13:37:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (85, 1, '启用', '1', 'device_status', NULL, 'success', 'N', '0', 'admin', '2023-05-12 11:18:42', 'admin', '2023-05-12 15:33:24', NULL);
INSERT INTO `sys_dict_data` VALUES (86, 0, '禁用', '0', 'device_status', NULL, 'danger', 'N', '0', 'admin', '2023-05-12 11:18:55', 'admin', '2023-05-12 11:19:02', NULL);
INSERT INTO `sys_dict_data` VALUES (87, 0, 'URL(链接)', 'url', 'file_type', NULL, 'default', 'N', '0', 'admin', '2023-06-01 17:15:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (88, 1, 'Base64(Base64编码)', 'base64', 'file_type', NULL, 'default', 'N', '0', 'admin', '2023-06-01 17:15:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (89, 3, 'binary', 'binary', 'file_type', NULL, 'default', 'N', '0', 'admin', '2023-06-01 17:15:46', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '是否启用', 'shenbao_state', '0', 'admin', '2023-03-10 08:03:43', 'admin', '2023-03-11 08:14:38', '0-否，1-是');
INSERT INTO `sys_dict_type` VALUES (12, '设备类型', 'device_type', '0', 'admin', '2023-03-10 17:28:16', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (13, '设备状态', 'device_status', '0', 'admin', '2023-03-10 17:44:27', 'admin', '2023-03-11 08:15:16', '（0正常 1停用）');
INSERT INTO `sys_dict_type` VALUES (14, '连网方式', 'network_way', '0', 'admin', '2023-03-11 08:53:44', '', NULL, '连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）');
INSERT INTO `sys_dict_type` VALUES (15, '存储策略', 'storage_strategy', '0', 'admin', '2023-05-05 13:48:46', '', NULL, '存储策略');
INSERT INTO `sys_dict_type` VALUES (16, '数据类型', 'data_type', '0', 'admin', '2023-05-05 13:53:01', 'admin', '2023-05-05 13:53:07', '数据类型');
INSERT INTO `sys_dict_type` VALUES (17, '是否', 'whether_or_not', '0', 'admin', '2023-05-05 14:02:47', 'admin', '2023-05-05 14:06:32', '是否 0-否、1-是');
INSERT INTO `sys_dict_type` VALUES (18, '属性来源', 'attribute_source', '0', 'admin', '2023-05-05 14:04:12', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (19, '预警级别', 'alarm_level', '0', 'admin', '2023-05-05 14:08:10', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (20, '设备运行状态', 'device_state', '0', 'admin', '2023-05-05 14:10:11', '', NULL, '设备运行状态');
INSERT INTO `sys_dict_type` VALUES (21, '协议类型', 'protocol_type', '0', 'admin', '2023-05-05 14:16:34', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (22, '告警级别配置', 'alarm_level_conf', '0', 'admin', '2023-05-07 16:53:19', '', NULL, '告警级别配置');
INSERT INTO `sys_dict_type` VALUES (23, '常用单位', 'common_unit', '0', 'admin', '2023-05-07 17:16:45', '', NULL, '常用单位');
INSERT INTO `sys_dict_type` VALUES (24, '读写类型', 'read_write_type', '0', 'admin', '2023-05-10 13:35:11', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (25, '是否', 'true_false', '0', 'admin', '2023-05-10 13:36:39', '', NULL, '是（true），否（false）');
INSERT INTO `sys_dict_type` VALUES (26, '文件类型', 'file_type', '0', 'admin', '2023-06-01 17:14:23', '', NULL, '文件类型');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2023-03-01 09:02:44', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 171 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-01 09:29:34');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-10 08:01:24');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '用户不存在/密码错误', '2023-03-10 10:11:11');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2023-03-10 10:11:11');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '用户不存在/密码错误', '2023-03-10 10:11:15');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误2次', '2023-03-10 10:11:15');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-10 10:11:23');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '用户不存在/密码错误', '2023-03-10 13:42:28');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2023-03-10 13:42:28');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-10 13:42:36');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-10 15:01:45');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-10 16:25:13');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-10 17:24:37');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-11 08:05:34');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-11 10:49:48');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-03-15 13:34:11');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-05 13:38:51');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-05-07 14:28:46');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-07 15:44:12');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-07 15:44:26');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-07 15:44:38');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-05-07 16:36:39');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码已失效', '2023-05-07 16:51:46');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-07 16:51:53');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2023-05-07 19:59:43');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-05-07 19:59:46');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2023-05-07 20:42:04');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-08 08:16:25');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-08 11:37:15');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-05-08 13:32:57');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-10 08:16:36');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-10 09:32:02');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-10 11:27:24');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-10 13:33:53');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-10 14:31:24');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-12 07:52:09');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-12 10:25:07');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-12 13:19:54');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-12 14:53:25');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-12 15:32:37');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-15 14:03:29');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-15 15:41:28');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-16 08:08:47');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-16 08:43:35');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-16 10:12:46');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-31 10:05:58');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-31 10:48:55');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-31 15:42:50');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-31 19:03:34');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-05-31 19:27:11');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-01 08:48:24');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-01 12:23:28');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-01 13:38:20');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-01 14:19:59');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-01 15:04:56');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-02 10:40:52');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-02 13:27:49');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-05 15:25:27');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-06 14:23:36');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-12 13:31:37');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '1', '验证码错误', '2023-06-12 15:53:09');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-12 15:53:17');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-12 15:53:20');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-12 15:53:32');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-12 16:28:42');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-12 17:20:07');
INSERT INTO `sys_logininfor` VALUES (166, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-12 17:20:11');
INSERT INTO `sys_logininfor` VALUES (167, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-26 09:22:00');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-26 11:37:15');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-06-26 13:43:31');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Windows 10', '0', '登录成功', '2023-07-06 11:17:36');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1112 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 100, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2023-03-01 09:02:44', 'admin', '2023-03-10 13:43:22', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 102, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2023-03-01 09:02:44', 'admin', '2023-03-10 13:43:31', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 101, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2023-03-01 09:02:44', 'admin', '2023-03-10 13:43:38', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '物联网官网', 0, 103, 'http://www.shenbaoiot.com', NULL, '', 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2023-03-01 09:02:44', 'admin', '2023-03-10 13:45:03', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2023-03-01 09:02:44', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2023-03-01 09:02:44', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2023-03-01 09:02:44', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2023-03-01 09:02:44', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2023-03-01 09:02:44', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2023-03-01 09:02:44', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2023-03-01 09:02:44', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2023-03-01 09:02:44', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2023-03-01 09:02:44', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2023-03-01 09:02:44', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2023-03-01 09:02:44', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2023-03-01 09:02:44', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2023-03-01 09:02:44', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2023-03-01 09:02:44', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2023-03-01 09:02:44', '', NULL, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2023-03-01 09:02:44', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2023-03-01 09:02:44', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2023-03-01 09:02:44', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2023-03-01 09:02:44', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2023-03-01 09:02:44', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '主数据', 0, 0, 'main', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'system', 'admin', '2023-03-10 08:11:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1068, '设备管理', 0, 1, 'dv', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'dict', 'admin', '2023-03-10 08:23:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1069, '项目管理', 1061, 1, 'project', 'basics/project/index', NULL, 1, 0, 'C', '0', '0', 'basics:project:list', 'cascader', 'admin', '2023-03-10 10:23:39', 'admin', '2023-03-11 08:46:14', '项目管理菜单');
INSERT INTO `sys_menu` VALUES (1070, '项目管理查询', 1069, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:project:query', '#', 'admin', '2023-03-10 10:23:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1071, '项目管理新增', 1069, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:project:add', '#', 'admin', '2023-03-10 10:23:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1072, '项目管理修改', 1069, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:project:edit', '#', 'admin', '2023-03-10 10:23:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1073, '项目管理删除', 1069, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:project:remove', '#', 'admin', '2023-03-10 10:23:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1074, '项目管理导出', 1069, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:project:export', '#', 'admin', '2023-03-10 10:23:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1081, '车间管理', 1061, 1, 'workshop', 'basics/workshop/index', NULL, 1, 0, 'C', '0', '0', 'basics:workshop:list', 'cascader', 'admin', '2023-03-10 10:53:57', 'admin', '2023-03-11 08:46:21', '车间管理菜单');
INSERT INTO `sys_menu` VALUES (1082, '车间管理查询', 1081, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:workshop:query', '#', 'admin', '2023-03-10 10:53:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1083, '车间管理新增', 1081, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:workshop:add', '#', 'admin', '2023-03-10 10:53:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1084, '车间管理修改', 1081, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:workshop:edit', '#', 'admin', '2023-03-10 10:53:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1085, '车间管理删除', 1081, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:workshop:remove', '#', 'admin', '2023-03-10 10:53:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1086, '车间管理导出', 1081, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'basics:workshop:export', '#', 'admin', '2023-03-10 10:53:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1087, '告警中心', 0, 4, '/ware', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'eye-open', 'admin', '2023-03-10 15:02:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1088, '设备类型管理', 1068, 1, 'classified', 'dev/classified/index', NULL, 1, 0, 'C', '0', '0', 'dev:classified:list', 'cascader', 'admin', '2023-03-11 08:09:52', 'admin', '2023-03-11 08:44:05', '设备类型菜单');
INSERT INTO `sys_menu` VALUES (1089, '设备类型查询', 1088, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:classified:query', '#', 'admin', '2023-03-11 08:09:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1090, '设备类型新增', 1088, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:classified:add', '#', 'admin', '2023-03-11 08:09:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1091, '设备类型修改', 1088, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:classified:edit', '#', 'admin', '2023-03-11 08:09:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1092, '设备类型删除', 1088, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:classified:remove', '#', 'admin', '2023-03-11 08:09:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1093, '设备类型导出', 1088, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:classified:export', '#', 'admin', '2023-03-11 08:09:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1094, '产品管理', 1068, 1, 'product', 'dev/product/index', NULL, 1, 0, 'C', '0', '0', 'dev:product:list', 'cascader', 'admin', '2023-03-11 11:00:26', 'admin', '2023-03-11 11:09:45', '产品管理菜单');
INSERT INTO `sys_menu` VALUES (1095, '产品管理查询', 1094, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:product:query', '#', 'admin', '2023-03-11 11:00:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1096, '产品管理新增', 1094, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:product:add', '#', 'admin', '2023-03-11 11:00:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1097, '产品管理修改', 1094, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:product:edit', '#', 'admin', '2023-03-11 11:00:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1098, '产品管理删除', 1094, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:product:remove', '#', 'admin', '2023-03-11 11:00:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1099, '产品管理导出', 1094, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:product:export', '#', 'admin', '2023-03-11 11:00:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1106, '设备管理', 1068, 1, 'devinfo', 'dev/devinfo/index', NULL, 1, 0, 'C', '0', '0', 'dev:devinfo:list', 'nested', 'admin', '2023-05-12 16:08:59', 'admin', '2023-05-12 16:30:26', '设备管理菜单');
INSERT INTO `sys_menu` VALUES (1107, '设备管理查询', 1106, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:devinfo:query', '#', 'admin', '2023-05-12 16:08:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1108, '设备管理新增', 1106, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:devinfo:add', '#', 'admin', '2023-05-12 16:08:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1109, '设备管理修改', 1106, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:devinfo:edit', '#', 'admin', '2023-05-12 16:08:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1110, '设备管理删除', 1106, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:devinfo:remove', '#', 'admin', '2023-05-12 16:08:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1111, '设备管理导出', 1106, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'dev:devinfo:export', '#', 'admin', '2023-05-12 16:08:59', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2023-03-01 09:02:44', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2023-03-01 09:02:44', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(20) NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 339 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_project\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:01:36', 92);
INSERT INTO `sys_oper_log` VALUES (2, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"状态\",\"dictType\":\"shenbao_state\",\"params\":{},\"remark\":\"0-否，1-是\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:03:43', 8);
INSERT INTO `sys_oper_log` VALUES (3, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"cssClass\":\"\",\"default\":false,\"dictLabel\":\"否\",\"dictSort\":0,\"dictType\":\"shenbao_state\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:04:07', 9);
INSERT INTO `sys_oper_log` VALUES (4, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"是\",\"dictSort\":1,\"dictType\":\"shenbao_state\",\"dictValue\":\"1\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:04:19', 8);
INSERT INTO `sys_oper_log` VALUES (5, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:04:07\",\"default\":false,\"dictCode\":30,\"dictLabel\":\"否\",\"dictSort\":0,\"dictType\":\"shenbao_state\",\"dictValue\":\"0\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:04:35', 8);
INSERT INTO `sys_oper_log` VALUES (6, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"project\",\"className\":\"ShenbaoProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProjectName\",\"columnComment\":\"项目名称\",\"columnId\":2,\"columnName\":\"project_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProjectNum\",\"columnComment\":\"项目编号\",\"columnId\":3,\"columnName\":\"project_num\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectNum\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProjectAddress\",\"columnComment\":\"项目地址\",\"columnId\":4,\"columnName\":\"project_address\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectAddress\",\"javaTy', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:04:47', 49);
INSERT INTO `sys_oper_log` VALUES (7, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"project\",\"className\":\"ShenbaoProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:04:47\",\"usableColumn\":false},{\"capJavaField\":\"ProjectName\",\"columnComment\":\"项目名称\",\"columnId\":2,\"columnName\":\"project_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:04:47\",\"usableColumn\":false},{\"capJavaField\":\"ProjectNum\",\"columnComment\":\"项目编号\",\"columnId\":3,\"columnName\":\"project_num\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectNum\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:04:47\",\"usableColumn\":false},{\"capJavaField\":\"ProjectAddress\",\"columnComment\":\"项目地址\",\"columnId\":4,\"columnName\":\"project_address\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"i', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:05:21', 39);
INSERT INTO `sys_oper_log` VALUES (8, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"basics\",\"className\":\"ShenbaoProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:05:21\",\"usableColumn\":false},{\"capJavaField\":\"ProjectName\",\"columnComment\":\"项目名称\",\"columnId\":2,\"columnName\":\"project_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:05:21\",\"usableColumn\":false},{\"capJavaField\":\"ProjectNum\",\"columnComment\":\"项目编号\",\"columnId\":3,\"columnName\":\"project_num\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectNum\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:05:21\",\"usableColumn\":false},{\"capJavaField\":\"ProjectAddress\",\"columnComment\":\"项目地址\",\"columnId\":4,\"columnName\":\"project_address\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"is', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:06:35', 27);
INSERT INTO `sys_oper_log` VALUES (9, '菜单管理', 1, 'com.shenbaoiot.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"system\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"主数据\",\"menuType\":\"M\",\"orderNum\":0,\"params\":{},\"parentId\":0,\"path\":\"main\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:11:02', 11);
INSERT INTO `sys_oper_log` VALUES (10, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"basics\",\"className\":\"ShenbaoProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:06:35\",\"usableColumn\":false},{\"capJavaField\":\"ProjectName\",\"columnComment\":\"项目名称\",\"columnId\":2,\"columnName\":\"project_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:06:35\",\"usableColumn\":false},{\"capJavaField\":\"ProjectNum\",\"columnComment\":\"项目编号\",\"columnId\":3,\"columnName\":\"project_num\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectNum\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:06:35\",\"usableColumn\":false},{\"capJavaField\":\"ProjectAddress\",\"columnComment\":\"项目地址\",\"columnId\":4,\"columnName\":\"project_address\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"is', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:11:33', 25);
INSERT INTO `sys_oper_log` VALUES (11, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_project\"}', NULL, 0, NULL, '2023-03-10 08:11:36', 158);
INSERT INTO `sys_oper_log` VALUES (12, '项目管理', 2, 'com.shenbaoiot.basics.controller.ShenbaoProjectController.edit()', 'PUT', 1, 'admin', NULL, '/basics/basics', '127.0.0.1', '内网IP', '{\"createTime\":\"2020-08-15 14:43:18\",\"delFlag\":\"1\",\"id\":1,\"open\":\"1\",\"params\":{},\"projectAddress\":\"山东省济南市历城区\",\"projectLinkman\":\"常宝坤\",\"projectName\":\"海尔数据采集平台\",\"projectNum\":\"balkwill\",\"projectTel\":\"13964179025\",\"remark\":\"                                                                                                                                                                                                                                                                                                                                                                                                                        \\r\\n                    \\r\\n                    \\r\\n                    \\r\\n                    \\r\\n  \",\"updateTime\":\"2023-03-10 08:16:06\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:16:06', 17);
INSERT INTO `sys_oper_log` VALUES (13, '菜单管理', 1, 'com.shenbaoiot.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"dict\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"设备管理\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"dv\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:23:26', 11);
INSERT INTO `sys_oper_log` VALUES (14, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"dv_machinery_type\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:25:18', 74);
INSERT INTO `sys_oper_log` VALUES (15, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"dv\",\"className\":\"DvMachineryType\",\"columns\":[{\"capJavaField\":\"MachineryTypeId\",\"columnComment\":\"设备类型ID\",\"columnId\":16,\"columnName\":\"machinery_type_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"machineryTypeId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"MachineryTypeCode\",\"columnComment\":\"设备类型编码\",\"columnId\":17,\"columnName\":\"machinery_type_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"machineryTypeCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"MachineryTypeName\",\"columnComment\":\"设备类型名称\",\"columnId\":18,\"columnName\":\"machinery_type_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"machineryTypeName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ParentTypeId\",\"columnComment\":\"父类型ID\",\"columnId\":19,\"columnName\":\"parent_type_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdi', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 08:26:19', 43);
INSERT INTO `sys_oper_log` VALUES (16, '项目管理', 1, 'com.shenbaoiot.basics.controller.ShenbaoProjectController.add()', 'POST', 1, 'admin', NULL, '/basics/basics', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-03-10 10:16:39\",\"delFlag\":\"1\",\"params\":{},\"projectName\":\"1\"}', NULL, 1, 'Invalid bound statement (not found): com.shenbaoiot.basics.mapper.ShenbaoProjectMapper.insertShenbaoProject', '2023-03-10 10:16:39', 11);
INSERT INTO `sys_oper_log` VALUES (17, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"project\",\"className\":\"ShenbaoProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:11:33\",\"usableColumn\":false},{\"capJavaField\":\"ProjectName\",\"columnComment\":\"项目名称\",\"columnId\":2,\"columnName\":\"project_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:11:33\",\"usableColumn\":false},{\"capJavaField\":\"ProjectNum\",\"columnComment\":\"项目编号\",\"columnId\":3,\"columnName\":\"project_num\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectNum\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:11:33\",\"usableColumn\":false},{\"capJavaField\":\"ProjectAddress\",\"columnComment\":\"项目地址\",\"columnId\":4,\"columnName\":\"project_address\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:01:36\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"i', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:21:08', 88);
INSERT INTO `sys_oper_log` VALUES (18, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_project\"}', NULL, 0, NULL, '2023-03-10 10:21:11', 287);
INSERT INTO `sys_oper_log` VALUES (19, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"machinerytype\",\"className\":\"DvMachineryType\",\"columns\":[{\"capJavaField\":\"MachineryTypeId\",\"columnComment\":\"设备类型ID\",\"columnId\":16,\"columnName\":\"machinery_type_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"machineryTypeId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:26:19\",\"usableColumn\":false},{\"capJavaField\":\"MachineryTypeCode\",\"columnComment\":\"设备类型编码\",\"columnId\":17,\"columnName\":\"machinery_type_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"machineryTypeCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:26:19\",\"usableColumn\":false},{\"capJavaField\":\"MachineryTypeName\",\"columnComment\":\"设备类型名称\",\"columnId\":18,\"columnName\":\"machinery_type_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"machineryTypeName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 08:26:19\",\"usableColumn\":false},{\"capJavaField\":\"ParentTypeId\",\"columnComment\":\"父类型ID\",\"columnId\":19,\"columnName\":\"parent_type_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"c', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:32:48', 60);
INSERT INTO `sys_oper_log` VALUES (20, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"machinerytype\",\"className\":\"DvMachineryType\",\"columns\":[{\"capJavaField\":\"MachineryTypeId\",\"columnComment\":\"设备类型ID\",\"columnId\":16,\"columnName\":\"machinery_type_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"machineryTypeId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:32:48\",\"usableColumn\":false},{\"capJavaField\":\"MachineryTypeCode\",\"columnComment\":\"设备类型编码\",\"columnId\":17,\"columnName\":\"machinery_type_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"machineryTypeCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:32:48\",\"usableColumn\":false},{\"capJavaField\":\"MachineryTypeName\",\"columnComment\":\"设备类型名称\",\"columnId\":18,\"columnName\":\"machinery_type_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:25:18\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"machineryTypeName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:32:48\",\"usableColumn\":false},{\"capJavaField\":\"ParentTypeId\",\"columnComment\":\"父类型ID\",\"columnId\":19,\"columnName\":\"parent_type_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"c', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:33:06', 36);
INSERT INTO `sys_oper_log` VALUES (21, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"dv_machinery_type\"}', NULL, 0, NULL, '2023-03-10 10:33:37', 152);
INSERT INTO `sys_oper_log` VALUES (22, '设备类型设置', 1, 'com.shenbaoiot.basics.controller.DvMachineryTypeController.add()', 'POST', 1, 'admin', NULL, '/dv/machinerytype', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-03-10 10:38:38\",\"enableFlag\":\"Y\",\"machineryTypeName\":\"22\",\"params\":{},\"parentTypeId\":0,\"remark\":\"22\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'machinery_type_code\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dv\\DvMachineryTypeMapper.xml]\r\n### The error may involve com.shenbaoiot.basics.mapper.DvMachineryTypeMapper.insertDvMachineryType-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into dv_machinery_type          ( machinery_type_name,             parent_type_id,                          enable_flag,             remark,                                                                              create_time )           values ( ?,             ?,                          ?,             ?,                                                                              ? )\r\n### Cause: java.sql.SQLException: Field \'machinery_type_code\' doesn\'t have a default value\n; Field \'machinery_type_code\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'machinery_type_code\' doesn\'t have a default value', '2023-03-10 10:38:38', 109);
INSERT INTO `sys_oper_log` VALUES (23, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_workshop\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:49:48', 84);
INSERT INTO `sys_oper_log` VALUES (24, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"workshop\",\"className\":\"ShenbaoWorkshop\",\"columns\":[{\"capJavaField\":\"WorkshopId\",\"columnComment\":\"车间ID\",\"columnId\":31,\"columnName\":\"workshop_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"workshopId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"WorkshopCode\",\"columnComment\":\"车间编码\",\"columnId\":32,\"columnName\":\"workshop_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"workshopCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"WorkshopName\",\"columnComment\":\"车间名称\",\"columnId\":33,\"columnName\":\"workshop_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"workshopName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Area\",\"columnComment\":\"面积\",\"columnId\":34,\"columnName\":\"area\",\"columnType\":\"double(12,2)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"is', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:50:28', 38);
INSERT INTO `sys_oper_log` VALUES (25, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"workshop\",\"className\":\"ShenbaoWorkshop\",\"columns\":[{\"capJavaField\":\"WorkshopId\",\"columnComment\":\"车间ID\",\"columnId\":31,\"columnName\":\"workshop_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"workshopId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:50:28\",\"usableColumn\":false},{\"capJavaField\":\"WorkshopCode\",\"columnComment\":\"车间编码\",\"columnId\":32,\"columnName\":\"workshop_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"workshopCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:50:28\",\"usableColumn\":false},{\"capJavaField\":\"WorkshopName\",\"columnComment\":\"车间名称\",\"columnId\":33,\"columnName\":\"workshop_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"workshopName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:50:28\",\"usableColumn\":false},{\"capJavaField\":\"Area\",\"columnComment\":\"面积\",\"columnId\":34,\"columnName\":\"area\",\"columnType\":\"double(12,2)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:51:34', 36);
INSERT INTO `sys_oper_log` VALUES (26, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"workshop\",\"className\":\"ShenbaoWorkshop\",\"columns\":[{\"capJavaField\":\"WorkshopId\",\"columnComment\":\"车间ID\",\"columnId\":31,\"columnName\":\"workshop_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"workshopId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:51:34\",\"usableColumn\":false},{\"capJavaField\":\"WorkshopCode\",\"columnComment\":\"车间编码\",\"columnId\":32,\"columnName\":\"workshop_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"workshopCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:51:34\",\"usableColumn\":false},{\"capJavaField\":\"WorkshopName\",\"columnComment\":\"车间名称\",\"columnId\":33,\"columnName\":\"workshop_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"workshopName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:51:34\",\"usableColumn\":false},{\"capJavaField\":\"Area\",\"columnComment\":\"面积\",\"columnId\":34,\"columnName\":\"area\",\"columnType\":\"double(12,2)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 10:49:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:52:55', 30);
INSERT INTO `sys_oper_log` VALUES (27, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_workshop\"}', NULL, 0, NULL, '2023-03-10 10:52:59', 165);
INSERT INTO `sys_oper_log` VALUES (28, '车间管理', 1, 'com.shenbaoiot.basics.controller.ShenbaoWorkshopController.add()', 'POST', 1, 'admin', NULL, '/basics/workshop', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-03-10 10:56:29\",\"enableFlag\":\"1\",\"params\":{},\"workshopCode\":\"11\",\"workshopId\":1,\"workshopName\":\"1号车间\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:56:29', 32);
INSERT INTO `sys_oper_log` VALUES (29, '车间管理', 2, 'com.shenbaoiot.basics.controller.ShenbaoWorkshopController.edit()', 'PUT', 1, 'admin', NULL, '/basics/workshop', '127.0.0.1', '内网IP', '{\"area\":125,\"attr3\":0,\"attr4\":0,\"charge\":\"常宝坤\",\"createBy\":\"\",\"createTime\":\"2023-03-10 10:56:29\",\"enableFlag\":\"1\",\"params\":{},\"remark\":\"\",\"updateBy\":\"\",\"updateTime\":\"2023-03-10 10:59:53\",\"workshopCode\":\"11\",\"workshopId\":1,\"workshopName\":\"1号车间\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 10:59:53', 11);
INSERT INTO `sys_oper_log` VALUES (30, '车间管理', 1, 'com.shenbaoiot.basics.controller.ShenbaoWorkshopController.add()', 'POST', 1, 'admin', NULL, '/basics/workshop', '127.0.0.1', '内网IP', '{\"area\":1110,\"charge\":\"常宝坤\",\"createTime\":\"2023-03-10 11:08:14\",\"enableFlag\":\"1\",\"params\":{},\"workshopCode\":\"222\",\"workshopId\":2,\"workshopName\":\"2号车间\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 11:08:14', 6);
INSERT INTO `sys_oper_log` VALUES (31, '车间管理', 2, 'com.shenbaoiot.basics.controller.ShenbaoWorkshopController.edit()', 'PUT', 1, 'admin', NULL, '/basics/workshop', '127.0.0.1', '内网IP', '{\"area\":125,\"attr3\":0,\"attr4\":0,\"charge\":\"常宝坤\",\"createBy\":\"\",\"createTime\":\"2023-03-10 10:56:29\",\"enableFlag\":\"1\",\"params\":{},\"remark\":\"\",\"updateBy\":\"\",\"updateTime\":\"2023-03-10 11:08:19\",\"workshopCode\":\"111\",\"workshopId\":1,\"workshopName\":\"1号车间\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 11:08:19', 6);
INSERT INTO `sys_oper_log` VALUES (32, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-03-01 09:02:44\",\"icon\":\"system\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1,\"menuName\":\"系统管理\",\"menuType\":\"M\",\"orderNum\":100,\"params\":{},\"parentId\":0,\"path\":\"system\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 13:43:22', 16);
INSERT INTO `sys_oper_log` VALUES (33, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-03-01 09:02:44\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2,\"menuName\":\"系统监控\",\"menuType\":\"M\",\"orderNum\":102,\"params\":{},\"parentId\":0,\"path\":\"monitor\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 13:43:31', 7);
INSERT INTO `sys_oper_log` VALUES (34, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-03-01 09:02:44\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":101,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 13:43:38', 7);
INSERT INTO `sys_oper_log` VALUES (35, '菜单管理', 3, 'com.shenbaoiot.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":601}', 0, NULL, '2023-03-10 13:44:12', 6);
INSERT INTO `sys_oper_log` VALUES (36, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-03-01 09:02:44\",\"icon\":\"guide\",\"isCache\":\"0\",\"isFrame\":\"0\",\"menuId\":4,\"menuName\":\"物联网官网\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"http://www.shenbaoiot.com\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 13:44:45', 7);
INSERT INTO `sys_oper_log` VALUES (37, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2023-03-01 09:02:44\",\"icon\":\"guide\",\"isCache\":\"0\",\"isFrame\":\"0\",\"menuId\":4,\"menuName\":\"物联网官网\",\"menuType\":\"M\",\"orderNum\":103,\"params\":{},\"parentId\":0,\"path\":\"http://www.shenbaoiot.com\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 13:45:03', 6);
INSERT INTO `sys_oper_log` VALUES (38, '菜单管理', 1, 'com.shenbaoiot.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"icon\":\"eye-open\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"告警中心\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"/ware\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 15:02:53', 11);
INSERT INTO `sys_oper_log` VALUES (39, '代码生成', 3, 'com.shenbaoiot.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:24:49', 54);
INSERT INTO `sys_oper_log` VALUES (40, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_dev_classified\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:25:19', 65);
INSERT INTO `sys_oper_log` VALUES (41, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"ShenbaoDevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":46,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":47,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":48,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型：0-直连设备，1-网关子设备，2-网关设备\",\"columnId\":49,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isI', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:26:22', 41);
INSERT INTO `sys_oper_log` VALUES (42, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"DevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":46,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 17:26:22\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":47,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 17:26:22\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":48,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2023-03-10 17:26:22\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型：0-直连设备，1-网关子设备，2-网关设备\",\"columnId\":49,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:25:19\",\"dictType\":\"\",\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:26:49', 45);
INSERT INTO `sys_oper_log` VALUES (43, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"设备类型\",\"dictType\":\"device_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:28:16', 10);
INSERT INTO `sys_oper_log` VALUES (44, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"直连设备\",\"dictSort\":0,\"dictType\":\"device_type\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"0-直连设备，1-网关子设备，2-网关设备\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:28:47', 10);
INSERT INTO `sys_oper_log` VALUES (45, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"cssClass\":\"\",\"default\":false,\"dictLabel\":\"网关子设备\",\"dictSort\":1,\"dictType\":\"device_type\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"0-直连设备，1-网关子设备，2-网关设备\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:28:58', 8);
INSERT INTO `sys_oper_log` VALUES (46, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"网关设备\",\"dictSort\":2,\"dictType\":\"device_type\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"remark\":\"0-直连设备，1-网关子设备，2-网关设备\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:29:27', 8);
INSERT INTO `sys_oper_log` VALUES (47, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"状态\",\"dictType\":\"status\",\"params\":{},\"remark\":\"（0正常 1停用）\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-10 17:44:27', 7);
INSERT INTO `sys_oper_log` VALUES (48, '代码生成', 3, 'com.shenbaoiot.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/4', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:06:00', 34);
INSERT INTO `sys_oper_log` VALUES (49, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_dev_classified\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:06:06', 58);
INSERT INTO `sys_oper_log` VALUES (50, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"ShenbaoDevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":64,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":65,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":66,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型：\",\"columnId\":67,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"device_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:07:27', 59);
INSERT INTO `sys_oper_log` VALUES (51, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"DevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":64,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:07:27\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":65,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:07:27\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":66,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:07:27\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型：\",\"columnId\":67,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"device_type\",\"edit\":true', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:07:49', 38);
INSERT INTO `sys_oper_log` VALUES (52, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"DevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":64,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:07:49\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":65,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:07:49\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":66,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:07:49\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型：\",\"columnId\":67,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"device_type\",\"edit\":true', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:08:52', 31);
INSERT INTO `sys_oper_log` VALUES (53, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_classified\"}', NULL, 0, NULL, '2023-03-11 08:09:01', 166);
INSERT INTO `sys_oper_log` VALUES (54, '字典类型', 2, 'com.shenbaoiot.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:44:27\",\"dictId\":13,\"dictName\":\"设备状态\",\"dictType\":\"status\",\"params\":{},\"remark\":\"（0正常 1停用）\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:13:55', 34);
INSERT INTO `sys_oper_log` VALUES (55, '字典类型', 2, 'com.shenbaoiot.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-03-10 08:03:43\",\"dictId\":11,\"dictName\":\"是否启用\",\"dictType\":\"shenbao_state\",\"params\":{},\"remark\":\"0-否，1-是\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:14:38', 17);
INSERT INTO `sys_oper_log` VALUES (56, '字典类型', 2, 'com.shenbaoiot.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:44:27\",\"dictId\":13,\"dictName\":\"设备状态\",\"dictType\":\"dev_status\",\"params\":{},\"remark\":\"（0正常 1停用）\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:14:58', 15);
INSERT INTO `sys_oper_log` VALUES (57, '字典类型', 2, 'com.shenbaoiot.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-03-10 17:44:27\",\"dictId\":13,\"dictName\":\"设备状态\",\"dictType\":\"device_status\",\"params\":{},\"remark\":\"（0正常 1停用）\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:15:16', 13);
INSERT INTO `sys_oper_log` VALUES (58, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"DevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":64,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:08:52\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":65,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:08:52\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":66,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:08:52\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型：\",\"columnId\":67,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"device_type\",\"edit\":true', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:15:44', 56);
INSERT INTO `sys_oper_log` VALUES (59, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_classified\"}', NULL, 0, NULL, '2023-03-11 08:16:11', 150);
INSERT INTO `sys_oper_log` VALUES (60, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:18:15\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":1,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:18:15', 54);
INSERT INTO `sys_oper_log` VALUES (61, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:18:19\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":1,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:18:19', 5);
INSERT INTO `sys_oper_log` VALUES (62, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:18:23\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":1,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:18:23', 5);
INSERT INTO `sys_oper_log` VALUES (63, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:19:11\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":1,\"params\":{},\"parentId\":0}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:19:11', 4);
INSERT INTO `sys_oper_log` VALUES (64, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"DevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":64,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:15:44\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":65,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:15:44\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":66,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:15:44\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型：\",\"columnId\":67,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:06:06\",\"dictType\":\"device_type\",\"edit\":true', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:20:25', 58);
INSERT INTO `sys_oper_log` VALUES (65, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_classified\"}', NULL, 0, NULL, '2023-03-11 08:20:32', 153);
INSERT INTO `sys_oper_log` VALUES (66, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:22:54\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":0,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:22:54', 57);
INSERT INTO `sys_oper_log` VALUES (67, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:22:58\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":0,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:22:58', 4);
INSERT INTO `sys_oper_log` VALUES (68, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:23:01\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":0,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:23:01', 3);
INSERT INTO `sys_oper_log` VALUES (69, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:23:06\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":0,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:23:06', 4);
INSERT INTO `sys_oper_log` VALUES (70, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:27:04\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":0,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:27:04', 62);
INSERT INTO `sys_oper_log` VALUES (71, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:28:05\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"orderNum\":0,\"params\":{},\"parentId\":200}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspaces\\Shenbao-Iot\\shenbao-server\\shenbaoiot-platform\\target\\classes\\mapper\\dev\\DevClassifiedMapper.xml]\r\n### The error may involve com.shenbaoiot.dev.mapper.DevClassifiedMapper.insertDevClassified-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into shenbao_dev_classified          ( classified_code,             classified_name,             device_type,             parent_id,                          enable_flag,             order_num,                                                                                           create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value\n; Field \'ancestors\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'ancestors\' doesn\'t have a default value', '2023-03-11 08:28:05', 4);
INSERT INTO `sys_oper_log` VALUES (72, '代码生成', 3, 'com.shenbaoiot.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/5', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:31:53', 27);
INSERT INTO `sys_oper_log` VALUES (73, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_dev_classified\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:31:58', 62);
INSERT INTO `sys_oper_log` VALUES (74, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"DevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":81,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":82,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":83,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型分类\",\"columnId\":84,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"device_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isLi', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:34:05', 45);
INSERT INTO `sys_oper_log` VALUES (75, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"classified\",\"className\":\"DevClassified\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"设备类型id\",\"columnId\":81,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:34:05\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedCode\",\"columnComment\":\"设备类型编码\",\"columnId\":82,\"columnName\":\"classified_code\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:34:05\",\"usableColumn\":false},{\"capJavaField\":\"ClassifiedName\",\"columnComment\":\"设备类型名称\",\"columnId\":83,\"columnName\":\"classified_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"classifiedName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 08:34:05\",\"usableColumn\":false},{\"capJavaField\":\"DeviceType\",\"columnComment\":\"设备类型分类\",\"columnId\":84,\"columnName\":\"device_type\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:31:58\",\"dictType\":\"device_type\",\"edit\":tru', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:34:27', 39);
INSERT INTO `sys_oper_log` VALUES (76, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_classified\"}', NULL, 0, NULL, '2023-03-11 08:34:30', 150);
INSERT INTO `sys_oper_log` VALUES (77, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_001\",\"classifiedName\":\"加注机\",\"createTime\":\"2023-03-11 08:38:27\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"id\":201,\"orderNum\":1,\"params\":{},\"parentId\":200}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:38:27', 23);
INSERT INTO `sys_oper_log` VALUES (78, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_002\",\"classifiedName\":\"打标机\",\"createTime\":\"2023-03-11 08:39:19\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"id\":202,\"orderNum\":2,\"params\":{},\"parentId\":200}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:39:19', 5);
INSERT INTO `sys_oper_log` VALUES (79, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"dev/classified/index\",\"createTime\":\"2023-03-11 08:09:52\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1088,\"menuName\":\"设备类型管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1068,\"path\":\"classified\",\"perms\":\"dev:classified:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:43:34', 13);
INSERT INTO `sys_oper_log` VALUES (80, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"dev/classified/index\",\"createTime\":\"2023-03-11 08:09:52\",\"icon\":\"cascader\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1088,\"menuName\":\"设备类型管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1068,\"path\":\"classified\",\"perms\":\"dev:classified:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:44:05', 7);
INSERT INTO `sys_oper_log` VALUES (81, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"basics/project/index\",\"createTime\":\"2023-03-10 10:23:39\",\"icon\":\"cascader\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1069,\"menuName\":\"项目管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1061,\"path\":\"project\",\"perms\":\"basics:project:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:46:14', 7);
INSERT INTO `sys_oper_log` VALUES (82, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"basics/workshop/index\",\"createTime\":\"2023-03-10 10:53:57\",\"icon\":\"cascader\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1081,\"menuName\":\"车间管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1061,\"path\":\"workshop\",\"perms\":\"basics:workshop:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:46:21', 7);
INSERT INTO `sys_oper_log` VALUES (83, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_dev_product\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:52:04', 101);
INSERT INTO `sys_oper_log` VALUES (84, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"连网方式\",\"dictType\":\"network_way\",\"params\":{},\"remark\":\"连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 08:53:44', 9);
INSERT INTO `sys_oper_log` VALUES (85, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"以太网\",\"dictSort\":0,\"dictType\":\"network_way\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 09:08:57', 8);
INSERT INTO `sys_oper_log` VALUES (86, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"wifi\",\"dictSort\":1,\"dictType\":\"network_way\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\\n\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 09:09:14', 6);
INSERT INTO `sys_oper_log` VALUES (87, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"串口\",\"dictSort\":2,\"dictType\":\"network_way\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"remark\":\"连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\\n\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 09:09:31', 6);
INSERT INTO `sys_oper_log` VALUES (88, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"蜂窝（2G / 3G / 4G / 5G）\",\"dictSort\":3,\"dictType\":\"network_way\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"remark\":\"连网方式：0-以太网，1-wifi,2-串口，3-蜂窝（2G / 3G / 4G / 5G）\\n\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 09:09:42', 6);
INSERT INTO `sys_oper_log` VALUES (89, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"product\",\"className\":\"ShenbaoDevProduct\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":98,\"columnName\":\"id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProductName\",\"columnComment\":\"产品名称\",\"columnId\":99,\"columnName\":\"product_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"productName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProductMarking\",\"columnComment\":\"产品唯一标识\",\"columnId\":100,\"columnName\":\"product_marking\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"productMarking\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProductClassifiedId\",\"columnComment\":\"所属品类ID\",\"columnId\":101,\"columnName\":\"product_classified_id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 09:09:53', 69);
INSERT INTO `sys_oper_log` VALUES (90, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"product\",\"className\":\"DevProduct\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":98,\"columnName\":\"id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 09:09:53\",\"usableColumn\":false},{\"capJavaField\":\"ProductName\",\"columnComment\":\"产品名称\",\"columnId\":99,\"columnName\":\"product_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"productName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 09:09:53\",\"usableColumn\":false},{\"capJavaField\":\"ProductMarking\",\"columnComment\":\"产品唯一标识\",\"columnId\":100,\"columnName\":\"product_marking\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"productMarking\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 09:09:53\",\"usableColumn\":false},{\"capJavaField\":\"ProductClassifiedId\",\"columnComment\":\"所属品类ID\",\"columnId\":101,\"columnName\":\"product_classified_id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 09:13:22', 61);
INSERT INTO `sys_oper_log` VALUES (91, '项目管理', 3, 'com.shenbaoiot.basics.controller.ShenbaoProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/basics/project/3', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 10:50:47', 4);
INSERT INTO `sys_oper_log` VALUES (92, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"product\",\"className\":\"DevProduct\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":98,\"columnName\":\"id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 09:13:22\",\"usableColumn\":false},{\"capJavaField\":\"ProductName\",\"columnComment\":\"产品名称\",\"columnId\":99,\"columnName\":\"product_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"productName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 09:13:22\",\"usableColumn\":false},{\"capJavaField\":\"ProductMarking\",\"columnComment\":\"产品唯一标识\",\"columnId\":100,\"columnName\":\"product_marking\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"productMarking\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"updateTime\":\"2023-03-11 09:13:22\",\"usableColumn\":false},{\"capJavaField\":\"ProductClassifiedId\",\"columnComment\":\"所属品类ID\",\"columnId\":101,\"columnName\":\"product_classified_id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-11 08:52:04\",\"dictType\":\"\",\"edit\":true,\"htmlType\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 10:59:46', 94);
INSERT INTO `sys_oper_log` VALUES (93, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_product\"}', NULL, 0, NULL, '2023-03-11 10:59:50', 179);
INSERT INTO `sys_oper_log` VALUES (94, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-03-11 11:06:08\",\"delFlag\":\"0\",\"deviceType\":\"1\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/03/11/device-product_20230311110552A001.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 11:06:08', 32);
INSERT INTO `sys_oper_log` VALUES (95, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-03-11 11:06:56\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":2,\"params\":{},\"photoUrl\":\"/profile/upload/2023/03/11/device-product_20230311110651A002.png\",\"productMarking\":\"ckfwq1\",\"productName\":\"串口服务器1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 11:06:56', 4);
INSERT INTO `sys_oper_log` VALUES (96, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-03-11 11:07:36\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":3,\"params\":{},\"photoUrl\":\"/profile/upload/2023/03/11/device-product_20230311110725A003.png\",\"productMarking\":\"jzj1\",\"productName\":\"加注机\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 11:07:36', 6);
INSERT INTO `sys_oper_log` VALUES (97, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_100\",\"classifiedName\":\"退火\",\"createTime\":\"2023-03-11 11:08:36\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"id\":203,\"params\":{},\"parentId\":0}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 11:08:36', 8);
INSERT INTO `sys_oper_log` VALUES (98, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_101\",\"classifiedName\":\"退火炉\",\"createTime\":\"2023-03-11 11:09:02\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"id\":204,\"params\":{},\"parentId\":203}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 11:09:02', 5);
INSERT INTO `sys_oper_log` VALUES (99, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"dev/product/index\",\"createTime\":\"2023-03-11 11:00:26\",\"icon\":\"cascader\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1094,\"menuName\":\"产品管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1068,\"path\":\"product\",\"perms\":\"dev:product:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-11 11:09:45', 13);
INSERT INTO `sys_oper_log` VALUES (100, '项目管理', 2, 'com.shenbaoiot.basics.controller.ShenbaoProjectController.edit()', 'PUT', 1, 'admin', NULL, '/basics/project', '127.0.0.1', '内网IP', '{\"createTime\":\"2020-08-15 14:43:18\",\"delFlag\":\"1\",\"id\":1,\"open\":\"1\",\"params\":{},\"projectAddress\":\"山东省济南市历城区\",\"projectLinkman\":\"常宝坤\",\"projectName\":\"数据采集平台\",\"projectNum\":\"balkwill\",\"projectTel\":\"13964179025\",\"remark\":\"                                                                                                                                                                                                                                                                                                                                                                                                                        \\r\\n                    \\r\\n                    \\r\\n                    \\r\\n                    \\r\\n  \",\"updateTime\":\"2023-03-15 13:35:10\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-15 13:35:10', 14);
INSERT INTO `sys_oper_log` VALUES (101, '项目管理', 2, 'com.shenbaoiot.basics.controller.ShenbaoProjectController.edit()', 'PUT', 1, 'admin', NULL, '/basics/project', '127.0.0.1', '内网IP', '{\"createTime\":\"2021-03-01 14:15:37\",\"delFlag\":\"1\",\"id\":2,\"open\":\"0\",\"params\":{},\"projectAddress\":\"山东省济南市历下区\",\"projectLinkman\":\"冯庆超\",\"projectName\":\"智慧消防演示平台\",\"projectNum\":\"0001\",\"projectTel\":\"13276447387\",\"remark\":\"                                                                                                \\r\\n                    \\r\\n                    \\r\\n                    \\r\\n                    \",\"updateTime\":\"2023-03-15 13:35:15\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-15 13:35:15', 6);
INSERT INTO `sys_oper_log` VALUES (102, '项目管理', 2, 'com.shenbaoiot.basics.controller.ShenbaoProjectController.edit()', 'PUT', 1, 'admin', NULL, '/basics/project', '127.0.0.1', '内网IP', '{\"createTime\":\"2021-03-01 14:15:37\",\"delFlag\":\"1\",\"id\":2,\"open\":\"0\",\"params\":{},\"projectAddress\":\"山东省济南市历下区\",\"projectLinkman\":\"常宝坤\",\"projectName\":\"智慧消防演示平台\",\"projectNum\":\"0001\",\"projectTel\":\"\",\"remark\":\"                                                                                                \\r\\n                    \\r\\n                    \\r\\n                    \\r\\n                    \",\"updateTime\":\"2023-03-15 13:35:25\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-15 13:35:25', 6);
INSERT INTO `sys_oper_log` VALUES (103, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"baokun_test\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-15 13:37:06', 97);
INSERT INTO `sys_oper_log` VALUES (104, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"test\",\"className\":\"BaokunTest\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":129,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-15 13:37:06\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":130,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-15 13:37:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Sex\",\"columnComment\":\"性别\",\"columnId\":131,\"columnName\":\"sex\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-15 13:37:06\",\"dictType\":\"sys_user_sex\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"sex\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"HeadFile\",\"columnComment\":\"头像地址\",\"columnId\":132,\"columnName\":\"head_file\",\"columnType\":\"varchar(500)\",\"createBy\":\"admin\",\"createTime\":\"2023-03-15 13:37:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"fileUpload\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"headFile\",\"javaType\":\"String\",\"list\":tru', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-03-15 13:38:15', 65);
INSERT INTO `sys_oper_log` VALUES (105, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"baokun_test\"}', NULL, 0, NULL, '2023-03-15 13:38:19', 239);
INSERT INTO `sys_oper_log` VALUES (106, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"存储策略\",\"dictType\":\"storage_strategy\",\"params\":{},\"remark\":\"存储策略\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:48:46', 23);
INSERT INTO `sys_oper_log` VALUES (107, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"cssClass\":\"\",\"default\":false,\"dictLabel\":\"不存储\",\"dictSort\":0,\"dictType\":\"storage_strategy\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:49:16', 13);
INSERT INTO `sys_oper_log` VALUES (108, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"ElasticSearch-行式存储\",\"dictSort\":1,\"dictType\":\"storage_strategy\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:49:25', 11);
INSERT INTO `sys_oper_log` VALUES (109, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"ElasticSearch-列式存储\",\"dictSort\":2,\"dictType\":\"storage_strategy\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:49:38', 11);
INSERT INTO `sys_oper_log` VALUES (110, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"Influxdb-行式存储\",\"dictSort\":3,\"dictType\":\"storage_strategy\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:49:48', 10);
INSERT INTO `sys_oper_log` VALUES (111, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"Influxdb-列式存储\",\"dictSort\":4,\"dictType\":\"storage_strategy\",\"dictValue\":\"4\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:49:56', 10);
INSERT INTO `sys_oper_log` VALUES (112, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"TDengine-行式存储\",\"dictSort\":5,\"dictType\":\"storage_strategy\",\"dictValue\":\"5\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:50:03', 11);
INSERT INTO `sys_oper_log` VALUES (113, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"TDengine-行式存储\",\"dictSort\":6,\"dictType\":\"storage_strategy\",\"dictValue\":\"6\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:50:12', 11);
INSERT INTO `sys_oper_log` VALUES (114, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"数据类型\",\"dictType\":\"data_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:53:01', 13);
INSERT INTO `sys_oper_log` VALUES (115, '字典类型', 2, 'com.shenbaoiot.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 13:53:01\",\"dictId\":16,\"dictName\":\"数据类型\",\"dictType\":\"data_type\",\"params\":{},\"remark\":\"数据类型\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:53:07', 41);
INSERT INTO `sys_oper_log` VALUES (116, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"int(整数型)\",\"dictSort\":0,\"dictType\":\"data_type\",\"dictValue\":\"int\",\"listClass\":\"default\",\"params\":{},\"remark\":\"int(整数型)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:57:08', 12);
INSERT INTO `sys_oper_log` VALUES (117, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"long(长整数型)\",\"dictSort\":1,\"dictType\":\"data_type\",\"dictValue\":\"long\",\"listClass\":\"default\",\"params\":{},\"remark\":\"long(长整数型)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:58:07', 10);
INSERT INTO `sys_oper_log` VALUES (118, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"float(单精度浮点型)\",\"dictSort\":2,\"dictType\":\"data_type\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:58:22', 9);
INSERT INTO `sys_oper_log` VALUES (119, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"double(双精度浮点数)\",\"dictSort\":4,\"dictType\":\"data_type\",\"dictValue\":\"double\",\"listClass\":\"default\",\"params\":{},\"remark\":\"double(双精度浮点数)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:58:40', 10);
INSERT INTO `sys_oper_log` VALUES (120, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 13:58:22\",\"default\":false,\"dictCode\":48,\"dictLabel\":\"float(单精度浮点型)\",\"dictSort\":2,\"dictType\":\"data_type\",\"dictValue\":\"2\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"float(单精度浮点型)\\n\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:58:46', 10);
INSERT INTO `sys_oper_log` VALUES (121, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"text(字符串)\",\"dictSort\":5,\"dictType\":\"data_type\",\"dictValue\":\"text\",\"listClass\":\"default\",\"params\":{},\"remark\":\"text(字符串)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:59:05', 10);
INSERT INTO `sys_oper_log` VALUES (122, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 13:58:40\",\"default\":false,\"dictCode\":49,\"dictLabel\":\"double(双精度浮点数)\",\"dictSort\":3,\"dictType\":\"data_type\",\"dictValue\":\"double\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"double(双精度浮点数)\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:59:11', 10);
INSERT INTO `sys_oper_log` VALUES (123, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 13:59:05\",\"default\":false,\"dictCode\":50,\"dictLabel\":\"text(字符串)\",\"dictSort\":4,\"dictType\":\"data_type\",\"dictValue\":\"text\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"text(字符串)\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:59:15', 8);
INSERT INTO `sys_oper_log` VALUES (124, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"bool(布尔型)\",\"dictSort\":0,\"dictType\":\"data_type\",\"dictValue\":\"bool\",\"listClass\":\"default\",\"params\":{},\"remark\":\"bool(布尔型)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:59:32', 11);
INSERT INTO `sys_oper_log` VALUES (125, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 13:59:32\",\"default\":false,\"dictCode\":51,\"dictLabel\":\"bool(布尔型)\",\"dictSort\":5,\"dictType\":\"data_type\",\"dictValue\":\"bool\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"bool(布尔型)\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:59:40', 11);
INSERT INTO `sys_oper_log` VALUES (126, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"date(时间型)\",\"dictSort\":6,\"dictType\":\"data_type\",\"dictValue\":\"date\",\"listClass\":\"default\",\"params\":{},\"remark\":\"date(时间型)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 13:59:54', 9);
INSERT INTO `sys_oper_log` VALUES (127, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"enum(枚举)\",\"dictSort\":7,\"dictType\":\"data_type\",\"dictValue\":\"enum\",\"listClass\":\"default\",\"params\":{},\"remark\":\"enum(枚举)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:00:12', 12);
INSERT INTO `sys_oper_log` VALUES (128, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"array(数组)\",\"dictSort\":8,\"dictType\":\"data_type\",\"dictValue\":\"array(数组)\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:00:28', 10);
INSERT INTO `sys_oper_log` VALUES (129, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 14:00:28\",\"default\":false,\"dictCode\":54,\"dictLabel\":\"array(数组)\",\"dictSort\":8,\"dictType\":\"data_type\",\"dictValue\":\"array(数组)\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"array(数组)\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:00:32', 10);
INSERT INTO `sys_oper_log` VALUES (130, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"object(结构体)\",\"dictSort\":9,\"dictType\":\"data_type\",\"dictValue\":\"object\",\"listClass\":\"default\",\"params\":{},\"remark\":\"object(结构体)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:00:51', 9);
INSERT INTO `sys_oper_log` VALUES (131, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"file(文件)\",\"dictSort\":10,\"dictType\":\"data_type\",\"dictValue\":\"file\",\"listClass\":\"default\",\"params\":{},\"remark\":\"file(文件)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:01:11', 8);
INSERT INTO `sys_oper_log` VALUES (132, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"password(密码)\",\"dictSort\":11,\"dictType\":\"data_type\",\"dictValue\":\"password\",\"listClass\":\"default\",\"params\":{},\"remark\":\"password(密码)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:01:43', 9);
INSERT INTO `sys_oper_log` VALUES (133, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"geoPoint(地理位置)\",\"dictSort\":12,\"dictType\":\"data_type\",\"dictValue\":\"geoPoint\",\"listClass\":\"default\",\"params\":{},\"remark\":\"geoPoint(地理位置)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:02:01', 9);
INSERT INTO `sys_oper_log` VALUES (134, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"是否只读\",\"dictType\":\"read_only\",\"params\":{},\"remark\":\"是否只读\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:02:47', 29);
INSERT INTO `sys_oper_log` VALUES (135, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"否\",\"dictSort\":0,\"dictType\":\"read_only\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:03:01', 9);
INSERT INTO `sys_oper_log` VALUES (136, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"是\",\"dictSort\":1,\"dictType\":\"read_only\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:03:07', 10);
INSERT INTO `sys_oper_log` VALUES (137, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"属性来源\",\"dictType\":\"attribute_source\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:04:12', 7);
INSERT INTO `sys_oper_log` VALUES (138, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"设备\",\"dictSort\":0,\"dictType\":\"attribute_source\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:04:27', 8);
INSERT INTO `sys_oper_log` VALUES (139, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"手动\",\"dictSort\":1,\"dictType\":\"attribute_source\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:04:39', 8);
INSERT INTO `sys_oper_log` VALUES (140, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"规则\",\"dictSort\":2,\"dictType\":\"attribute_source\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:04:49', 8);
INSERT INTO `sys_oper_log` VALUES (141, '字典类型', 2, 'com.shenbaoiot.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 14:02:47\",\"dictId\":17,\"dictName\":\"是否\",\"dictType\":\"whether_or_not\",\"params\":{},\"remark\":\"是否 0-否、1-是\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:06:32', 19);
INSERT INTO `sys_oper_log` VALUES (142, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"报警级别\",\"dictSort\":0,\"dictType\":\"read_only\",\"dictValue\":\"alarm_level\",\"listClass\":\"default\",\"params\":{},\"remark\":\"报警级别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:07:41', 8);
INSERT INTO `sys_oper_log` VALUES (143, '字典类型', 3, 'com.shenbaoiot.web.controller.system.SysDictDataController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dict/data/64', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:07:54', 18);
INSERT INTO `sys_oper_log` VALUES (144, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"预警级别\",\"dictType\":\"alarm_level\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:08:10', 9);
INSERT INTO `sys_oper_log` VALUES (145, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"一般\",\"dictSort\":0,\"dictType\":\"alarm_level\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:08:23', 7);
INSERT INTO `sys_oper_log` VALUES (146, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"较重\",\"dictSort\":1,\"dictType\":\"alarm_level\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:08:29', 9);
INSERT INTO `sys_oper_log` VALUES (147, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"严重\",\"dictSort\":2,\"dictType\":\"alarm_level\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:08:38', 10);
INSERT INTO `sys_oper_log` VALUES (148, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"设备运行状态\",\"dictType\":\"device_state\",\"params\":{},\"remark\":\"设备运行状态\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:10:11', 8);
INSERT INTO `sys_oper_log` VALUES (149, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"在线\",\"dictSort\":0,\"dictType\":\"device_state\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:10:27', 7);
INSERT INTO `sys_oper_log` VALUES (150, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"离线\",\"dictSort\":1,\"dictType\":\"device_state\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:10:33', 8);
INSERT INTO `sys_oper_log` VALUES (151, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"协议类型\",\"dictType\":\"protocol_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:16:34', 7);
INSERT INTO `sys_oper_log` VALUES (152, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"script\",\"dictSort\":0,\"dictType\":\"protocol_type\",\"dictValue\":\"script\",\"listClass\":\"default\",\"params\":{},\"remark\":\"script\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-05 14:17:01', 8);
INSERT INTO `sys_oper_log` VALUES (153, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"告警级别配置\",\"dictType\":\"alarm_level_conf\",\"params\":{},\"remark\":\"告警级别配置\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 16:53:19', 222);
INSERT INTO `sys_oper_log` VALUES (154, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"一般报警\",\"dictSort\":0,\"dictType\":\"alarm_level_conf\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 16:54:08', 168);
INSERT INTO `sys_oper_log` VALUES (155, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-07 16:54:08\",\"default\":false,\"dictCode\":71,\"dictLabel\":\"一般报警\",\"dictSort\":0,\"dictType\":\"alarm_level_conf\",\"dictValue\":\"0\",\"isDefault\":\"N\",\"listClass\":\"info\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 16:54:17', 178);
INSERT INTO `sys_oper_log` VALUES (156, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"较重报警\",\"dictSort\":1,\"dictType\":\"alarm_level_conf\",\"dictValue\":\"1\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 16:54:30', 353);
INSERT INTO `sys_oper_log` VALUES (157, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"严重报警\",\"dictSort\":2,\"dictType\":\"alarm_level_conf\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 16:54:39', 164);
INSERT INTO `sys_oper_log` VALUES (158, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-07 17:06:53\",\"delFlag\":\"11\",\"params\":{},\"productMarking\":\"v34\",\"productName\":\"ceshi\"}', NULL, 1, '\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n### The error may exist in URL [jar:file:/home/wwwroot/iot.jnsbwl.com/shenbaoiot-admin.jar!/BOOT-INF/lib/shenbaoiot-platform-3.8.5.jar!/mapper/dev/DevProductMapper.xml]\n### The error may involve com.shenbaoiot.dev.mapper.DevProductMapper.insertDevProduct-Inline\n### The error occurred while setting parameters\n### SQL: insert into shenbao_dev_product          ( product_name,             product_marking,                                                                                                                                                                                                                                                                    create_time,                                                    del_flag )           values ( ?,             ?,                                                                                                                                                                                                                                                                    ?,                                                    ? )\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n; Data truncation: Data too long for column \'del_flag\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1', '2023-05-07 17:06:53', 273);
INSERT INTO `sys_oper_log` VALUES (159, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-07 17:07:01\",\"delFlag\":\"11\",\"params\":{},\"productMarking\":\"v34\",\"productName\":\"ceshi\"}', NULL, 1, '\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n### The error may exist in URL [jar:file:/home/wwwroot/iot.jnsbwl.com/shenbaoiot-admin.jar!/BOOT-INF/lib/shenbaoiot-platform-3.8.5.jar!/mapper/dev/DevProductMapper.xml]\n### The error may involve com.shenbaoiot.dev.mapper.DevProductMapper.insertDevProduct-Inline\n### The error occurred while setting parameters\n### SQL: insert into shenbao_dev_product          ( product_name,             product_marking,                                                                                                                                                                                                                                                                    create_time,                                                    del_flag )           values ( ?,             ?,                                                                                                                                                                                                                                                                    ?,                                                    ? )\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n; Data truncation: Data too long for column \'del_flag\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1', '2023-05-07 17:07:02', 13);
INSERT INTO `sys_oper_log` VALUES (160, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"accessName\":\"111\",\"accessProvider\":\"1111\",\"createTime\":\"2023-05-07 17:08:07\",\"delFlag\":\"333\",\"deptId\":11,\"deptName\":\"11\",\"deviceType\":\"0\",\"messageProtocol\":\"111\",\"params\":{},\"productClassifiedId\":11,\"productMarking\":\"3333\",\"productName\":\"测试数据\",\"projectId\":\"11\",\"projectName\":\"11\",\"protocolName\":\"111\",\"remark\":\"打打\",\"storePolicy\":\"ad\",\"storePolicyConfiguration\":\"11\",\"transportProtocol\":\"111\"}', NULL, 1, '\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n### The error may exist in URL [jar:file:/home/wwwroot/iot.jnsbwl.com/shenbaoiot-admin.jar!/BOOT-INF/lib/shenbaoiot-platform-3.8.5.jar!/mapper/dev/DevProductMapper.xml]\n### The error may involve com.shenbaoiot.dev.mapper.DevProductMapper.insertDevProduct-Inline\n### The error occurred while setting parameters\n### SQL: insert into shenbao_dev_product          ( product_name,             product_marking,             product_classified_id,                          device_type,             project_id,             project_name,             dept_id,             dept_name,                                                                 message_protocol,             protocol_name,             transport_protocol,             store_policy_configuration,             access_name,             access_provider,             store_policy,                          create_time,                                       remark,             del_flag )           values ( ?,             ?,             ?,                          ?,             ?,             ?,             ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ? )\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n; Data truncation: Data too long for column \'del_flag\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1', '2023-05-07 17:08:07', 14);
INSERT INTO `sys_oper_log` VALUES (161, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-07 17:10:02\",\"delFlag\":\"222\",\"deviceType\":\"2\",\"params\":{},\"productMarking\":\"wsd2\",\"productName\":\"温湿度1\"}', NULL, 1, '\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n### The error may exist in URL [jar:file:/home/wwwroot/iot.jnsbwl.com/shenbaoiot-admin.jar!/BOOT-INF/lib/shenbaoiot-platform-3.8.5.jar!/mapper/dev/DevProductMapper.xml]\n### The error may involve com.shenbaoiot.dev.mapper.DevProductMapper.insertDevProduct-Inline\n### The error occurred while setting parameters\n### SQL: insert into shenbao_dev_product          ( product_name,             product_marking,                                       device_type,                                                                                                                                                                                                                             create_time,                                                    del_flag )           values ( ?,             ?,                                       ?,                                                                                                                                                                                                                             ?,                                                    ? )\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1\n; Data truncation: Data too long for column \'del_flag\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'del_flag\' at row 1', '2023-05-07 17:10:02', 20);
INSERT INTO `sys_oper_log` VALUES (162, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"1\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/03/11/device-product_20230311110552A001.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"updateTime\":\"2023-05-07 17:12:48\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:12:49', 26);
INSERT INTO `sys_oper_log` VALUES (163, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"常用单位\",\"dictType\":\"common_unit\",\"params\":{},\"remark\":\"常用单位\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:16:45', 617);
INSERT INTO `sys_oper_log` VALUES (164, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"dictName\":\"常用单位\",\"dictType\":\"common_unit\",\"params\":{},\"remark\":\"常用单位\",\"status\":\"0\"}', '{\"msg\":\"新增字典\'常用单位\'失败，字典类型已存在\",\"code\":500}', 0, NULL, '2023-05-07 17:17:05', 143);
INSERT INTO `sys_oper_log` VALUES (165, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"百分比(%)\",\"dictSort\":0,\"dictType\":\"common_unit\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:17:26', 162);
INSERT INTO `sys_oper_log` VALUES (166, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"长度单位:纳米(nm)\",\"dictSort\":1,\"dictType\":\"common_unit\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"长度单位:纳米(nm)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:17:56', 156);
INSERT INTO `sys_oper_log` VALUES (167, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"长度单位:微米(μm)\",\"dictSort\":2,\"dictType\":\"common_unit\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:18:19', 146);
INSERT INTO `sys_oper_log` VALUES (168, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"长度单位:毫米(mm)\",\"dictSort\":3,\"dictType\":\"common_unit\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:18:53', 427);
INSERT INTO `sys_oper_log` VALUES (169, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"长度单位:厘米(cm)\",\"dictSort\":4,\"dictType\":\"common_unit\",\"dictValue\":\"4\",\"listClass\":\"default\",\"params\":{},\"remark\":\"长度单位:厘米(cm)\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:19:24', 142);
INSERT INTO `sys_oper_log` VALUES (170, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"0\",\"attr2\":\"0\",\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-07 17:19:34\",\"delFlag\":\"0\",\"id\":4,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507171913A001.png\",\"productMarking\":\"1111\",\"productName\":\"测试1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:19:34', 28);
INSERT INTO `sys_oper_log` VALUES (171, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"长度单位:米(m)\",\"dictSort\":5,\"dictType\":\"common_unit\",\"dictValue\":\"5\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:19:42', 378);
INSERT INTO `sys_oper_log` VALUES (172, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"长度单位:千米(km)\",\"dictSort\":6,\"dictType\":\"common_unit\",\"dictValue\":\"6\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:19:57', 152);
INSERT INTO `sys_oper_log` VALUES (173, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"1\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507174428A002.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"updateTime\":\"2023-05-07 17:44:30\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:44:30', 18);
INSERT INTO `sys_oper_log` VALUES (174, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:57\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":2,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507174438A003.png\",\"productMarking\":\"ckfwq1\",\"productName\":\"串口服务器1\",\"updateTime\":\"2023-05-07 17:44:40\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:44:40', 18);
INSERT INTO `sys_oper_log` VALUES (175, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:07:37\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":3,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507174447A004.png\",\"productMarking\":\"jzj1\",\"productName\":\"加注机\",\"updateTime\":\"2023-05-07 17:44:49\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 17:44:49', 25);
INSERT INTO `sys_oper_log` VALUES (176, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"0\",\"attr2\":\"0\",\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-07 18:18:23\",\"delFlag\":\"0\",\"id\":5,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507181817A005.png\",\"productMarking\":\"22\",\"productName\":\"测试2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 18:18:23', 25);
INSERT INTO `sys_oper_log` VALUES (177, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr2\":\"2\",\"attr3\":2,\"attr4\":2,\"createTime\":\"2023-05-07 18:36:56\",\"delFlag\":\"2\",\"id\":6,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507183649A006.png\",\"productMarking\":\"22\",\"productName\":\"测试2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 18:36:56', 30);
INSERT INTO `sys_oper_log` VALUES (178, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"1\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"updateTime\":\"2023-05-07 19:07:43\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 19:07:43', 31);
INSERT INTO `sys_oper_log` VALUES (179, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:57\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":2,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190750A008.png\",\"productMarking\":\"ckfwq1\",\"productName\":\"串口服务器1\",\"updateTime\":\"2023-05-07 19:07:52\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 19:07:52', 17);
INSERT INTO `sys_oper_log` VALUES (180, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr2\":\"3\",\"attr3\":3,\"attr4\":3,\"createTime\":\"2023-05-07 20:01:25\",\"delFlag\":\"3\",\"id\":7,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507200120A009.png\",\"productMarking\":\"33\",\"productName\":\"测试3\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 20:01:25', 43);
INSERT INTO `sys_oper_log` VALUES (181, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"4\",\"attr2\":\"4\",\"attr3\":4,\"attr4\":4,\"createTime\":\"2023-05-07 20:01:42\",\"delFlag\":\"4\",\"id\":8,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507200136A010.png\",\"productMarking\":\"44\",\"productName\":\"测试4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 20:01:42', 16);
INSERT INTO `sys_oper_log` VALUES (182, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"5\",\"attr2\":\"5\",\"attr3\":5,\"attr4\":5,\"createTime\":\"2023-05-07 20:01:59\",\"delFlag\":\"5\",\"id\":9,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507200152A011.png\",\"productMarking\":\"5\",\"productName\":\"测试5\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 20:01:59', 43);
INSERT INTO `sys_oper_log` VALUES (183, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"6\",\"attr2\":\"6\",\"attr3\":6,\"attr4\":6,\"createTime\":\"2023-05-07 20:02:16\",\"delFlag\":\"6\",\"id\":10,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507200210A012.png\",\"productMarking\":\"6\",\"productName\":\"测试6\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 20:02:16', 25);
INSERT INTO `sys_oper_log` VALUES (184, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"7\",\"attr2\":\"7\",\"attr3\":7,\"attr4\":7,\"createTime\":\"2023-05-07 20:02:35\",\"delFlag\":\"7\",\"id\":11,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507200227A013.png\",\"productMarking\":\"7\",\"productName\":\"测试7\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 20:02:35', 21);
INSERT INTO `sys_oper_log` VALUES (185, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"8\",\"attr2\":\"8\",\"attr3\":8,\"attr4\":8,\"createTime\":\"2023-05-07 20:02:49\",\"delFlag\":\"8\",\"id\":12,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507200245A014.png\",\"productMarking\":\"8\",\"productName\":\"测试8\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 20:02:49', 21);
INSERT INTO `sys_oper_log` VALUES (186, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"9\",\"attr2\":\"9\",\"attr3\":9,\"attr4\":9,\"createTime\":\"2023-05-07 20:04:11\",\"delFlag\":\"9\",\"id\":13,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/111_20230507200406A015.png\",\"productMarking\":\"9\",\"productName\":\"测试9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-07 20:04:11', 20);
INSERT INTO `sys_oper_log` VALUES (187, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"读写类型\",\"dictType\":\"read_write_type\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 13:35:11', 45);
INSERT INTO `sys_oper_log` VALUES (188, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"只读\",\"dictSort\":0,\"dictType\":\"read_write_type\",\"dictValue\":\"r\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 13:35:29', 43);
INSERT INTO `sys_oper_log` VALUES (189, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"读写\",\"dictSort\":1,\"dictType\":\"read_write_type\",\"dictValue\":\"rw\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 13:35:44', 35);
INSERT INTO `sys_oper_log` VALUES (190, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"是否\",\"dictType\":\"true_false\",\"params\":{},\"remark\":\"是（true），否（false）\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 13:36:39', 33);
INSERT INTO `sys_oper_log` VALUES (191, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"是\",\"dictSort\":0,\"dictType\":\"true_false\",\"dictValue\":\"true\",\"listClass\":\"default\",\"params\":{},\"remark\":\"true\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 13:37:10', 32);
INSERT INTO `sys_oper_log` VALUES (192, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"否\",\"dictSort\":1,\"dictType\":\"true_false\",\"dictValue\":\"false\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 13:37:21', 27);
INSERT INTO `sys_oper_log` VALUES (193, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"model_properties\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 13:44:06', 443);
INSERT INTO `sys_oper_log` VALUES (194, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"model_properties\"}', NULL, 0, NULL, '2023-05-10 13:44:16', 1217);
INSERT INTO `sys_oper_log` VALUES (195, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"properties\",\"className\":\"ModelProperties\",\"columns\":[{\"capJavaField\":\"Identifier\",\"columnComment\":\"属性唯一标识符\",\"columnId\":144,\"columnName\":\"identifier\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 13:44:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"identifier\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"属性名称\",\"columnId\":145,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 13:44:06\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AccessMode\",\"columnComment\":\"属性读写类型\",\"columnId\":146,\"columnName\":\"accessMode\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 13:44:06\",\"dictType\":\"read_write_type\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"accessMode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Required\",\"columnComment\":\"是否是标准功能\",\"columnId\":147,\"columnName\":\"required\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 13:44:06\",\"dictType\":\"true_false\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 14:32:57', 143);
INSERT INTO `sys_oper_log` VALUES (196, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"model_properties\"}', NULL, 0, NULL, '2023-05-10 14:33:02', 358);
INSERT INTO `sys_oper_log` VALUES (197, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_metadata\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 14:46:28', 144);
INSERT INTO `sys_oper_log` VALUES (198, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"metadata\",\"className\":\"Metadata\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":149,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Metadata\",\"columnComment\":\"物模型\",\"columnId\":150,\"columnName\":\"metadata\",\"columnType\":\"longtext\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"metadata\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"WhoBelongsId\",\"columnComment\":\"属于产品或设备的id\",\"columnId\":151,\"columnName\":\"who_belongs_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"whoBelongsId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"WhoBelongs\",\"columnComment\":\"属于谁（0-产品，1-设备）\",\"columnId\":152,\"columnName\":\"who_belongs\",\"columnType\":\"char(2)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"whoBelongs\",\"javaType\":\"Stri', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 14:47:24', 159);
INSERT INTO `sys_oper_log` VALUES (199, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_metadata\"}', NULL, 0, NULL, '2023-05-10 14:47:28', 323);
INSERT INTO `sys_oper_log` VALUES (200, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_metadata\"}', NULL, 0, NULL, '2023-05-10 14:49:05', 209);
INSERT INTO `sys_oper_log` VALUES (201, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"metadata\",\"className\":\"Metadata\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":149,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-05-10 14:47:24\",\"usableColumn\":false},{\"capJavaField\":\"Metadata\",\"columnComment\":\"物模型\",\"columnId\":150,\"columnName\":\"metadata\",\"columnType\":\"longtext\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"metadata\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-05-10 14:47:24\",\"usableColumn\":false},{\"capJavaField\":\"WhoBelongsId\",\"columnComment\":\"属于产品或设备的id\",\"columnId\":151,\"columnName\":\"who_belongs_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"whoBelongsId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-05-10 14:47:24\",\"usableColumn\":false},{\"capJavaField\":\"WhoBelongs\",\"columnComment\":\"属于谁（0-产品，1-设备）\",\"columnId\":152,\"columnName\":\"who_belongs\",\"columnType\":\"char(2)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncr', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 14:54:41', 231);
INSERT INTO `sys_oper_log` VALUES (202, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_metadata\"}', NULL, 0, NULL, '2023-05-10 14:55:05', 212);
INSERT INTO `sys_oper_log` VALUES (203, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"metadata\",\"className\":\"Metadata\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"编号\",\"columnId\":149,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-05-10 14:54:41\",\"usableColumn\":false},{\"capJavaField\":\"Metadata\",\"columnComment\":\"物模型\",\"columnId\":150,\"columnName\":\"metadata\",\"columnType\":\"longtext\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"metadata\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-05-10 14:54:41\",\"usableColumn\":false},{\"capJavaField\":\"WhoBelongsId\",\"columnComment\":\"属于产品或设备的id\",\"columnId\":151,\"columnName\":\"who_belongs_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"whoBelongsId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2023-05-10 14:54:41\",\"usableColumn\":false},{\"capJavaField\":\"WhoBelongs\",\"columnComment\":\"属于谁（0-产品，1-设备）\",\"columnId\":152,\"columnName\":\"who_belongs\",\"columnType\":\"char(2)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-10 14:46:27\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncr', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-10 16:26:42', 451);
INSERT INTO `sys_oper_log` VALUES (204, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_metadata\"}', NULL, 0, NULL, '2023-05-10 16:26:46', 837);
INSERT INTO `sys_oper_log` VALUES (205, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_product\"}', NULL, 0, NULL, '2023-05-10 16:41:42', 925);
INSERT INTO `sys_oper_log` VALUES (206, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"updateTime\":\"2023-05-12 08:13:38\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 08:13:38', 63);
INSERT INTO `sys_oper_log` VALUES (207, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"updateTime\":\"2023-05-12 11:06:34\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 11:06:34', 27);
INSERT INTO `sys_oper_log` VALUES (208, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 11:13:02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 11:13:02', 26);
INSERT INTO `sys_oper_log` VALUES (209, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"正常\",\"dictSort\":1,\"dictType\":\"device_status\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 11:18:42', 100);
INSERT INTO `sys_oper_log` VALUES (210, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"禁用\",\"dictSort\":0,\"dictType\":\"device_status\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 11:18:55', 26);
INSERT INTO `sys_oper_log` VALUES (211, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-12 11:18:55\",\"default\":false,\"dictCode\":86,\"dictLabel\":\"禁用\",\"dictSort\":0,\"dictType\":\"device_status\",\"dictValue\":\"0\",\"isDefault\":\"N\",\"listClass\":\"danger\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 11:19:02', 27);
INSERT INTO `sys_oper_log` VALUES (212, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-12 11:18:42\",\"default\":false,\"dictCode\":85,\"dictLabel\":\"正常\",\"dictSort\":1,\"dictType\":\"device_status\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 11:19:07', 24);
INSERT INTO `sys_oper_log` VALUES (213, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-12 11:18:42\",\"default\":false,\"dictCode\":85,\"dictLabel\":\"启用\",\"dictSort\":1,\"dictType\":\"device_status\",\"dictValue\":\"1\",\"isDefault\":\"N\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 15:33:24', 30);
INSERT INTO `sys_oper_log` VALUES (214, '代码生成', 6, 'com.shenbaoiot.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"shenbao_dev_info\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 15:40:02', 606);
INSERT INTO `sys_oper_log` VALUES (215, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"ShenbaoDevInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":164,\"columnName\":\"id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DevName\",\"columnComment\":\"设备名称\",\"columnId\":165,\"columnName\":\"dev_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"devName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DevMarking\",\"columnComment\":\"设备唯一标识\",\"columnId\":166,\"columnName\":\"dev_marking\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"devMarking\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProductId\",\"columnComment\":\"所属产品ID\",\"columnId\":167,\"columnName\":\"product_id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"produc', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 15:41:43', 214);
INSERT INTO `sys_oper_log` VALUES (216, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"devinfo\",\"className\":\"ShenbaoDevInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":164,\"columnName\":\"id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"updateTime\":\"2023-05-12 15:41:43\",\"usableColumn\":false},{\"capJavaField\":\"DevName\",\"columnComment\":\"设备名称\",\"columnId\":165,\"columnName\":\"dev_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"devName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"updateTime\":\"2023-05-12 15:41:43\",\"usableColumn\":false},{\"capJavaField\":\"DevMarking\",\"columnComment\":\"设备唯一标识\",\"columnId\":166,\"columnName\":\"dev_marking\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"devMarking\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"updateTime\":\"2023-05-12 15:41:43\",\"usableColumn\":false},{\"capJavaField\":\"ProductId\",\"columnComment\":\"所属产品ID\",\"columnId\":167,\"columnName\":\"product_id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 15:45:01', 191);
INSERT INTO `sys_oper_log` VALUES (217, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_info\"}', NULL, 0, NULL, '2023-05-12 15:45:07', 475);
INSERT INTO `sys_oper_log` VALUES (218, '代码生成', 2, 'com.shenbaoiot.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"devinfo\",\"className\":\"DevInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"ID\",\"columnId\":164,\"columnName\":\"id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"updateTime\":\"2023-05-12 15:45:01\",\"usableColumn\":false},{\"capJavaField\":\"DevName\",\"columnComment\":\"设备名称\",\"columnId\":165,\"columnName\":\"dev_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"devName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"updateTime\":\"2023-05-12 15:45:01\",\"usableColumn\":false},{\"capJavaField\":\"DevMarking\",\"columnComment\":\"设备唯一标识\",\"columnId\":166,\"columnName\":\"dev_marking\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"devMarking\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":11,\"updateBy\":\"\",\"updateTime\":\"2023-05-12 15:45:01\",\"usableColumn\":false},{\"capJavaField\":\"ProductId\",\"columnComment\":\"所属产品ID\",\"columnId\":167,\"columnName\":\"product_id\",\"columnType\":\"bigint(64)\",\"createBy\":\"admin\",\"createTime\":\"2023-05-12 15:40:02\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"i', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:07:19', 114);
INSERT INTO `sys_oper_log` VALUES (219, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_info\"}', NULL, 0, NULL, '2023-05-12 16:07:23', 403);
INSERT INTO `sys_oper_log` VALUES (220, '菜单管理', 2, 'com.shenbaoiot.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"dev/devinfo/index\",\"createTime\":\"2023-05-12 16:08:59\",\"icon\":\"nested\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1106,\"menuName\":\"设备管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1068,\"path\":\"devinfo\",\"perms\":\"dev:devinfo:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:30:27', 139);
INSERT INTO `sys_oper_log` VALUES (221, '设备管理', 1, 'com.shenbaoiot.dev.controller.DevInfoController.add()', 'POST', 1, 'admin', NULL, '/dev/devinfo', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-12 16:31:04\",\"delFlag\":\"0\",\"devMarking\":\"wsd1\",\"devName\":\"温湿度1\",\"devState\":1,\"id\":1,\"params\":{},\"productId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:31:04', 89);
INSERT INTO `sys_oper_log` VALUES (222, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_product\"}', NULL, 0, NULL, '2023-05-12 16:36:23', 1853);
INSERT INTO `sys_oper_log` VALUES (223, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:47:28\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:47:28', 68);
INSERT INTO `sys_oper_log` VALUES (224, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:47:37\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:47:37', 22);
INSERT INTO `sys_oper_log` VALUES (225, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:49:20\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:49:20', 27);
INSERT INTO `sys_oper_log` VALUES (226, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:50:01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:50:01', 36);
INSERT INTO `sys_oper_log` VALUES (227, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-12 16:50:57\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:50:57', 20);
INSERT INTO `sys_oper_log` VALUES (228, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:51:07\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:51:07', 31);
INSERT INTO `sys_oper_log` VALUES (229, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-12 16:51:49\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:51:49', 30);
INSERT INTO `sys_oper_log` VALUES (230, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:51:54\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:51:54', 23);
INSERT INTO `sys_oper_log` VALUES (231, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-12 16:51:59\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:51:59', 25);
INSERT INTO `sys_oper_log` VALUES (232, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:52:01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:52:01', 21);
INSERT INTO `sys_oper_log` VALUES (233, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-12 16:52:04\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:52:04', 28);
INSERT INTO `sys_oper_log` VALUES (234, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:52:06\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:52:06', 19);
INSERT INTO `sys_oper_log` VALUES (235, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-12 16:53:11\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:53:11', 56);
INSERT INTO `sys_oper_log` VALUES (236, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:53:55\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:53:55', 26);
INSERT INTO `sys_oper_log` VALUES (237, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-12 16:54:06\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:54:06', 26);
INSERT INTO `sys_oper_log` VALUES (238, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:54:11\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:54:11', 33);
INSERT INTO `sys_oper_log` VALUES (239, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-12 16:54:22\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:54:22', 57);
INSERT INTO `sys_oper_log` VALUES (240, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:57\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":2,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190750A008.png\",\"productMarking\":\"ckfwq1\",\"productName\":\"串口服务器1\",\"productState\":1,\"updateTime\":\"2023-05-12 16:54:57\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:54:57', 19);
INSERT INTO `sys_oper_log` VALUES (241, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-12 16:55:01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-12 16:55:01', 25);
INSERT INTO `sys_oper_log` VALUES (242, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_metadata\"}', NULL, 0, NULL, '2023-05-12 16:59:15', 283);
INSERT INTO `sys_oper_log` VALUES (243, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_dev_product\"}', NULL, 0, NULL, '2023-05-15 14:10:16', 293);
INSERT INTO `sys_oper_log` VALUES (244, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr1\":\"5\",\"attr2\":\"5\",\"attr3\":5,\"attr4\":5,\"createTime\":\"2023-05-07 20:02:00\",\"delFlag\":\"5\",\"deviceType\":\"0\",\"id\":9,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507200152A011.png\",\"productMarking\":\"5\",\"productName\":\"测试5\",\"productState\":0,\"updateTime\":\"2023-05-15 14:18:18\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-15 14:18:18', 22);
INSERT INTO `sys_oper_log` VALUES (245, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-15 14:18:51\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-15 14:18:51', 30);
INSERT INTO `sys_oper_log` VALUES (246, '设备类型', 2, 'com.shenbaoiot.dev.controller.DevClassifiedController.edit()', 'PUT', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"ancestors\":\"\",\"attr3\":0,\"attr4\":0,\"children\":[],\"classifiedCode\":\"M_TYPE_000\",\"classifiedName\":\"设备分类\",\"createBy\":\"\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"id\":200,\"orderNum\":0,\"params\":{},\"parentId\":0,\"remark\":\"\",\"updateBy\":\"\",\"updateTime\":\"2023-05-15 14:20:17\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-15 14:20:17', 44);
INSERT INTO `sys_oper_log` VALUES (247, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-15 15:49:17\",\"deviceType\":\"0\",\"id\":14,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/15/企业微信截图_20230512103620_20230515154915A001.png\",\"productMarking\":\"cp1\",\"productName\":\"产品测试1\",\"productState\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-15 15:49:17', 30);
INSERT INTO `sys_oper_log` VALUES (248, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/13', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-15 15:49:32', 21);
INSERT INTO `sys_oper_log` VALUES (249, '产品管理', 5, 'com.shenbaoiot.dev.controller.DevProductController.export()', 'POST', 1, 'admin', NULL, '/dev/product/export', '127.0.0.1', '内网IP', '{\"params\":{}}', NULL, 0, NULL, '2023-05-15 15:50:44', 3400);
INSERT INTO `sys_oper_log` VALUES (250, '设备类型', 1, 'com.shenbaoiot.dev.controller.DevClassifiedController.add()', 'POST', 1, 'admin', NULL, '/dev/classified', '127.0.0.1', '内网IP', '{\"children\":[],\"classifiedCode\":\"M_TYPE_200\",\"classifiedName\":\"温湿度\",\"createTime\":\"2023-05-16 10:13:51\",\"deviceType\":\"0\",\"enableFlag\":\"1\",\"id\":205,\"params\":{},\"parentId\":0}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:13:51', 17);
INSERT INTO `sys_oper_log` VALUES (251, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/11', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:26', 26);
INSERT INTO `sys_oper_log` VALUES (252, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/12', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:30', 17);
INSERT INTO `sys_oper_log` VALUES (253, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/10', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:31', 30);
INSERT INTO `sys_oper_log` VALUES (254, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/8', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:35', 13);
INSERT INTO `sys_oper_log` VALUES (255, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/7', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:37', 47);
INSERT INTO `sys_oper_log` VALUES (256, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/6', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:39', 12);
INSERT INTO `sys_oper_log` VALUES (257, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/5', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:40', 17);
INSERT INTO `sys_oper_log` VALUES (258, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/4', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:17:43', 16);
INSERT INTO `sys_oper_log` VALUES (259, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-16 10:18:18\",\"deviceType\":\"0\",\"id\":15,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/16/设备_20230507200245A014_20230516101810A002.png\",\"productMarking\":\"6\",\"productName\":\"6\",\"productState\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-16 10:18:18', 26);
INSERT INTO `sys_oper_log` VALUES (260, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 11:08:28\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 11:08:28', 17);
INSERT INTO `sys_oper_log` VALUES (261, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 16:04:35\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:04:35', 20);
INSERT INTO `sys_oper_log` VALUES (262, '代码生成', 8, 'com.shenbaoiot.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"shenbao_project\"}', NULL, 0, NULL, '2023-05-31 16:05:17', 281);
INSERT INTO `sys_oper_log` VALUES (263, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 16:13:33\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:13:33', 18);
INSERT INTO `sys_oper_log` VALUES (264, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 16:13:38\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:13:38', 22);
INSERT INTO `sys_oper_log` VALUES (265, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 16:13:41\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:13:41', 15);
INSERT INTO `sys_oper_log` VALUES (266, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 16:13:43\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:13:44', 30);
INSERT INTO `sys_oper_log` VALUES (267, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 16:13:52\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:13:52', 15);
INSERT INTO `sys_oper_log` VALUES (268, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:57\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":2,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190750A008.png\",\"productMarking\":\"ckfwq1\",\"productName\":\"串口服务器1\",\"productState\":0,\"updateTime\":\"2023-05-31 16:14:25\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:14:25', 29);
INSERT INTO `sys_oper_log` VALUES (269, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:57\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":2,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190750A008.png\",\"productMarking\":\"ckfwq1\",\"productName\":\"串口服务器1\",\"productState\":1,\"updateTime\":\"2023-05-31 16:14:35\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:14:35', 23);
INSERT INTO `sys_oper_log` VALUES (270, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 16:21:02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:21:02', 17);
INSERT INTO `sys_oper_log` VALUES (271, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-31 16:22:40\",\"deviceType\":\"0\",\"id\":16,\"params\":{},\"productMarking\":\"1\",\"productName\":\"1\",\"productState\":0}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:22:40', 60);
INSERT INTO `sys_oper_log` VALUES (272, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-31 16:22:40\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":16,\"params\":{},\"productMarking\":\"1\",\"productName\":\"1\",\"productState\":1,\"updateTime\":\"2023-05-31 16:22:54\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:22:54', 16);
INSERT INTO `sys_oper_log` VALUES (273, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-31 16:22:40\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":16,\"params\":{},\"productMarking\":\"1\",\"productName\":\"1\",\"productState\":0,\"updateTime\":\"2023-05-31 16:23:00\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:23:00', 25);
INSERT INTO `sys_oper_log` VALUES (274, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 16:26:37\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:26:37', 16);
INSERT INTO `sys_oper_log` VALUES (275, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 16:30:07\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:30:07', 17);
INSERT INTO `sys_oper_log` VALUES (276, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 16:40:44\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:40:44', 15);
INSERT INTO `sys_oper_log` VALUES (277, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:57\",\"delFlag\":\"0\",\"deviceType\":\"2\",\"id\":2,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190750A008.png\",\"productMarking\":\"ckfwq1\",\"productName\":\"串口服务器1\",\"productState\":0,\"updateTime\":\"2023-05-31 16:40:51\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:40:51', 18);
INSERT INTO `sys_oper_log` VALUES (278, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 16:50:45\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 16:50:45', 17);
INSERT INTO `sys_oper_log` VALUES (279, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 19:28:39\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:28:39', 122);
INSERT INTO `sys_oper_log` VALUES (280, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 19:28:47\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:28:47', 23);
INSERT INTO `sys_oper_log` VALUES (281, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 19:28:53\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:28:53', 27);
INSERT INTO `sys_oper_log` VALUES (282, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 19:30:26\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:30:26', 29);
INSERT INTO `sys_oper_log` VALUES (283, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 19:32:51\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:32:51', 24);
INSERT INTO `sys_oper_log` VALUES (284, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 19:37:07\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:37:07', 25);
INSERT INTO `sys_oper_log` VALUES (285, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-05-31 19:42:40\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:42:40', 123);
INSERT INTO `sys_oper_log` VALUES (286, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 19:48:19\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 19:48:19', 27);
INSERT INTO `sys_oper_log` VALUES (287, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-31 20:13:38\",\"deviceType\":\"0\",\"id\":17,\"params\":{},\"productClassifiedId\":201,\"productMarking\":\"1\",\"productName\":\"测试所属品类\",\"productState\":1,\"remark\":\"测试所属品类\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:13:38', 103);
INSERT INTO `sys_oper_log` VALUES (288, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-31 20:13:38\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":17,\"params\":{},\"productClassifiedId\":201,\"productMarking\":\"1\",\"productName\":\"测试所属品类\",\"productState\":0,\"remark\":\"测试所属品类\",\"updateTime\":\"2023-05-31 20:13:46\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:13:46', 24);
INSERT INTO `sys_oper_log` VALUES (289, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-31 20:13:38\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":17,\"params\":{},\"productClassifiedId\":201,\"productMarking\":\"1\",\"productName\":\"测试所属品类\",\"productState\":1,\"remark\":\"测试所属品类\",\"updateTime\":\"2023-05-31 20:13:49\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:13:49', 17);
INSERT INTO `sys_oper_log` VALUES (290, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":201,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 20:22:07\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:22:07', 20);
INSERT INTO `sys_oper_log` VALUES (291, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-05-31 20:22:22\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:22:22', 21);
INSERT INTO `sys_oper_log` VALUES (292, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/17', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:25:21', 65);
INSERT INTO `sys_oper_log` VALUES (293, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-05-31 20:25:44\",\"deviceType\":\"0\",\"id\":18,\"params\":{},\"productClassifiedId\":201,\"productMarking\":\"20\",\"productName\":\"测试所属分类\",\"productState\":0}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:25:44', 21);
INSERT INTO `sys_oper_log` VALUES (294, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-31 20:25:44\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":18,\"params\":{},\"productClassifiedId\":204,\"productMarking\":\"20\",\"productName\":\"测试所属分类\",\"productState\":0,\"updateTime\":\"2023-05-31 20:25:52\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-05-31 20:25:52', 20);
INSERT INTO `sys_oper_log` VALUES (295, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-06-01 08:49:06\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 08:49:07', 239);
INSERT INTO `sys_oper_log` VALUES (296, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-06-01 08:49:10\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 08:49:10', 17);
INSERT INTO `sys_oper_log` VALUES (297, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-06-01 08:49:21\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 08:49:21', 21);
INSERT INTO `sys_oper_log` VALUES (298, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-06-01 08:50:16\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 08:50:16', 19);
INSERT INTO `sys_oper_log` VALUES (299, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-05-31 16:22:40\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":16,\"params\":{},\"productMarking\":\"1\",\"productName\":\"1\",\"productState\":1,\"updateTime\":\"2023-06-01 08:50:40\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 08:50:40', 18);
INSERT INTO `sys_oper_log` VALUES (300, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-06-01 09:00:35\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:00:35', 572);
INSERT INTO `sys_oper_log` VALUES (301, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-06-01 09:00:40\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:00:40', 24);
INSERT INTO `sys_oper_log` VALUES (302, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-06-01 09:04:50\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:04:50', 22);
INSERT INTO `sys_oper_log` VALUES (303, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-06-01 09:08:54\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:08:54', 29);
INSERT INTO `sys_oper_log` VALUES (304, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd1\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-06-01 09:09:05\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:09:05', 21);
INSERT INTO `sys_oper_log` VALUES (305, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/18', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:09:11', 15);
INSERT INTO `sys_oper_log` VALUES (306, '产品管理', 3, 'com.shenbaoiot.dev.controller.DevProductController.remove()', 'DELETE', 1, 'admin', NULL, '/dev/product/16', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:09:13', 249);
INSERT INTO `sys_oper_log` VALUES (307, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/05/07/设备_20230507190741A007.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd12\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-06-01 09:17:19\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 09:17:19', 22);
INSERT INTO `sys_oper_log` VALUES (308, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 13:58:22\",\"default\":false,\"dictCode\":48,\"dictLabel\":\"float(单精度浮点型)\",\"dictSort\":2,\"dictType\":\"data_type\",\"dictValue\":\"float\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"float(单精度浮点型)\\n\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 11:25:54', 33);
INSERT INTO `sys_oper_log` VALUES (309, '字典数据', 2, 'com.shenbaoiot.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2023-05-05 13:59:32\",\"default\":false,\"dictCode\":51,\"dictLabel\":\"boolean(布尔型)\",\"dictSort\":5,\"dictType\":\"data_type\",\"dictValue\":\"boolean\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"bool(布尔型)\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 15:35:15', 30);
INSERT INTO `sys_oper_log` VALUES (310, '字典类型', 1, 'com.shenbaoiot.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"文件类型\",\"dictType\":\"file_type\",\"params\":{},\"remark\":\"文件类型\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 17:14:23', 77);
INSERT INTO `sys_oper_log` VALUES (311, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"URL(链接)\",\"dictSort\":0,\"dictType\":\"file_type\",\"dictValue\":\"url\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 17:15:01', 32);
INSERT INTO `sys_oper_log` VALUES (312, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"Base64(Base64编码)\",\"dictSort\":1,\"dictType\":\"file_type\",\"dictValue\":\"base64\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 17:15:28', 34);
INSERT INTO `sys_oper_log` VALUES (313, '字典数据', 1, 'com.shenbaoiot.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"binary\",\"dictSort\":3,\"dictType\":\"file_type\",\"dictValue\":\"binary\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-01 17:15:46', 26);
INSERT INTO `sys_oper_log` VALUES (314, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEdit()', 'POST', 1, 'admin', NULL, '/dev/tsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"111\\\",\\\"name\\\":\\\"111\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"1\\\",\\\"unit\\\":\\\"1\\\",\\\"unitName\\\":\\\"1\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-02 13:28:09', 56);
INSERT INTO `sys_oper_log` VALUES (315, '产品管理', 1, 'com.shenbaoiot.dev.controller.DevProductController.add()', 'POST', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"createTime\":\"2023-06-05 15:38:33\",\"deviceType\":\"0\",\"id\":19,\"params\":{},\"photoUrl\":\"/profile/upload/2023/06/05/微信图片_20230424173539_20230605153831A001.png\",\"productClassifiedId\":201,\"productMarking\":\"11\",\"productName\":\"11\",\"productState\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-05 15:27:01', 106);
INSERT INTO `sys_oper_log` VALUES (316, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/06/12/微信图片_20230424173539_20230612140824A001.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd12\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-06-12 14:08:26\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-12 13:56:46', 79);
INSERT INTO `sys_oper_log` VALUES (317, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/06/12/微信图片_20230424173539_20230612140824A001.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd12\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-06-12 14:08:35\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-12 13:56:55', 56);
INSERT INTO `sys_oper_log` VALUES (318, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/06/12/微信图片_20230424173539_20230612140824A001.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd12\",\"productName\":\"温湿度\",\"productState\":0,\"updateTime\":\"2023-06-12 14:08:43\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-12 13:57:03', 62);
INSERT INTO `sys_oper_log` VALUES (319, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"aa\\\",\\\"name\\\":\\\"aa\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"aa\\\",\\\"max\\\":\\\"a\\\",\\\"unit\\\":\\\"a\\\",\\\"unitName\\\":\\\"a\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":1}', 0, NULL, '2023-06-12 15:55:57', 39);
INSERT INTO `sys_oper_log` VALUES (320, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-12 16:07:37\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-12 15:55:57', 54);
INSERT INTO `sys_oper_log` VALUES (321, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-12 16:07:56\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-12 15:56:17', 1334);
INSERT INTO `sys_oper_log` VALUES (322, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"aa\\\",\\\"name\\\":\\\"aa\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"aa\\\",\\\"max\\\":\\\"a\\\",\\\"unit\\\":\\\"a\\\",\\\"unitName\\\":\\\"a\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":1}', 0, NULL, '2023-06-12 15:56:45', 37721);
INSERT INTO `sys_oper_log` VALUES (323, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-12 16:09:03\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-12 15:57:26', 2497);
INSERT INTO `sys_oper_log` VALUES (324, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"aa\\\",\\\"name\\\":\\\"aa\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"aa\\\",\\\"max\\\":\\\"a\\\",\\\"unit\\\":\\\"a\\\",\\\"unitName\\\":\\\"a\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":1}', 0, NULL, '2023-06-12 15:57:26', 2522);
INSERT INTO `sys_oper_log` VALUES (325, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"aa\\\",\\\"name\\\":\\\"aa\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"aa\\\",\\\"max\\\":\\\"a\\\",\\\"unit\\\":\\\"a\\\",\\\"unitName\\\":\\\"a\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":1}', 0, NULL, '2023-06-12 15:59:08', 46637);
INSERT INTO `sys_oper_log` VALUES (326, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-12 16:10:03\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-12 15:59:08', 44736);
INSERT INTO `sys_oper_log` VALUES (327, '产品管理', 2, 'com.shenbaoiot.dev.controller.DevProductController.edit()', 'PUT', 1, 'admin', NULL, '/dev/product', '127.0.0.1', '内网IP', '{\"attr3\":0,\"attr4\":0,\"createTime\":\"2023-03-11 11:06:09\",\"delFlag\":\"0\",\"deviceType\":\"0\",\"id\":1,\"params\":{},\"photoUrl\":\"/profile/upload/2023/06/12/微信图片_20230424173539_20230612140824A001.png\",\"productClassifiedId\":202,\"productMarking\":\"wsd12\",\"productName\":\"温湿度\",\"productState\":1,\"updateTime\":\"2023-06-26 09:34:15\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-06-26 09:22:12', 78);
INSERT INTO `sys_oper_log` VALUES (328, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"1\\\",\\\"name\\\":\\\"1\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"1\\\",\\\"unit\\\":\\\"1\\\",\\\"unitName\\\":\\\"1\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":1}', 0, NULL, '2023-06-26 11:01:54', 5436624);
INSERT INTO `sys_oper_log` VALUES (329, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-26 09:43:19\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-26 11:01:54', 5436624);
INSERT INTO `sys_oper_log` VALUES (330, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"wendu\\\",\\\"name\\\":\\\"温度\\\",\\\"accessMode\\\":\\\"rw\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"10\\\",\\\"unit\\\":\\\"摄氏度\\\",\\\"unitName\\\":\\\"摄氏度\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', NULL, 1, '', '2023-06-26 11:37:54', 89);
INSERT INTO `sys_oper_log` VALUES (331, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-26 11:49:56\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-26 11:37:54', 68);
INSERT INTO `sys_oper_log` VALUES (332, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-26 11:50:14\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-26 11:38:56', 58);
INSERT INTO `sys_oper_log` VALUES (333, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"wendu\\\",\\\"name\\\":\\\"温度\\\",\\\"accessMode\\\":\\\"rw\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"10\\\",\\\"unit\\\":\\\"摄氏度\\\",\\\"unitName\\\":\\\"摄氏度\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', NULL, 1, '', '2023-06-26 11:39:07', 56237);
INSERT INTO `sys_oper_log` VALUES (334, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-26 11:51:20\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-26 11:39:20', 60);
INSERT INTO `sys_oper_log` VALUES (335, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"wendu\\\",\\\"name\\\":\\\"温度\\\",\\\"accessMode\\\":\\\"rw\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"10\\\",\\\"unit\\\":\\\"摄氏度\\\",\\\"unitName\\\":\\\"摄氏度\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', NULL, 1, '', '2023-06-26 11:41:58', 161314);
INSERT INTO `sys_oper_log` VALUES (336, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"wd\\\",\\\"name\\\":\\\"温度\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"10\\\",\\\"unit\\\":\\\"度\\\",\\\"unitName\\\":\\\"摄氏度\\\",\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":1}', 0, NULL, '2023-06-26 13:44:02', 128);
INSERT INTO `sys_oper_log` VALUES (337, '物模型信息', 2, 'com.shenbaoiot.dev.controller.MetadataController.edit()', 'PUT', 1, 'admin', NULL, '/dev/metadata', '127.0.0.1', '内网IP', '{\"params\":{},\"updateTime\":\"2023-06-26 13:56:05\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2023-06-26 13:44:03', 59);
INSERT INTO `sys_oper_log` VALUES (338, '物模型信息', 1, 'com.shenbaoiot.dev.controller.TSLController.addEitTsl()', 'POST', 1, 'admin', NULL, '/dev/tsl/addEitTsl', '127.0.0.1', '内网IP', '{\"param\":\"{\\\"identifier\\\":\\\"sd\\\",\\\"name\\\":\\\"湿度\\\",\\\"accessMode\\\":\\\"r\\\",\\\"required\\\":null,\\\"dataType\\\":{\\\"type\\\":\\\"int\\\",\\\"specs\\\":{\\\"min\\\":\\\"1\\\",\\\"max\\\":\\\"1\\\",\\\"unit\\\":null,\\\"unitName\\\":null,\\\"size\\\":null,\\\"step\\\":null,\\\"length\\\":null,\\\"timeFormat\\\":null,\\\"trueText\\\":null,\\\"trueValue\\\":null,\\\"falseText\\\":null,\\\"falseValue\\\":null,\\\"enumdata\\\":[{\\\"text\\\":\\\"\\\",\\\"value\\\":\\\"\\\"}],\\\"item\\\":{\\\"type\\\":null}}}}\",\"params\":{},\"type\":\"properties\",\"whoBelongs\":\"0\",\"whoBelongsId\":1}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":1}', 0, NULL, '2023-06-26 13:48:33', 166);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2023-03-01 09:02:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2023-03-01 09:02:44', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2023-03-01 09:02:44', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 117);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2023-07-06 11:29:54', 'admin', '2023-03-01 09:02:44', '', '2023-07-06 11:17:36', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2023-03-01 09:02:44', 'admin', '2023-03-01 09:02:44', '', NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
