package com.shenbaoiot.openapi.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/2 15:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEsDTO {

    private String name;

    private Integer age;
}
