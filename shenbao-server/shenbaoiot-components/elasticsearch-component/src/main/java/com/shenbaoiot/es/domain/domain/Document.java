package com.shenbaoiot.openapi.domain.domain;

import lombok.Data;

/**
 * @description:
 * @author: black tea
 * @date: 2022/3/15 16:12
 */
@Data
public class Document {
    /**
     * es中的唯一id
     */
    private String id;
    /**
     * 文档标题
     */
    private String title;
    /**
     * 文档内容
     */
    private String content;
}
