package com.shenbaoiot.openapi.domain.vo;

import com.shenbaoiot.openapi.domain.dto.DemoEsDTO;
import com.shenbaoiot.openapi.domain.dto.RequestSearchAfterPage;
import lombok.Data;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/9 13:58
 */
@Data
public class DemoEsVo extends DemoEsDTO {

    /** 上一次查询最后一个文档的sort,用于下也一个查询,只用于 {@link RequestSearchAfterPage} **/
    private Object[] sort;

}
