package com.shenbaoiot.openapi.service.strategy;

import com.shenbaoiot.openapi.domain.dto.PageRequest;
import org.elasticsearch.action.search.SearchRequest;

import java.io.IOException;
import java.util.List;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/7 13:28
 */
public class RequestPageContext<T> {

    private ESRequestPageStrategy<T> esRequestPageStrategy;

    private RequestPageContext(){

    }

    public RequestPageContext(ESRequestPageStrategy<T> esRequestPageStrategy) {
        this.esRequestPageStrategy = esRequestPageStrategy;
    }

    public List<T> list(Class<T> var1, SearchRequest searchRequest, PageRequest pageRequest) throws IOException {
        List<T> list = esRequestPageStrategy.list(var1,searchRequest,pageRequest);
        return list;
    }
}
