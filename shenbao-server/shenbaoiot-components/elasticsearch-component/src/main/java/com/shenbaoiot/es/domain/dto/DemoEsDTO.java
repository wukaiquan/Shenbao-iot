package com.shenbaoiot.openapi.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/2 15:39
 */
@Document(indexName="demo")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DemoEsDTO {

    @Id
    private Long id;

    private String name;

    private String des;

    private Integer number;
}
