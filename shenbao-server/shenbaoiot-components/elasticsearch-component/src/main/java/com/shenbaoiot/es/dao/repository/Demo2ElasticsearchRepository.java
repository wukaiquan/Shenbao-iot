package com.shenbaoiot.openapi.dao.repository;

import com.shenbaoiot.openapi.domain.dto.Demo2EsDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/10 15:24
 */
@Repository
public interface Demo2ElasticsearchRepository extends ElasticsearchRepository<Demo2EsDTO,String> {

    List<Demo2EsDTO> getByDes(String des);

    void deleteByDes(String des);

    List<Demo2EsDTO> getByUserEsDTO_NameLike(String name);

}
