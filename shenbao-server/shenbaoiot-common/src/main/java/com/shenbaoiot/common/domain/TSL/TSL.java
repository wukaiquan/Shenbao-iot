package com.shenbaoiot.common.domain.TSL;

import lombok.Data;

import java.util.List;
@Data
public class TSL {
    private static final long serialVersionUID = 1L;
    //物模型结构定义的访问URL。
    private String schema;
    //当前产品的ProductKey。
    private Profile profile;
    //属性
    private List<Properties> properties;
    //事件
    private List<Events> events;
    //服务
    private List<Services> services;
    //出参
    private List<OutputData> outputData;

}
