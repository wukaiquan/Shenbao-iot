package com.shenbaoiot.common.domain.TSL;

import lombok.Data;

@Data
public class DataType {
    private static final long serialVersionUID = 1L;
    //属性类型
    private String type;
    private Specs specs;
}
