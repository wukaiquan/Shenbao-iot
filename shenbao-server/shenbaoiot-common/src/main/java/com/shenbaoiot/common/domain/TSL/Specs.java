package com.shenbaoiot.common.domain.TSL;

import lombok.Data;

@Data
public class Specs {
    private static final long serialVersionUID = 1L;
    //参数最小值（int、float、double类型特有）
    private String min;
    //参数最大值（int、float、double类型特有）。
    private String max;
    //属性单位（int、float、double类型特有，非必填）。
    private String unit;
    //单位名称（int、float、double类型特有，非必填）。
    private String unitName;
    //数组元素的个数，最大512（array类型特有）。
    private String size;
    //步长（text、enum类型无此参数）。
    private String step;
    //数据长度，最大10240（text类型特有）。
    private String length;
    //布尔值
    private String trueText;
    private String trueValue;
    private String falseText;
    private String falseValue;
}
