package com.shenbaoiot.common.domain.TSL;

import lombok.Data;

@Data
public class Properties {
    private static final long serialVersionUID = 1L;
    //属性唯一标识符。
    private String identifier;
    //属性名称
    private String name;
    //属性读写类型：只读（r）或读写（rw）
    private String accessMode;
    //是否是标准功能的必选属性：是（true），否（false）
    private boolean required;
    //属性类型
    private String dataType;
}
