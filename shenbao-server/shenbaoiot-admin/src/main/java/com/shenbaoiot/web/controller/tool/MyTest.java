package com.shenbaoiot.web.controller.tool;

import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mytest/test")
public class MyTest extends BaseController {

    @ApiOperation("获取用户列表")
    @GetMapping("/list")
    public R userList() {
        return R.ok("1111111111111111111111");
    }


}
