package com.shenbaoiot.basics.mapper;

import java.util.List;
import com.shenbaoiot.basics.domain.ShenbaoProject;

/**
 * 项目管理Mapper接口
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-10
 */
public interface ShenbaoProjectMapper 
{
    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    public ShenbaoProject selectShenbaoProjectById(Long id);

    /**
     * 查询项目管理列表
     * 
     * @param shenbaoProject 项目管理
     * @return 项目管理集合
     */
    public List<ShenbaoProject> selectShenbaoProjectList(ShenbaoProject shenbaoProject);

    /**
     * 新增项目管理
     * 
     * @param shenbaoProject 项目管理
     * @return 结果
     */
    public int insertShenbaoProject(ShenbaoProject shenbaoProject);

    /**
     * 修改项目管理
     * 
     * @param shenbaoProject 项目管理
     * @return 结果
     */
    public int updateShenbaoProject(ShenbaoProject shenbaoProject);

    /**
     * 删除项目管理
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    public int deleteShenbaoProjectById(Long id);

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteShenbaoProjectByIds(Long[] ids);
}
