package com.shenbaoiot.basics.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shenbaoiot.common.annotation.Log;
import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.AjaxResult;
import com.shenbaoiot.common.enums.BusinessType;
import com.shenbaoiot.basics.domain.ShenbaoProject;
import com.shenbaoiot.basics.service.IShenbaoProjectService;
import com.shenbaoiot.common.utils.poi.ExcelUtil;
import com.shenbaoiot.common.core.page.TableDataInfo;

/**
 * 项目管理Controller
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-10
 */
@RestController
@RequestMapping("/basics/project")
public class ShenbaoProjectController extends BaseController
{
    @Autowired
    private IShenbaoProjectService shenbaoProjectService;

    /**
     * 查询项目管理列表
     */
    @PreAuthorize("@ss.hasPermi('basics:project:list')")
    @GetMapping("/list")
    public TableDataInfo list(ShenbaoProject shenbaoProject)
    {
        startPage();
        List<ShenbaoProject> list = shenbaoProjectService.selectShenbaoProjectList(shenbaoProject);
        return getDataTable(list);
    }

    /**
     * 导出项目管理列表
     */
    @PreAuthorize("@ss.hasPermi('basics:project:export')")
    @Log(title = "项目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ShenbaoProject shenbaoProject)
    {
        List<ShenbaoProject> list = shenbaoProjectService.selectShenbaoProjectList(shenbaoProject);
        ExcelUtil<ShenbaoProject> util = new ExcelUtil<ShenbaoProject>(ShenbaoProject.class);
        util.exportExcel(response, list, "项目管理数据");
    }

    /**
     * 获取项目管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('basics:project:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(shenbaoProjectService.selectShenbaoProjectById(id));
    }

    /**
     * 新增项目管理
     */
    @PreAuthorize("@ss.hasPermi('basics:project:add')")
    @Log(title = "项目管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ShenbaoProject shenbaoProject)
    {
        return toAjax(shenbaoProjectService.insertShenbaoProject(shenbaoProject));
    }

    /**
     * 修改项目管理
     */
    @PreAuthorize("@ss.hasPermi('basics:project:edit')")
    @Log(title = "项目管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ShenbaoProject shenbaoProject)
    {
        return toAjax(shenbaoProjectService.updateShenbaoProject(shenbaoProject));
    }

    /**
     * 删除项目管理
     */
    @PreAuthorize("@ss.hasPermi('basics:project:remove')")
    @Log(title = "项目管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(shenbaoProjectService.deleteShenbaoProjectByIds(ids));
    }
}
