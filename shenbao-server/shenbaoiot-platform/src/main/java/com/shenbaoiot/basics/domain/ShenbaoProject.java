package com.shenbaoiot.basics.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shenbaoiot.common.annotation.Excel;
import com.shenbaoiot.common.core.domain.BaseEntity;

/**
 * 项目管理对象 shenbao_project
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-10
 */
public class ShenbaoProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 项目编号 */
    @Excel(name = "项目编号")
    private String projectNum;

    /** 项目地址 */
    @Excel(name = "项目地址")
    private String projectAddress;

    /** 联系人 */
    @Excel(name = "联系人")
    private String projectLinkman;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String projectTel;

    /** 项目描述 */
    @Excel(name = "项目描述")
    private String projectDes;

    /** 项目资料 */
    @Excel(name = "项目资料")
    private String projectFile;

    /** 是否启用 */
    @Excel(name = "是否启用")
    private String open;

    /** 删除标记 */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setProjectNum(String projectNum) 
    {
        this.projectNum = projectNum;
    }

    public String getProjectNum() 
    {
        return projectNum;
    }
    public void setProjectAddress(String projectAddress) 
    {
        this.projectAddress = projectAddress;
    }

    public String getProjectAddress() 
    {
        return projectAddress;
    }
    public void setProjectLinkman(String projectLinkman) 
    {
        this.projectLinkman = projectLinkman;
    }

    public String getProjectLinkman() 
    {
        return projectLinkman;
    }
    public void setProjectTel(String projectTel) 
    {
        this.projectTel = projectTel;
    }

    public String getProjectTel() 
    {
        return projectTel;
    }
    public void setProjectDes(String projectDes) 
    {
        this.projectDes = projectDes;
    }

    public String getProjectDes() 
    {
        return projectDes;
    }
    public void setProjectFile(String projectFile) 
    {
        this.projectFile = projectFile;
    }

    public String getProjectFile() 
    {
        return projectFile;
    }
    public void setOpen(String open) 
    {
        this.open = open;
    }

    public String getOpen() 
    {
        return open;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectName", getProjectName())
            .append("projectNum", getProjectNum())
            .append("projectAddress", getProjectAddress())
            .append("projectLinkman", getProjectLinkman())
            .append("projectTel", getProjectTel())
            .append("projectDes", getProjectDes())
            .append("projectFile", getProjectFile())
            .append("open", getOpen())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
