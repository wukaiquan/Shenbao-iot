package com.shenbaoiot.basics.service.impl;

import java.util.List;
import com.shenbaoiot.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shenbaoiot.basics.mapper.ShenbaoProjectMapper;
import com.shenbaoiot.basics.domain.ShenbaoProject;
import com.shenbaoiot.basics.service.IShenbaoProjectService;

/**
 * 项目管理Service业务层处理
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-10
 */
@Service
public class ShenbaoProjectServiceImpl implements IShenbaoProjectService 
{
    @Autowired
    private ShenbaoProjectMapper shenbaoProjectMapper;

    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    @Override
    public ShenbaoProject selectShenbaoProjectById(Long id)
    {
        return shenbaoProjectMapper.selectShenbaoProjectById(id);
    }

    /**
     * 查询项目管理列表
     * 
     * @param shenbaoProject 项目管理
     * @return 项目管理
     */
    @Override
    public List<ShenbaoProject> selectShenbaoProjectList(ShenbaoProject shenbaoProject)
    {
        return shenbaoProjectMapper.selectShenbaoProjectList(shenbaoProject);
    }

    /**
     * 新增项目管理
     * 
     * @param shenbaoProject 项目管理
     * @return 结果
     */
    @Override
    public int insertShenbaoProject(ShenbaoProject shenbaoProject)
    {
        shenbaoProject.setCreateTime(DateUtils.getNowDate());
        return shenbaoProjectMapper.insertShenbaoProject(shenbaoProject);
    }

    /**
     * 修改项目管理
     * 
     * @param shenbaoProject 项目管理
     * @return 结果
     */
    @Override
    public int updateShenbaoProject(ShenbaoProject shenbaoProject)
    {
        shenbaoProject.setUpdateTime(DateUtils.getNowDate());
        return shenbaoProjectMapper.updateShenbaoProject(shenbaoProject);
    }

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的项目管理主键
     * @return 结果
     */
    @Override
    public int deleteShenbaoProjectByIds(Long[] ids)
    {
        return shenbaoProjectMapper.deleteShenbaoProjectByIds(ids);
    }

    /**
     * 删除项目管理信息
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    @Override
    public int deleteShenbaoProjectById(Long id)
    {
        return shenbaoProjectMapper.deleteShenbaoProjectById(id);
    }
}
