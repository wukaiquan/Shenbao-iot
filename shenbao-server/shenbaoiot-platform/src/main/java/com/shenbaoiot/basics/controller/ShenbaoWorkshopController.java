package com.shenbaoiot.basics.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shenbaoiot.common.annotation.Log;
import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.AjaxResult;
import com.shenbaoiot.common.enums.BusinessType;
import com.shenbaoiot.basics.domain.ShenbaoWorkshop;
import com.shenbaoiot.basics.service.IShenbaoWorkshopService;
import com.shenbaoiot.common.utils.poi.ExcelUtil;
import com.shenbaoiot.common.core.page.TableDataInfo;

/**
 * 车间管理Controller
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-10
 */
@RestController
@RequestMapping("/basics/workshop")
public class ShenbaoWorkshopController extends BaseController
{
    @Autowired
    private IShenbaoWorkshopService shenbaoWorkshopService;

    /**
     * 查询车间管理列表
     */
    @PreAuthorize("@ss.hasPermi('basics:workshop:list')")
    @GetMapping("/list")
    public TableDataInfo list(ShenbaoWorkshop shenbaoWorkshop)
    {
        startPage();
        List<ShenbaoWorkshop> list = shenbaoWorkshopService.selectShenbaoWorkshopList(shenbaoWorkshop);
        return getDataTable(list);
    }

    /**
     * 导出车间管理列表
     */
    @PreAuthorize("@ss.hasPermi('basics:workshop:export')")
    @Log(title = "车间管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ShenbaoWorkshop shenbaoWorkshop)
    {
        List<ShenbaoWorkshop> list = shenbaoWorkshopService.selectShenbaoWorkshopList(shenbaoWorkshop);
        ExcelUtil<ShenbaoWorkshop> util = new ExcelUtil<ShenbaoWorkshop>(ShenbaoWorkshop.class);
        util.exportExcel(response, list, "车间管理数据");
    }

    /**
     * 获取车间管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('basics:workshop:query')")
    @GetMapping(value = "/{workshopId}")
    public AjaxResult getInfo(@PathVariable("workshopId") Long workshopId)
    {
        return success(shenbaoWorkshopService.selectShenbaoWorkshopByWorkshopId(workshopId));
    }

    /**
     * 新增车间管理
     */
    @PreAuthorize("@ss.hasPermi('basics:workshop:add')")
    @Log(title = "车间管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ShenbaoWorkshop shenbaoWorkshop)
    {
        return toAjax(shenbaoWorkshopService.insertShenbaoWorkshop(shenbaoWorkshop));
    }

    /**
     * 修改车间管理
     */
    @PreAuthorize("@ss.hasPermi('basics:workshop:edit')")
    @Log(title = "车间管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ShenbaoWorkshop shenbaoWorkshop)
    {
        return toAjax(shenbaoWorkshopService.updateShenbaoWorkshop(shenbaoWorkshop));
    }

    /**
     * 删除车间管理
     */
    @PreAuthorize("@ss.hasPermi('basics:workshop:remove')")
    @Log(title = "车间管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{workshopIds}")
    public AjaxResult remove(@PathVariable Long[] workshopIds)
    {
        return toAjax(shenbaoWorkshopService.deleteShenbaoWorkshopByWorkshopIds(workshopIds));
    }
}
