package com.shenbaoiot.basics.mapper;

import java.util.List;
import com.shenbaoiot.basics.domain.ShenbaoWorkshop;

/**
 * 车间管理Mapper接口
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-10
 */
public interface ShenbaoWorkshopMapper 
{
    /**
     * 查询车间管理
     * 
     * @param workshopId 车间管理主键
     * @return 车间管理
     */
    public ShenbaoWorkshop selectShenbaoWorkshopByWorkshopId(Long workshopId);

    /**
     * 查询车间管理列表
     * 
     * @param shenbaoWorkshop 车间管理
     * @return 车间管理集合
     */
    public List<ShenbaoWorkshop> selectShenbaoWorkshopList(ShenbaoWorkshop shenbaoWorkshop);

    /**
     * 新增车间管理
     * 
     * @param shenbaoWorkshop 车间管理
     * @return 结果
     */
    public int insertShenbaoWorkshop(ShenbaoWorkshop shenbaoWorkshop);

    /**
     * 修改车间管理
     * 
     * @param shenbaoWorkshop 车间管理
     * @return 结果
     */
    public int updateShenbaoWorkshop(ShenbaoWorkshop shenbaoWorkshop);

    /**
     * 删除车间管理
     * 
     * @param workshopId 车间管理主键
     * @return 结果
     */
    public int deleteShenbaoWorkshopByWorkshopId(Long workshopId);

    /**
     * 批量删除车间管理
     * 
     * @param workshopIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteShenbaoWorkshopByWorkshopIds(Long[] workshopIds);
}
