package com.shenbaoiot.basics.service.impl;

import java.util.List;
import com.shenbaoiot.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shenbaoiot.basics.mapper.ShenbaoWorkshopMapper;
import com.shenbaoiot.basics.domain.ShenbaoWorkshop;
import com.shenbaoiot.basics.service.IShenbaoWorkshopService;

/**
 * 车间管理Service业务层处理
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-10
 */
@Service
public class ShenbaoWorkshopServiceImpl implements IShenbaoWorkshopService 
{
    @Autowired
    private ShenbaoWorkshopMapper shenbaoWorkshopMapper;

    /**
     * 查询车间管理
     * 
     * @param workshopId 车间管理主键
     * @return 车间管理
     */
    @Override
    public ShenbaoWorkshop selectShenbaoWorkshopByWorkshopId(Long workshopId)
    {
        return shenbaoWorkshopMapper.selectShenbaoWorkshopByWorkshopId(workshopId);
    }

    /**
     * 查询车间管理列表
     * 
     * @param shenbaoWorkshop 车间管理
     * @return 车间管理
     */
    @Override
    public List<ShenbaoWorkshop> selectShenbaoWorkshopList(ShenbaoWorkshop shenbaoWorkshop)
    {
        return shenbaoWorkshopMapper.selectShenbaoWorkshopList(shenbaoWorkshop);
    }

    /**
     * 新增车间管理
     * 
     * @param shenbaoWorkshop 车间管理
     * @return 结果
     */
    @Override
    public int insertShenbaoWorkshop(ShenbaoWorkshop shenbaoWorkshop)
    {
        shenbaoWorkshop.setCreateTime(DateUtils.getNowDate());
        return shenbaoWorkshopMapper.insertShenbaoWorkshop(shenbaoWorkshop);
    }

    /**
     * 修改车间管理
     * 
     * @param shenbaoWorkshop 车间管理
     * @return 结果
     */
    @Override
    public int updateShenbaoWorkshop(ShenbaoWorkshop shenbaoWorkshop)
    {
        shenbaoWorkshop.setUpdateTime(DateUtils.getNowDate());
        return shenbaoWorkshopMapper.updateShenbaoWorkshop(shenbaoWorkshop);
    }

    /**
     * 批量删除车间管理
     * 
     * @param workshopIds 需要删除的车间管理主键
     * @return 结果
     */
    @Override
    public int deleteShenbaoWorkshopByWorkshopIds(Long[] workshopIds)
    {
        return shenbaoWorkshopMapper.deleteShenbaoWorkshopByWorkshopIds(workshopIds);
    }

    /**
     * 删除车间管理信息
     * 
     * @param workshopId 车间管理主键
     * @return 结果
     */
    @Override
    public int deleteShenbaoWorkshopByWorkshopId(Long workshopId)
    {
        return shenbaoWorkshopMapper.deleteShenbaoWorkshopByWorkshopId(workshopId);
    }
}
