package com.shenbaoiot.dev.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shenbaoiot.common.annotation.Log;
import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.AjaxResult;
import com.shenbaoiot.common.enums.BusinessType;
import com.shenbaoiot.dev.domain.DevClassified;
import com.shenbaoiot.dev.service.IDevClassifiedService;
import com.shenbaoiot.common.utils.poi.ExcelUtil;

/**
 * 设备类型Controller
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-11
 */
@RestController
@RequestMapping("/dev/classified")
public class DevClassifiedController extends BaseController
{
    @Autowired
    private IDevClassifiedService devClassifiedService;

    /**
     * 查询设备类型列表
     */
    @PreAuthorize("@ss.hasPermi('dev:classified:list')")
    @GetMapping("/list")
    public AjaxResult list(DevClassified devClassified)
    {
        List<DevClassified> list = devClassifiedService.selectDevClassifiedList(devClassified);
        return success(list);
    }

    /**
     * 导出设备类型列表
     */
    @PreAuthorize("@ss.hasPermi('dev:classified:export')")
    @Log(title = "设备类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DevClassified devClassified)
    {
        List<DevClassified> list = devClassifiedService.selectDevClassifiedList(devClassified);
        ExcelUtil<DevClassified> util = new ExcelUtil<DevClassified>(DevClassified.class);
        util.exportExcel(response, list, "设备类型数据");
    }

    /**
     * 获取设备类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('dev:classified:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(devClassifiedService.selectDevClassifiedById(id));
    }

    /**
     * 新增设备类型
     */
    @PreAuthorize("@ss.hasPermi('dev:classified:add')")
    @Log(title = "设备类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DevClassified devClassified)
    {
        return toAjax(devClassifiedService.insertDevClassified(devClassified));
    }

    /**
     * 修改设备类型
     */
    @PreAuthorize("@ss.hasPermi('dev:classified:edit')")
    @Log(title = "设备类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DevClassified devClassified)
    {
        return toAjax(devClassifiedService.updateDevClassified(devClassified));
    }

    /**
     * 删除设备类型
     */
    @PreAuthorize("@ss.hasPermi('dev:classified:remove')")
    @Log(title = "设备类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(devClassifiedService.deleteDevClassifiedByIds(ids));
    }
}
