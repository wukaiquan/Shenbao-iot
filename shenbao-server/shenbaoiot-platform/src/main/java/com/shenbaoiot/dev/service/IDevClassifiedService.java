package com.shenbaoiot.dev.service;

import java.util.List;
import com.shenbaoiot.dev.domain.DevClassified;

/**
 * 设备类型Service接口
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-11
 */
public interface IDevClassifiedService 
{
    /**
     * 查询设备类型
     * 
     * @param id 设备类型主键
     * @return 设备类型
     */
    public DevClassified selectDevClassifiedById(Long id);

    /**
     * 查询设备类型列表
     * 
     * @param devClassified 设备类型
     * @return 设备类型集合
     */
    public List<DevClassified> selectDevClassifiedList(DevClassified devClassified);

    /**
     * 新增设备类型
     * 
     * @param devClassified 设备类型
     * @return 结果
     */
    public int insertDevClassified(DevClassified devClassified);

    /**
     * 修改设备类型
     * 
     * @param devClassified 设备类型
     * @return 结果
     */
    public int updateDevClassified(DevClassified devClassified);

    /**
     * 批量删除设备类型
     * 
     * @param ids 需要删除的设备类型主键集合
     * @return 结果
     */
    public int deleteDevClassifiedByIds(Long[] ids);

    /**
     * 删除设备类型信息
     * 
     * @param id 设备类型主键
     * @return 结果
     */
    public int deleteDevClassifiedById(Long id);
}
