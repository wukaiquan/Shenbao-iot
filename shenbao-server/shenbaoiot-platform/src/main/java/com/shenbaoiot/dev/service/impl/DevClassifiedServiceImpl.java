package com.shenbaoiot.dev.service.impl;

import java.util.List;
import com.shenbaoiot.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shenbaoiot.dev.mapper.DevClassifiedMapper;
import com.shenbaoiot.dev.domain.DevClassified;
import com.shenbaoiot.dev.service.IDevClassifiedService;

/**
 * 设备类型Service业务层处理
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-11
 */
@Service
public class DevClassifiedServiceImpl implements IDevClassifiedService 
{
    @Autowired
    private DevClassifiedMapper devClassifiedMapper;

    /**
     * 查询设备类型
     * 
     * @param id 设备类型主键
     * @return 设备类型
     */
    @Override
    public DevClassified selectDevClassifiedById(Long id)
    {
        return devClassifiedMapper.selectDevClassifiedById(id);
    }

    /**
     * 查询设备类型列表
     * 
     * @param devClassified 设备类型
     * @return 设备类型
     */
    @Override
    public List<DevClassified> selectDevClassifiedList(DevClassified devClassified)
    {
        return devClassifiedMapper.selectDevClassifiedList(devClassified);
    }

    /**
     * 新增设备类型
     * 
     * @param devClassified 设备类型
     * @return 结果
     */
    @Override
    public int insertDevClassified(DevClassified devClassified)
    {
        devClassified.setCreateTime(DateUtils.getNowDate());
        return devClassifiedMapper.insertDevClassified(devClassified);
    }

    /**
     * 修改设备类型
     * 
     * @param devClassified 设备类型
     * @return 结果
     */
    @Override
    public int updateDevClassified(DevClassified devClassified)
    {
        devClassified.setUpdateTime(DateUtils.getNowDate());
        return devClassifiedMapper.updateDevClassified(devClassified);
    }

    /**
     * 批量删除设备类型
     * 
     * @param ids 需要删除的设备类型主键
     * @return 结果
     */
    @Override
    public int deleteDevClassifiedByIds(Long[] ids)
    {
        return devClassifiedMapper.deleteDevClassifiedByIds(ids);
    }

    /**
     * 删除设备类型信息
     * 
     * @param id 设备类型主键
     * @return 结果
     */
    @Override
    public int deleteDevClassifiedById(Long id)
    {
        return devClassifiedMapper.deleteDevClassifiedById(id);
    }
}
