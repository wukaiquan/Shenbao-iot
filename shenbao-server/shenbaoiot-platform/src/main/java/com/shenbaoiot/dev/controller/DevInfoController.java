package com.shenbaoiot.dev.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shenbaoiot.common.annotation.Log;
import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.AjaxResult;
import com.shenbaoiot.common.enums.BusinessType;
import com.shenbaoiot.dev.domain.DevInfo;
import com.shenbaoiot.dev.service.IDevInfoService;
import com.shenbaoiot.common.utils.poi.ExcelUtil;
import com.shenbaoiot.common.core.page.TableDataInfo;

/**
 * 设备管理Controller
 * 
 * @author 常宝坤
 * @date 2023-05-12
 */
@RestController
@RequestMapping("/dev/devinfo")
public class DevInfoController extends BaseController
{
    @Autowired
    private IDevInfoService devInfoService;

    /**
     * 查询设备管理列表
     */
    @PreAuthorize("@ss.hasPermi('dev:devinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(DevInfo devInfo)
    {
        startPage();
        List<DevInfo> list = devInfoService.selectDevInfoList(devInfo);
        return getDataTable(list);
    }

    /**
     * 导出设备管理列表
     */
    @PreAuthorize("@ss.hasPermi('dev:devinfo:export')")
    @Log(title = "设备管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DevInfo devInfo)
    {
        List<DevInfo> list = devInfoService.selectDevInfoList(devInfo);
        ExcelUtil<DevInfo> util = new ExcelUtil<DevInfo>(DevInfo.class);
        util.exportExcel(response, list, "设备管理数据");
    }

    /**
     * 获取设备管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('dev:devinfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(devInfoService.selectDevInfoById(id));
    }

    /**
     * 新增设备管理
     */
    @PreAuthorize("@ss.hasPermi('dev:devinfo:add')")
    @Log(title = "设备管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DevInfo devInfo)
    {
        return toAjax(devInfoService.insertDevInfo(devInfo));
    }

    /**
     * 修改设备管理
     */
    @PreAuthorize("@ss.hasPermi('dev:devinfo:edit')")
    @Log(title = "设备管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DevInfo devInfo)
    {
        return toAjax(devInfoService.updateDevInfo(devInfo));
    }

    /**
     * 删除设备管理
     */
    @PreAuthorize("@ss.hasPermi('dev:devinfo:remove')")
    @Log(title = "设备管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(devInfoService.deleteDevInfoByIds(ids));
    }
}
