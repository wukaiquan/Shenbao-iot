package com.shenbaoiot.dev.service;

import com.shenbaoiot.dev.domain.TSL.TSLReceive;

/**
 * 物模型信息Service接口
 *
 * @author 常宝坤
 * @date 2023-05-10
 */
public interface ITSLService
{

    /**
     * 查询物模型信息
     * @param  tslReceive 物模型信息
     * @return
     */
    public int addEitTsl(TSLReceive tslReceive);
    /**
     * 新增物模型信息
     * @param tslReceive 物模型信息
     * @return 结果
     */
    public int addTSL(TSLReceive tslReceive);
    /**
     * 新增物模型信息
     * @param tslReceive 物模型信息
     * @return 结果
     */
    public int editTSL(TSLReceive tslReceive);

    /**
     * 批量删除物模型信息
     * @param tslReceive 需要删除的物模型信息主键集合
     * @return 结果
     */
    public int deleteTSL(TSLReceive tslReceive);
}
