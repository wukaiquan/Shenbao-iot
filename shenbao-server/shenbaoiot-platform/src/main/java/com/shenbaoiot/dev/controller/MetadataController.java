package com.shenbaoiot.dev.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shenbaoiot.common.annotation.Log;
import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.AjaxResult;
import com.shenbaoiot.common.enums.BusinessType;
import com.shenbaoiot.dev.domain.Metadata;
import com.shenbaoiot.dev.service.IMetadataService;
import com.shenbaoiot.common.utils.poi.ExcelUtil;
import com.shenbaoiot.common.core.page.TableDataInfo;

/**
 * 物模型信息Controller
 * 
 * @author 常宝坤
 * @date 2023-05-10
 */
@RestController
@RequestMapping("/dev/metadata")
public class MetadataController extends BaseController
{
    @Autowired
    private IMetadataService metadataService;

    /**
     * 查询物模型信息列表
     */
    @PreAuthorize("@ss.hasPermi('dev:metadata:list')")
    @GetMapping("/list")
    public TableDataInfo list(Metadata metadata)
    {
        startPage();
        List<Metadata> list = metadataService.selectMetadataList(metadata);
        return getDataTable(list);
    }

    /**
     * 导出物模型信息列表
     */
    @PreAuthorize("@ss.hasPermi('dev:metadata:export')")
    @Log(title = "物模型信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Metadata metadata)
    {
        List<Metadata> list = metadataService.selectMetadataList(metadata);
        ExcelUtil<Metadata> util = new ExcelUtil<Metadata>(Metadata.class);
        util.exportExcel(response, list, "物模型信息数据");
    }

    /**
     * 获取物模型信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('dev:metadata:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(metadataService.selectMetadataById(id));
    }

    /**
     * 新增物模型信息
     */
    @PreAuthorize("@ss.hasPermi('dev:metadata:add')")
    @Log(title = "物模型信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Metadata metadata)
    {
        return toAjax(metadataService.insertMetadata(metadata));
    }

    /**
     * 修改物模型信息
     */
    @PreAuthorize("@ss.hasPermi('dev:metadata:edit')")
    @Log(title = "物模型信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Metadata metadata)
    {
        return toAjax(metadataService.updateMetadata(metadata));
    }

    /**
     * 删除物模型信息
     */
    @PreAuthorize("@ss.hasPermi('dev:metadata:remove')")
    @Log(title = "物模型信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(metadataService.deleteMetadataByIds(ids));
    }
}
