package com.shenbaoiot.dev.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shenbaoiot.common.annotation.Excel;
import com.shenbaoiot.common.core.domain.BaseEntity;

/**
 * 物模型信息对象 shenbao_metadata
 * 
 * @author 常宝坤
 * @date 2023-05-10
 */
public class Metadata extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 物模型 */
    @Excel(name = "物模型")
    private String metadata;

    /** 属于产品或设备的id */
    @Excel(name = "属于产品或设备的id")
    private Long whoBelongsId;

    /** 属于谁（0-产品，1-设备） */
    @Excel(name = "属于谁", readConverterExp = "0=-产品，1-设备")
    private String whoBelongs;

    /** 是否启用（0.否，1. 是） */
    @Excel(name = "是否启用", readConverterExp = "0=.否，1.,是=")
    private String open;

    /** 删除标记 */
    private String delFlag;

    /** 预留字段1 */
    @Excel(name = "预留字段1")
    private String attr1;

    /** 预留字段2 */
    @Excel(name = "预留字段2")
    private String attr2;

    /** 预留字段3 */
    @Excel(name = "预留字段3")
    private Long attr3;

    /** 预留字段4 */
    @Excel(name = "预留字段4")
    private Long attr4;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMetadata(String metadata) 
    {
        this.metadata = metadata;
    }

    public String getMetadata() 
    {
        return metadata;
    }
    public void setWhoBelongsId(Long whoBelongsId) 
    {
        this.whoBelongsId = whoBelongsId;
    }

    public Long getWhoBelongsId() 
    {
        return whoBelongsId;
    }
    public void setWhoBelongs(String whoBelongs) 
    {
        this.whoBelongs = whoBelongs;
    }

    public String getWhoBelongs() 
    {
        return whoBelongs;
    }
    public void setOpen(String open) 
    {
        this.open = open;
    }

    public String getOpen() 
    {
        return open;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setAttr1(String attr1) 
    {
        this.attr1 = attr1;
    }

    public String getAttr1() 
    {
        return attr1;
    }
    public void setAttr2(String attr2) 
    {
        this.attr2 = attr2;
    }

    public String getAttr2() 
    {
        return attr2;
    }
    public void setAttr3(Long attr3) 
    {
        this.attr3 = attr3;
    }

    public Long getAttr3() 
    {
        return attr3;
    }
    public void setAttr4(Long attr4) 
    {
        this.attr4 = attr4;
    }

    public Long getAttr4() 
    {
        return attr4;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("metadata", getMetadata())
            .append("whoBelongsId", getWhoBelongsId())
            .append("whoBelongs", getWhoBelongs())
            .append("open", getOpen())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("attr1", getAttr1())
            .append("attr2", getAttr2())
            .append("attr3", getAttr3())
            .append("attr4", getAttr4())
            .toString();
    }
}
