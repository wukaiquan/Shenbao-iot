package com.shenbaoiot.dev.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;


import com.shenbaoiot.dev.domain.TSL.TSLReceive;
import com.shenbaoiot.dev.service.ITSLService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shenbaoiot.common.annotation.Log;
import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.AjaxResult;
import com.shenbaoiot.common.enums.BusinessType;
import com.shenbaoiot.dev.domain.Metadata;
import com.shenbaoiot.dev.service.IMetadataService;
import com.shenbaoiot.common.utils.poi.ExcelUtil;
import com.shenbaoiot.common.core.page.TableDataInfo;

/**
 * 物模型详细信息Controller
 *
 * @author 常宝坤
 * @date 2023-05-10
 */
@RestController
@RequestMapping("/dev/tsl")
public class TSLController extends BaseController
{
    @Autowired
    private ITSLService tslService;
    /**
     * 查询物模型信息
     */
    //@PreAuthorize("@ss.hasPermi('dev:metadata:add')")
    @Log(title = "物模型信息", businessType = BusinessType.INSERT)
    @PostMapping("/addEitTsl")
    public AjaxResult addEitTsl(@RequestBody TSLReceive tsl)
    {
        return success(tslService.addEitTsl(tsl));
    }

    /**
     * 新增物模型信息
     */
    //@PreAuthorize("@ss.hasPermi('dev:metadata:add')")
    @Log(title = "物模型信息", businessType = BusinessType.INSERT)
    @PostMapping("/addTsl")
    public AjaxResult add(@RequestBody TSLReceive tsl)
    {
        return toAjax(tslService.addTSL(tsl));
    }

    /**
     * 修改物模型信息
     */
   // @PreAuthorize("@ss.hasPermi('dev:metadata:edit')")
    @Log(title = "物模型信息", businessType = BusinessType.UPDATE)
    @PostMapping("/editTsl")
    public AjaxResult editTsl(@RequestBody TSLReceive tsl)
    {
        return toAjax(tslService.editTSL(tsl));
    }

    /**
     * 删除物模型信息
     */
    //@PreAuthorize("@ss.hasPermi('dev:metadata:remove')")
    @Log(title = "物模型信息", businessType = BusinessType.DELETE)
    @PostMapping("/removeTsl")
    public AjaxResult removeTsl(@RequestBody TSLReceive tsl)
    {
        return toAjax(tslService.deleteTSL(tsl));
    }
}
