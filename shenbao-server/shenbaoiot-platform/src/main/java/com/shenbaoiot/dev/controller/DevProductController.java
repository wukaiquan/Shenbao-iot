package com.shenbaoiot.dev.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shenbaoiot.common.annotation.Log;
import com.shenbaoiot.common.core.controller.BaseController;
import com.shenbaoiot.common.core.domain.AjaxResult;
import com.shenbaoiot.common.enums.BusinessType;
import com.shenbaoiot.dev.domain.DevProduct;
import com.shenbaoiot.dev.service.IDevProductService;
import com.shenbaoiot.common.utils.poi.ExcelUtil;
import com.shenbaoiot.common.core.page.TableDataInfo;

/**
 * 产品管理Controller
 * 
 * @author 常宝坤 - 13964179025
 * @date 2023-03-11
 */
@RestController
@RequestMapping("/dev/product")
public class DevProductController extends BaseController
{
    @Autowired
    private IDevProductService devProductService;

    /**
     * 查询产品管理列表
     */
    @PreAuthorize("@ss.hasPermi('dev:product:list')")
    @GetMapping("/list")
    public TableDataInfo list(DevProduct devProduct)
    {
        startPage();
        List<DevProduct> list = devProductService.selectDevProductList(devProduct);
        return getDataTable(list);
    }

    /**
     * 导出产品管理列表
     */
    @PreAuthorize("@ss.hasPermi('dev:product:export')")
    @Log(title = "产品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DevProduct devProduct)
    {
        List<DevProduct> list = devProductService.selectDevProductList(devProduct);
        ExcelUtil<DevProduct> util = new ExcelUtil<DevProduct>(DevProduct.class);
        util.exportExcel(response, list, "产品管理数据");
    }

    /**
     * 获取产品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('dev:product:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(devProductService.selectDevProductById(id));
    }

    /**
     * 新增产品管理
     */
    @PreAuthorize("@ss.hasPermi('dev:product:add')")
    @Log(title = "产品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DevProduct devProduct)
    {
        return toAjax(devProductService.insertDevProduct(devProduct));
    }

    /**
     * 修改产品管理
     */
    @PreAuthorize("@ss.hasPermi('dev:product:edit')")
    @Log(title = "产品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DevProduct devProduct)
    {
        return toAjax(devProductService.updateDevProduct(devProduct));
    }

    /**
     * 删除产品管理
     */
    @PreAuthorize("@ss.hasPermi('dev:product:remove')")
    @Log(title = "产品管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(devProductService.deleteDevProductByIds(ids));
    }
}
