package com.shenbaoiot.dev.domain.TSL;

import com.shenbaoiot.common.annotation.Excel;
import com.shenbaoiot.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Map;

/**
 * 物模型信息对象 shenbao_metadata
 *
 * @author 常宝坤
 * @date 2023-05-10
 */
@Data
public class TSLReceive extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /** 属于产品或设备的id */
    private Long whoBelongsId;

    /** 属于谁（0-产品，1-设备） */
    private String whoBelongs;
    /** 操作类型operateType  add-新增 update-更新 edit-编辑 delete-删除*/
    private String operateType;

    /**
     * 产品唯一标识
     */
    private String productKey;

    /**
     * tsl类型
     * "properties"--属性
     * "events"--事件
     * "services"--服务
     * "outputData"--参数
     */
    private String type;
    /**
     * 入参
     */
    private String param;


}
