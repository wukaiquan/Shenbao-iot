package com.shenbaoiot.dev.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shenbaoiot.common.annotation.Excel;
import com.shenbaoiot.common.core.domain.TreeEntity;

/**
 * 设备类型对象 shenbao_dev_classified
 * 
 * @author 常宝坤-13964179025
 * @date 2023-03-11
 */
public class DevClassified extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备类型id */
    private Long id;

    /** 设备类型编码 */
    @Excel(name = "设备类型编码")
    private String classifiedCode;

    /** 设备类型名称 */
    @Excel(name = "设备类型名称")
    private String classifiedName;

    /** 设备类型分类 */
    @Excel(name = "设备类型分类")
    private String deviceType;

    /** 是否启用 */
    @Excel(name = "是否启用")
    private String enableFlag;

    /** 预留字段1 */
    @Excel(name = "预留字段1")
    private String attr1;

    /** 预留字段2 */
    @Excel(name = "预留字段2")
    private String attr2;

    /** 预留字段3 */
    @Excel(name = "预留字段3")
    private Long attr3;

    /** 预留字段4 */
    @Excel(name = "预留字段4")
    private Long attr4;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setClassifiedCode(String classifiedCode) 
    {
        this.classifiedCode = classifiedCode;
    }

    public String getClassifiedCode() 
    {
        return classifiedCode;
    }
    public void setClassifiedName(String classifiedName) 
    {
        this.classifiedName = classifiedName;
    }

    public String getClassifiedName() 
    {
        return classifiedName;
    }
    public void setDeviceType(String deviceType) 
    {
        this.deviceType = deviceType;
    }

    public String getDeviceType() 
    {
        return deviceType;
    }
    public void setEnableFlag(String enableFlag) 
    {
        this.enableFlag = enableFlag;
    }

    public String getEnableFlag() 
    {
        return enableFlag;
    }
    public void setAttr1(String attr1) 
    {
        this.attr1 = attr1;
    }

    public String getAttr1() 
    {
        return attr1;
    }
    public void setAttr2(String attr2) 
    {
        this.attr2 = attr2;
    }

    public String getAttr2() 
    {
        return attr2;
    }
    public void setAttr3(Long attr3) 
    {
        this.attr3 = attr3;
    }

    public Long getAttr3() 
    {
        return attr3;
    }
    public void setAttr4(Long attr4) 
    {
        this.attr4 = attr4;
    }

    public Long getAttr4() 
    {
        return attr4;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("classifiedCode", getClassifiedCode())
            .append("classifiedName", getClassifiedName())
            .append("deviceType", getDeviceType())
            .append("parentId", getParentId())
            .append("enableFlag", getEnableFlag())
            .append("ancestors", getAncestors())
            .append("orderNum", getOrderNum())
            .append("remark", getRemark())
            .append("attr1", getAttr1())
            .append("attr2", getAttr2())
            .append("attr3", getAttr3())
            .append("attr4", getAttr4())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
