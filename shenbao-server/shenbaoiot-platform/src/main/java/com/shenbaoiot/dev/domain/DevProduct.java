package com.shenbaoiot.dev.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shenbaoiot.common.annotation.Excel;
import com.shenbaoiot.common.core.domain.BaseEntity;

/**
 * 产品管理对象 shenbao_dev_product
 * 
 * @author 常宝坤 - 13964179025
 * @date 2023-03-11
 */
public class DevProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品唯一标识 */
    @Excel(name = "产品唯一标识")
    private String productMarking;

    /** 所属品类ID */
    @Excel(name = "所属品类ID")
    private Long productClassifiedId;

    /** 所属品类名称 */
    @Excel(name = "所属品类名称")
    private String productClassifiedName;

    /** 设备类型 */
    @Excel(name = "设备类型")
    private String deviceType;

    /** 所属项目 */
    @Excel(name = "所属项目")
    private String projectId;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 所属部门 */
    @Excel(name = "所属部门")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 产品状态 */
    @Excel(name = "产品状态")
    private Integer productState;

    /** 物模型定义 */
    @Excel(name = "物模型定义")
    private String metadata;

    /** 设备图标 */
    @Excel(name = "设备图标")
    private String photoUrl;

    /** 连网方式 */
    @Excel(name = "连网方式")
    private String networkWay;

    /** 消息协议ID */
    @Excel(name = "消息协议ID")
    private String messageProtocol;

    /** 消息协议名称 */
    @Excel(name = "消息协议名称")
    private String protocolName;

    /** 传输协议 */
    @Excel(name = "传输协议")
    private String transportProtocol;

    /** 数据存储策略相关配置 */
    @Excel(name = "数据存储策略相关配置")
    private String storePolicyConfiguration;

    /** 设备接入方式名称 */
    @Excel(name = "设备接入方式名称")
    private String accessName;

    /** 设备接入方式 */
    @Excel(name = "设备接入方式")
    private String accessProvider;

    /** 数据存储策略 */
    @Excel(name = "数据存储策略")
    private String storePolicy;

    /** 删除标记 */
    private String delFlag;

    /** 预留字段1 */
    @Excel(name = "预留字段1")
    private String attr1;

    /** 预留字段2 */
    @Excel(name = "预留字段2")
    private String attr2;

    /** 预留字段3 */
    @Excel(name = "预留字段3")
    private Long attr3;

    /** 预留字段4 */
    @Excel(name = "预留字段4")
    private Long attr4;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setProductMarking(String productMarking) 
    {
        this.productMarking = productMarking;
    }

    public String getProductMarking() 
    {
        return productMarking;
    }
    public void setProductClassifiedId(Long productClassifiedId) 
    {
        this.productClassifiedId = productClassifiedId;
    }

    public Long getProductClassifiedId() 
    {
        return productClassifiedId;
    }
    public void setProductClassifiedName(String productClassifiedName) 
    {
        this.productClassifiedName = productClassifiedName;
    }

    public String getProductClassifiedName() 
    {
        return productClassifiedName;
    }
    public void setDeviceType(String deviceType) 
    {
        this.deviceType = deviceType;
    }

    public String getDeviceType() 
    {
        return deviceType;
    }
    public void setProjectId(String projectId) 
    {
        this.projectId = projectId;
    }

    public String getProjectId() 
    {
        return projectId;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setDeptName(String deptName) 
    {
        this.deptName = deptName;
    }

    public String getDeptName() 
    {
        return deptName;
    }
    public void setProductState(Integer productState) 
    {
        this.productState = productState;
    }

    public Integer getProductState() 
    {
        return productState;
    }
    public void setMetadata(String metadata) 
    {
        this.metadata = metadata;
    }

    public String getMetadata() 
    {
        return metadata;
    }
    public void setPhotoUrl(String photoUrl) 
    {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrl() 
    {
        return photoUrl;
    }
    public void setNetworkWay(String networkWay) 
    {
        this.networkWay = networkWay;
    }

    public String getNetworkWay() 
    {
        return networkWay;
    }
    public void setMessageProtocol(String messageProtocol) 
    {
        this.messageProtocol = messageProtocol;
    }

    public String getMessageProtocol() 
    {
        return messageProtocol;
    }
    public void setProtocolName(String protocolName) 
    {
        this.protocolName = protocolName;
    }

    public String getProtocolName() 
    {
        return protocolName;
    }
    public void setTransportProtocol(String transportProtocol) 
    {
        this.transportProtocol = transportProtocol;
    }

    public String getTransportProtocol() 
    {
        return transportProtocol;
    }
    public void setStorePolicyConfiguration(String storePolicyConfiguration) 
    {
        this.storePolicyConfiguration = storePolicyConfiguration;
    }

    public String getStorePolicyConfiguration() 
    {
        return storePolicyConfiguration;
    }
    public void setAccessName(String accessName) 
    {
        this.accessName = accessName;
    }

    public String getAccessName() 
    {
        return accessName;
    }
    public void setAccessProvider(String accessProvider) 
    {
        this.accessProvider = accessProvider;
    }

    public String getAccessProvider() 
    {
        return accessProvider;
    }
    public void setStorePolicy(String storePolicy) 
    {
        this.storePolicy = storePolicy;
    }

    public String getStorePolicy() 
    {
        return storePolicy;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setAttr1(String attr1) 
    {
        this.attr1 = attr1;
    }

    public String getAttr1() 
    {
        return attr1;
    }
    public void setAttr2(String attr2) 
    {
        this.attr2 = attr2;
    }

    public String getAttr2() 
    {
        return attr2;
    }
    public void setAttr3(Long attr3) 
    {
        this.attr3 = attr3;
    }

    public Long getAttr3() 
    {
        return attr3;
    }
    public void setAttr4(Long attr4) 
    {
        this.attr4 = attr4;
    }

    public Long getAttr4() 
    {
        return attr4;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productName", getProductName())
            .append("productMarking", getProductMarking())
            .append("productClassifiedId", getProductClassifiedId())
            .append("productClassifiedName", getProductClassifiedName())
            .append("deviceType", getDeviceType())
            .append("projectId", getProjectId())
            .append("projectName", getProjectName())
            .append("deptId", getDeptId())
            .append("deptName", getDeptName())
            .append("productState", getProductState())
            .append("metadata", getMetadata())
            .append("photoUrl", getPhotoUrl())
            .append("networkWay", getNetworkWay())
            .append("messageProtocol", getMessageProtocol())
            .append("protocolName", getProtocolName())
            .append("transportProtocol", getTransportProtocol())
            .append("storePolicyConfiguration", getStorePolicyConfiguration())
            .append("accessName", getAccessName())
            .append("accessProvider", getAccessProvider())
            .append("storePolicy", getStorePolicy())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("attr1", getAttr1())
            .append("attr2", getAttr2())
            .append("attr3", getAttr3())
            .append("attr4", getAttr4())
            .toString();
    }
}
