package com.shenbaoiot.dev.mapper;

import java.util.List;
import com.shenbaoiot.dev.domain.Metadata;

/**
 * 物模型信息Mapper接口
 * 
 * @author 常宝坤
 * @date 2023-05-10
 */
public interface MetadataMapper 
{
    /**
     * 查询物模型信息
     * 
     * @param id 物模型信息主键
     * @return 物模型信息
     */
    public Metadata selectMetadataById(Long id);

    /**
     * 查询物模型信息列表
     * 
     * @param metadata 物模型信息
     * @return 物模型信息集合
     */
    public List<Metadata> selectMetadataList(Metadata metadata);

    /**
     * 新增物模型信息
     * 
     * @param metadata 物模型信息
     * @return 结果
     */
    public int insertMetadata(Metadata metadata);

    /**
     * 修改物模型信息
     * 
     * @param metadata 物模型信息
     * @return 结果
     */
    public int updateMetadata(Metadata metadata);

    /**
     * 删除物模型信息
     * 
     * @param id 物模型信息主键
     * @return 结果
     */
    public int deleteMetadataById(Long id);

    /**
     * 批量删除物模型信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMetadataByIds(Long[] ids);
}
