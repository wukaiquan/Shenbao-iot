package com.shenbaoiot.dev.mapper;

import java.util.List;
import com.shenbaoiot.dev.domain.DevProduct;

/**
 * 产品管理Mapper接口
 * 
 * @author 常宝坤 - 13964179025
 * @date 2023-03-11
 */
public interface DevProductMapper 
{
    /**
     * 查询产品管理
     * 
     * @param id 产品管理主键
     * @return 产品管理
     */
    public DevProduct selectDevProductById(Long id);

    /**
     * 查询产品管理列表
     * 
     * @param devProduct 产品管理
     * @return 产品管理集合
     */
    public List<DevProduct> selectDevProductList(DevProduct devProduct);

    /**
     * 新增产品管理
     * 
     * @param devProduct 产品管理
     * @return 结果
     */
    public int insertDevProduct(DevProduct devProduct);

    /**
     * 修改产品管理
     * 
     * @param devProduct 产品管理
     * @return 结果
     */
    public int updateDevProduct(DevProduct devProduct);

    /**
     * 删除产品管理
     * 
     * @param id 产品管理主键
     * @return 结果
     */
    public int deleteDevProductById(Long id);

    /**
     * 批量删除产品管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDevProductByIds(Long[] ids);
}
