package com.shenbaoiot.dev.mapper;

import java.util.List;
import com.shenbaoiot.dev.domain.DevInfo;

/**
 * 设备管理Mapper接口
 * 
 * @author 常宝坤
 * @date 2023-05-12
 */
public interface DevInfoMapper 
{
    /**
     * 查询设备管理
     * 
     * @param id 设备管理主键
     * @return 设备管理
     */
    public DevInfo selectDevInfoById(Long id);

    /**
     * 查询设备管理列表
     * 
     * @param devInfo 设备管理
     * @return 设备管理集合
     */
    public List<DevInfo> selectDevInfoList(DevInfo devInfo);

    /**
     * 新增设备管理
     * 
     * @param devInfo 设备管理
     * @return 结果
     */
    public int insertDevInfo(DevInfo devInfo);

    /**
     * 修改设备管理
     * 
     * @param devInfo 设备管理
     * @return 结果
     */
    public int updateDevInfo(DevInfo devInfo);

    /**
     * 删除设备管理
     * 
     * @param id 设备管理主键
     * @return 结果
     */
    public int deleteDevInfoById(Long id);

    /**
     * 批量删除设备管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDevInfoByIds(Long[] ids);
}
