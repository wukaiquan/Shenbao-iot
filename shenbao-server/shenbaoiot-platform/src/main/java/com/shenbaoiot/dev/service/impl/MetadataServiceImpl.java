package com.shenbaoiot.dev.service.impl;

import java.util.List;
import com.shenbaoiot.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shenbaoiot.dev.mapper.MetadataMapper;
import com.shenbaoiot.dev.domain.Metadata;
import com.shenbaoiot.dev.service.IMetadataService;

/**
 * 物模型信息Service业务层处理
 * 
 * @author 常宝坤
 * @date 2023-05-10
 */
@Service
public class MetadataServiceImpl implements IMetadataService 
{
    @Autowired
    private MetadataMapper metadataMapper;

    /**
     * 查询物模型信息
     * 
     * @param id 物模型信息主键
     * @return 物模型信息
     */
    @Override
    public Metadata selectMetadataById(Long id)
    {
        return metadataMapper.selectMetadataById(id);
    }

    /**
     * 查询物模型信息列表
     * 
     * @param metadata 物模型信息
     * @return 物模型信息
     */
    @Override
    public List<Metadata> selectMetadataList(Metadata metadata)
    {
        return metadataMapper.selectMetadataList(metadata);
    }

    /**
     * 新增物模型信息
     * 
     * @param metadata 物模型信息
     * @return 结果
     */
    @Override
    public int insertMetadata(Metadata metadata)
    {
        metadata.setCreateTime(DateUtils.getNowDate());
        return metadataMapper.insertMetadata(metadata);
    }

    /**
     * 修改物模型信息
     * 
     * @param metadata 物模型信息
     * @return 结果
     */
    @Override
    public int updateMetadata(Metadata metadata)
    {
        metadata.setUpdateTime(DateUtils.getNowDate());
        return metadataMapper.updateMetadata(metadata);
    }

    /**
     * 批量删除物模型信息
     * 
     * @param ids 需要删除的物模型信息主键
     * @return 结果
     */
    @Override
    public int deleteMetadataByIds(Long[] ids)
    {
        return metadataMapper.deleteMetadataByIds(ids);
    }

    /**
     * 删除物模型信息信息
     * 
     * @param id 物模型信息主键
     * @return 结果
     */
    @Override
    public int deleteMetadataById(Long id)
    {
        return metadataMapper.deleteMetadataById(id);
    }
}
