package com.shenbaoiot.dev.service.impl;

import java.util.List;
import com.shenbaoiot.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shenbaoiot.dev.mapper.DevInfoMapper;
import com.shenbaoiot.dev.domain.DevInfo;
import com.shenbaoiot.dev.service.IDevInfoService;

/**
 * 设备管理Service业务层处理
 * 
 * @author 常宝坤
 * @date 2023-05-12
 */
@Service
public class DevInfoServiceImpl implements IDevInfoService 
{
    @Autowired
    private DevInfoMapper devInfoMapper;

    /**
     * 查询设备管理
     * 
     * @param id 设备管理主键
     * @return 设备管理
     */
    @Override
    public DevInfo selectDevInfoById(Long id)
    {
        return devInfoMapper.selectDevInfoById(id);
    }

    /**
     * 查询设备管理列表
     * 
     * @param devInfo 设备管理
     * @return 设备管理
     */
    @Override
    public List<DevInfo> selectDevInfoList(DevInfo devInfo)
    {
        return devInfoMapper.selectDevInfoList(devInfo);
    }

    /**
     * 新增设备管理
     * 
     * @param devInfo 设备管理
     * @return 结果
     */
    @Override
    public int insertDevInfo(DevInfo devInfo)
    {
        devInfo.setCreateTime(DateUtils.getNowDate());
        return devInfoMapper.insertDevInfo(devInfo);
    }

    /**
     * 修改设备管理
     * 
     * @param devInfo 设备管理
     * @return 结果
     */
    @Override
    public int updateDevInfo(DevInfo devInfo)
    {
        devInfo.setUpdateTime(DateUtils.getNowDate());
        return devInfoMapper.updateDevInfo(devInfo);
    }

    /**
     * 批量删除设备管理
     * 
     * @param ids 需要删除的设备管理主键
     * @return 结果
     */
    @Override
    public int deleteDevInfoByIds(Long[] ids)
    {
        return devInfoMapper.deleteDevInfoByIds(ids);
    }

    /**
     * 删除设备管理信息
     * 
     * @param id 设备管理主键
     * @return 结果
     */
    @Override
    public int deleteDevInfoById(Long id)
    {
        return devInfoMapper.deleteDevInfoById(id);
    }
}
