package com.shenbaoiot.dev.service.impl;

import java.util.List;
import com.shenbaoiot.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shenbaoiot.dev.mapper.DevProductMapper;
import com.shenbaoiot.dev.domain.DevProduct;
import com.shenbaoiot.dev.service.IDevProductService;

/**
 * 产品管理Service业务层处理
 * 
 * @author 常宝坤 - 13964179025
 * @date 2023-03-11
 */
@Service
public class DevProductServiceImpl implements IDevProductService 
{
    @Autowired
    private DevProductMapper devProductMapper;

    /**
     * 查询产品管理
     * 
     * @param id 产品管理主键
     * @return 产品管理
     */
    @Override
    public DevProduct selectDevProductById(Long id)
    {
        return devProductMapper.selectDevProductById(id);
    }

    /**
     * 查询产品管理列表
     * 
     * @param devProduct 产品管理
     * @return 产品管理
     */
    @Override
    public List<DevProduct> selectDevProductList(DevProduct devProduct)
    {
        return devProductMapper.selectDevProductList(devProduct);
    }

    /**
     * 新增产品管理
     * 
     * @param devProduct 产品管理
     * @return 结果
     */
    @Override
    public int insertDevProduct(DevProduct devProduct)
    {
        devProduct.setCreateTime(DateUtils.getNowDate());
        return devProductMapper.insertDevProduct(devProduct);
    }

    /**
     * 修改产品管理
     * 
     * @param devProduct 产品管理
     * @return 结果
     */
    @Override
    public int updateDevProduct(DevProduct devProduct)
    {
        devProduct.setUpdateTime(DateUtils.getNowDate());
        return devProductMapper.updateDevProduct(devProduct);
    }

    /**
     * 批量删除产品管理
     * 
     * @param ids 需要删除的产品管理主键
     * @return 结果
     */
    @Override
    public int deleteDevProductByIds(Long[] ids)
    {
        return devProductMapper.deleteDevProductByIds(ids);
    }

    /**
     * 删除产品管理信息
     * 
     * @param id 产品管理主键
     * @return 结果
     */
    @Override
    public int deleteDevProductById(Long id)
    {
        return devProductMapper.deleteDevProductById(id);
    }
}
