package com.shenbaoiot.dev.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.shenbaoiot.common.domain.TSL.Properties;
import com.shenbaoiot.common.domain.TSL.TSL;
import com.shenbaoiot.dev.domain.Metadata;
import com.shenbaoiot.dev.domain.TSL.TSLReceive;
import com.shenbaoiot.dev.mapper.MetadataMapper;
import com.shenbaoiot.dev.mapper.TSLMapper;
import com.shenbaoiot.dev.service.ITSLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 物模型信息Service业务层处理
 *
 * @author 常宝坤
 * @date 2023-05-10
 */
@Service
public class TSLServiceImpl implements ITSLService
{
    @Autowired
    private TSLMapper tslMapper;
    @Autowired
    private MetadataMapper metadataMapper;
//    /**
//     * 新增物联网模型
//     * @param tsl 物模型信息
//     * @return
//     */
//
//    @Override
//    public int add(TSL tsl) {
//        //json解析，注意此处必须转成 JSONObject，后面才可以转成java对象
//        JSONObject reqMetadata = JSONObject.parseObject(tsl.getParam());
//        System.out.println(reqMetadata);
//
//
//        //根据属性唯一标识获取整个物模型数据 metadata.metadata
//        Metadata req = new Metadata();
//        req.setWhoBelongs(tsl.getWhoBelongs());
//        req.setWhoBelongsId(tsl.getWhoBelongsId());
//        List<Metadata> reqList = metadataMapper.selectMetadataList(req);
//        String metadata = reqList.size()>0?reqList.get(0).getMetadata():null;
//        //整个物模型数据
//        JSONObject allMetadata = JSONObject.parseObject(metadata);
//        List<Map> typeData = (List<Map>) allMetadata.get(tsl.getType());
//        for (Map map:typeData) {
//            //存在
//            if(reqMetadata.get("identifier").equals(map.get("identifier"))){
//                System.out.println("存在");
//            }
//
//        }
//        System.out.println("新增");
//        return 0;
//    }
//
//    /**
//     * 编辑物联网模型
//     * @param tsl 物模型信息
//     * @return
//     */

    @Override
    public int addEitTsl(TSLReceive tslReceive) {
        //操作类型operateType  add-新增 update-更新 delete-删除
        //json解析，注意此处必须转成 JSONObject，后面才可以转成java对象
        JSONObject reqMetadata = JSONObject.parseObject(tslReceive.getParam());
        System.out.println(reqMetadata);
        //根据属性唯一标识获取整个物模型数据 metadata.metadata
        Metadata req = new Metadata();
        req.setWhoBelongs(tslReceive.getWhoBelongs());
        req.setWhoBelongsId(tslReceive.getWhoBelongsId());
        List<Metadata> reqList = metadataMapper.selectMetadataList(req);
        //整个物模型数据--tsl
        Metadata metadata = reqList.size()>0?reqList.get(0):null;
        TSL tsl = JSONObject.parseObject(metadata.getMetadata(), TSL.class);
        TSL tslReturn = tsl;
        //属性
        if("properties".equals(tslReceive.getType())){
            //请求的properties - propertiesReq
            Properties propertiesReq = JSONObject.parseObject(tslReceive.getParam(), Properties.class);
            //物模型的properties - properties
            List<Properties> properties = tsl.getProperties();
            //最终返回的properties - propertiesRet
            List<Properties> propertiesRet = properties;
            Boolean isAdd = true;
            for (Properties propertie:properties) {
                //存在,则修改
                if(propertiesReq.getIdentifier().equals(propertie.getIdentifier())){
                    System.out.println("存在");
                    isAdd = false;
                    propertiesRet.remove(propertie);
                    propertiesRet.add(propertiesReq);
                }
            }
            //不存在，新增
            if(isAdd){
                propertiesRet.add(propertiesReq);
            }
            tslReturn.setProperties(propertiesRet);
        }
        //实体类转string 并存储
        String tslStringReturn = JSONObject.toJSONString(tslReturn);
        metadata.setMetadata(tslStringReturn);
        return metadataMapper.updateMetadata(metadata);
    }

    @Override
    public int addTSL(TSLReceive tslReceive) {
        return 0;
    }

    @Override
    public int editTSL(TSLReceive tslReceive) {
        return 0;
    }

    @Override
    public int deleteTSL(TSLReceive tslReceive) {
        return 0;
    }
}
