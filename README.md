# shenbao-iot开源物联网平台

### 平台官网

[http://www.shenbaoiot.com](http://www.shenbaoiot.com)

### 平台介绍

shenbao-iot物联网平台是一款全部开源的物联网基础平台,支持设备管理，多协议适配，数据收集，规则引擎，快速构建物联网相关业务系统,全部源代码开放,可自由拓展功能,不再受制于人.前后端分离,接口全开放。
统一设备接入,海量设备管理
TCP/UDP/MQTT/HTTP、TLS/DTLS、不同厂商、不同设备、不同报文、统一接入，统一管理。
灵活的规则模型配置,支持多种规则模型以及自定义规则模型. 设备告警,场景。

### 软件架构 

软件架构说明
前端采用Vue、Element UI。
后端采用Spring Boot、Spring Security、Redis & Jwt。
JDK >= 1.8
MySQL >= 5.7
Maven >= 3.0
Node >= 12
Redis >= 3
netty >= 4.1.94

### 部分页面展示

![输入图片说明](shenbao-ui/public/html/1.png)

特别鸣谢：RuoYi-Vue
### 平台快速体验与技术交流微信群

![输入图片说明](shenbao-ui/public/html/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230516083410.jpg)




